# PPROG Trabalho Prático 1
### 2º Semestre - 2018/2019

##  
**Objetivos Específicos** : Declaração de classes. Construtores. Membros de instância e de classe. Herança.
Polimorfismo.

##  
**Enunciado**

Um clube desportivo regista informação básica acerca dos seus atletas para poder calcular o valor a pagar a
cada atleta no final de cada mês. Para auxiliar à informatização deste processo, pretende-se que implemente
em JAVA, um projeto com as classes necessárias para representar os atletas do clube desportivo.

Um atleta é caraterizado pelo: nome, número de identificação civil, género e idade. Os atletas também têm
uma atividade associada que pode ser: _caminhada_ , _corrida_ , _ciclismo_ ou _natação._

Todos os atletas são sujeitos ao cálculo da Frequência Cardíaca Máxima (FCM) e da Frequência Cardíaca de
Trabalho (FCT). A forma de cálculo da FCM é apresentada na Tabela 1 e, conforme se verifica, difere de acordo
com a atividade e/ou género do atleta.

##  
| Atividade | Género               | FCM                           |
|-----------|----------------------|-------------------------------|
| Caminhada Corrida | Feminino e Masculino | 208,75 − (0,73 𝑥 𝑖𝑑𝑎𝑑𝑒) |
| Ciclismo  | Feminino             | 189 − (0,56 𝑥 𝑖𝑑𝑎𝑑𝑒)    |
| Ciclismo          | Masculino            | 202 − (0,72 𝑥 𝑖𝑑𝑎𝑑𝑒)    |
| Natação   | Feminino e Masculino | 204 − (1,7 𝑥 𝑖𝑑𝑎𝑑𝑒)     |

```
Tabela 1 - Cálculo da FCM
```

A FCT, cuja fórmula de cálculo se apresenta abaixo, envolve os valores da: FCM, Frequência Cardíaca em
Repouso (FCR) e a percentagem de Intensidade do Treino (IT) necessária para o objetivo. A FCR é um valor que
representa o número de batimentos cardíacos em repouso durante 1 minuto e a IT pode ter como valor: 0,
quando o objetivo é a queima de gordura ou 0,75 quando o objetivo é trabalhar a capacidade
cardiorrespiratória.
##  
```
𝐹𝐶𝑇 = 𝐹𝐶𝑅 + [IT 𝑥 (𝐹𝐶𝑀 − 𝐹𝐶𝑅)]
```
###  
A disponibilidade para treinar e/ou competir faz com que os atletas possam ser considerados: profissionais ou
não profissionais. No caso de serem não profissionais, podem ser ainda considerados: semiprofissionais ou
amadores. Um atleta não profissional deve ter, também, associado a antiguidade (número de anos que está
filiado ao clube). Todos os atletas têm associado um valor mensal relativo aos prémios que consiga arrecadar
nas competições em que participa.

De seguida, são apresentadas algumas particularidades para cada tipo de atleta.

Atletas Profissionais: estes atletas recebem ao fim do mês o valor resultante da soma de duas parcelas: uma
fixa e outra variável. A parcela fixa é diferente entre atletas e pode ser atualizada, enquanto que a parcela
variável está dependente do valor mensal arrecadado em prémios pelo atleta. O valor da parcela variável é de
20% do valor mensal arrecadado. No entanto, o valor desta percentagem pode ser atualizado.

Atletas Semiprofissionais: estes atletas recebem ao fim do mês o valor resultante da soma de duas parcelas:
uma fixa e outra variável. A parcela fixa é igual para todos estes atletas e pode ser atualizada, enquanto que a
parcela variável está dependente da antiguidade. O valor da parcela variável é uma percentagem sobre o valor
da parcela fixa, de acordo com a antiguidade (ver Tabela 2 ).

Atletas Amadores: estes atletas recebem ao fim do mês o valor resultante da soma de duas parcelas variáveis
associadas ao valor mensal arrecadado em prémios pelo atleta. A primeira parcela é uma percentagem do
valor mensal arrecadado, de acordo com a antiguidade (ver Tabela 2 ). A segunda parcela é também uma
percentagem do valor mensal arrecadado, tendo um valor igual para todos os atletas de 7%. Por indicação do
clube, todos os atletas amadores recebem pelo menos 5 euros por mês. No entanto, o valor da percentagem
da segunda parcela e o valor mínimo a receber pode ser atualizado.

##  
| Antiguidade | Percentagem^1               |
|-----------|----------------------|-------------------------------|
| [5,10] anos  | 2% | 
| ]10,20] anos  | 8% | 
| mais que 20 anos  | 20% | 
```
Tabela 2 - Percentagens mediante a antiguidade
```

###  
Todo o código produzido deve ter sempre em consideração os principais princípios da programação orientada
por objetos: abstração, encapsulamento, herança e polimorfismo.

As classes criadas (com exceção da classe principal) devem obedecer ao seguinte conjunto de especificações:

* implementação de construtores (pelo menos o construtor completo e o construtor sem parâmetros);
* implementação de métodos que sejam relevantes para aceder e modificar o valor dos atributos;
* reescrita do método _toString_ ;
* inclusão de comentários _javadoc_.

Adicionalmente devem ser implementadas, nas respetivas classes, funcionalidades para o cálculo do valor a
pagar mensalmente a cada atleta. Devem também ser criados testes unitários para o cálculo referido
anteriormente, apenas para os atletas profissionais e amadores.

Na classe principal, o código a implementar deve preencher os seguintes requisitos:

* criação do seguinte conjunto de instâncias:
	* 2 atletas profissionais;
	* 3 atletas semiprofissionais;
	* 2 atletas amadores.
* criação de um contentor do tipo _array_ e armazenamento no mesmo das instâncias criadas;
* criação de listagens separadas, sobre o contentor, para:
	* obter o nome, a FCM e as FCT de cada atleta semiprofissional e amador;
	* obter o nome e o valor a pagar de cada atleta.
* apresentação das quantidades de instâncias de atletas amadores e profissionais criadas, sem
percorrer o contentor;
* cálculo e apresentação do valor total a pagar a cada tipo de atleta (profissional, semiprofissional e
amador), percorrendo apenas uma vez o contentor. Deve ser também calculado e apresentado o valor
total a pagar a todos os atletas.

(^1) Estes valores podem ser atualizados.

##  

**Normas**

A designação de todos os elementos a serem submetidos, através do Moodle do ISEP, tem de obedecer ao
seguinte formato **Turma_TP1_Nºaluno_Nºaluno** , como por exemplo, 1DA_TP1_1180123_1180456. Estes
elementos são: um ficheiro ZIP com o projeto Maven e um ficheiro com o diagrama de classes, legível, em
formato PDF. A designação do projeto Maven (visível no IDE) tem de obedecer ao mesmo formato. Todos os
elementos do grupo têm de submeter no Moodle do ISEP os elementos. No dia da apresentação deverá ser
entregue o diagrama de classes impresso, ao docente da aula PL.

O projeto Maven deve ser implementado recorrendo a um repositório do Bitbucket, criado e configurado por
um dos elementos do grupo. O professor das aulas PL tem que ser adicionado à lista de elementos com acesso
ao repositório.


# PPROG Trabalho Prático 2
### 2º Semestre - 2018/2019

##  
**Objetivos Específicos** : Interfaces, Agregação e Composição. 
Polimorfismo.

##  
**Enunciado**

Pretende-se uma nova iteração do trabalho prático TP1 para satisfazer novos requisitos, utilizando os seguintes conceitos: interfaces, agregação e composição de classes.
Tal como referido no TP1, os atletas que fazem parte do clube podem ser: 

* Profissional
* Semiprofissional
* Amador

**Note bem:** Apesar de, neste momento, existirem apenas estas três categorias de atletas poderão surgir outras no futuro.

Todos os atletas que auferem uma componente fixa (neste momento, apenas os __Profissionais__ e __Semiprofissionais__) devem descontar, sobre essa componente, uma taxa imutável de 10% para o IRS.
Deverá existir também uma classe instanciável de nome ClubeDesportivo que tem por atributos o
nome, a data de fundação (Data) e um contentor de objetos para armazenar os atletas (associados por
composição). Esta classe deverá permitir às suas instâncias disponibilizar as seguintes funcionalidades: 
1. Retornar o nome do clube;
2. Inserir um novo atleta no contentor (associado por composição);
3. Retornar uma lista de atletas, ordenada alfabeticamente por nome;
4. Retornar uma lista de atletas, ordenada inversamente pelo valor dos prémios;
5. Retornar o valor total, para efeitos de IRS, da totalidade dos atletas;
6. Retornar uma lista de atletas do clube, ordenada alfabeticamente por categoria, modalidade e nome.

O método _main()_ da classe principal deverá ter o seguinte comportamento:
* Criar uma instancia da classe ClubeDesportivo;
* Armazenar nessa instância 3 objetos de cada uma das categorias (Profissional, Semiprofissional e Amador);
* Executar as funcionalidades de 1 a 6 e visualizar o seu resultado

Deverá utilizar o plugin JaCoCo no Netbeans para verificar a cobertura de testes. Deverá implementar testes unitários para as funcionalidades de 1 a 3.
O código deverá conter os comentários necessários para que possa ser gerada a documentação usando a ferramenta Javadoc.

Deverá entregar através do Moodle um ficheiro em formato ZIP que contenha o projeto desenvolvido e o diagrama de classes da solução. Este diagrama deverá estar num documento PDF. O **nome do projeto** (no NetBeans) e o **nome do documento PDF**, deverão ter, obrigatoriamente, o seguinte formato:

PPROG_TURMA_NÚMEROALUNO1_NÚMEROALUNO2_TP2

Exemplo: PPROG_1DJ_1113212_111323_TP2

Todos os alunos deverão proceder à submissão, devendo os elementos de cada grupo submeter versões
iguais.

__No dia da apresentação deverá ser entregue ao docente das aulas PL o diagrama de classes impresso.__

O projeto Maven deve ser implementado recorrendo a um repositório do Bitbucket, criado e
configurado por um dos elementos do grupo. O professor das aulas PL tem que ser adicionado à lista
de elementos com acesso ao repositório.


# PPROG Trabalho Prático 3
### 2º Semestre - 2018/2019

##  
**Enunciado**

O  trabalho  prático  de  Paradigmas  de  Programação  (PPROG)  será  realizado  em 
articulação  com  a  unidade  curricular  de  Engenharia  de  Software  (ESOFT).  O 
trabalho de PPROG consiste na implementação das funcionalidades definidas nos
seguintes  Casos  de  Uso  do  projeto que  está  a  ser  desenvolvido  em  ESOFT, 
intitulado “Sistema de Gestão de Prestação de Serviços Domésticos”:
* **Caso de Uso UC6:** Efetuar pedido de prestação de serviços (Ator: Cliente)
* **Caso de Uso UC9:** Indicar disponibilidade diária de prestação de serviços (Ator: Prestador de Serviços)(Caso de uso introduzido na Iteração 2 de ESOFT)

Para além das funcionalidades relativas aos casos de uso **UC6** e **UC9**, pretende-se 
ainda a implementação de uma outra funcionalidade: **Atribuir Pedido de Prestação 
de Serviço a um Prestador de Serviços (Ator: Administrativo)**.
Esta funcionalidade deverá permitir atribuir um prestador de serviço a um serviço 
solicitado por um cliente,  tendo em consideração o cumprimento dos seguintes 
critérios:
* A  afetação  de  um  prestador  a  um  serviço  deve  ser  feita  de  forma  a 
minimizar a distância entre o local da prestação do serviço (definido pelo 
código postal do local de prestação do serviço) e uma das zonas geográficas 
de atuação do prestador de serviços (definidas também pelo código postal 
da zona geográfica);
* A afetação de um prestador a um serviço deverá respeitar a existência de 
compatibilidade  entre  o  horário  de  disponibilidade  do  prestador  e  o 
horário indicado pelo cliente para a realização do serviço.

De  forma  a  calcular  a  distância  entre  códigos  postais,  será  usada  a  distância 
euclidiana entre o ponto central das zonas representadas por cada código postal. 
O ponto central de cada código postal é definido por coordenadas de latitude e 
longitude em graus, definidas num ficheiro de texto com o seguinte formato:
####  
```
Código postal; Latitude (graus); Longitude(graus)
4100-000; 41.149304; -8.610880
```
####  
A	distância	entre	pontos	definidos	pelas	suas	coordenadas	pode	ser	determinada	usando	o	seguinte	código:
####  
```
double distancia (double lat1, double lon1, double lat2, double lon2) {
    // shortest distance over the earth’s surface
    // https://www.movable-type.co.uk/scripts/latlong.html
    final double R = 6371e3;
    double theta1 = Math.toRadians(lat1);
    double theta2 = Math.toRadians(lat2);
    double deltaTheta = Math.toRadians(lat2 – lat1);
    double deltaLambda = Math.toRadians(lon2 – lon1);
    double a = Math.sin(deltaTheta / 2) * Math.sin(deltaTheta / 2)
        + Math.cos(theta1) * Math.cos(theta2) 
				* Math.sin(deltaLambda / 2) * Math.sin(deltaLambda / 2);
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    double d = R * c; // distância em metros
    return d;
}
```
####  
A implementação desta  funcionalidade deve  ter em conta a possibilidade de no futuro serem considerados critérios alternativos de afetação  de  prestadores de serviço.

Na implementação do projeto devem ser considerados os requisitos da Iteração 2 do trabalho de ESOFT. Os enunciados das Iterações 1 e 2 encontram-se em anexo.
Os respetivos artefactos podem ser consultados no endereço https://bitbucket.org/pafomaio/esoft2018-2019-resolucao/. Este repositório é partilhado pelo que o seu conteúdo não deve ser alterado.

No mesmo repositório está disponível o código de algumas classes Java que pode ser usado na implementação do trabalho de PPROG.

De forma a que a aplicação desenvolvida seja funcional, será necessário criar objetos de várias classes (cujo código está disponível). A implementação deverá incluir um mecanismo a executar no arranque da aplicação que permita criar os objetos das várias classes a partir de dados armazenados em  ficheiros de texto. Por exemplo, o excerto de um ficheiro usado para armazenar informação acerca de instâncias da classe Categoria (de Serviços) que possui os atributos “código” e “descrição” poderia ser:
####  
```
cat1; Limpeza
cat2; Reparação de Canalizações
...
```
####  
Desta  forma, após o arranque da aplicação, existirão em  memória objetos contendo a descrição de categorias, serviços, áreas geográficas, clientes, endereços postais dos clientes e prestadores de serviço.
Os requisitos funcionais da implementação incluem:

* implementação das funcionalidades descritas;
* implementação de uma classe que instancie  todas as classes necessárias para que as funcionalidades descritas possam ser utilizadas/testadas durante a apresentação do trabalho; esta classe deve criar instâncias de classes como por exemplo Categoria, Cliente, Servico, entre outras, já definidas no modelo de design, e ainda de outras classes a definir, como por exemplo PrestadorServico;
* implementação de um mecanismo que permita garantir a persistência dos dados, através do seu armazenamento num ficheiro binário, e posterior recuperação (implementação de mecanismos de leitura e de escrita); esta funcionalidade deverá aplicar-se apenas à classe PedidoPrestacaoServico;
* implementação de uma interface com o utilizador que permita usar as funcionalidades implementadas; a interface poderá ser baseada em consola ou poderá consistir numa interface gráfica (esta última com valorização adicional – ver tópico avaliação);
* implementação de um mecanismo de instanciação de classes a partir do conteúdo de um ficheiro de texto – o ficheiro de texto deverá estar disponível no momento da apresentação do projeto contendo informação para a instanciação de todas as classes necessárias ao funcionamento da aplicação.

Os requisitos não funcionais incluem:

 * desenvolvimento em linguagem Java e num projeto Maven;
 * documentação do código, usando a ferramenta Javadoc;
 * diagrama de classes em formato PDF;
 * documento contendo a descrição das funcionalidades implementadas;
 * implementação de testes unitários usando JUnit excetuando métodos getter, setter e toString;
 * geração do relatório de cobertura de testes usando o plugin JaCoCo;
 * utilização do repositório Bitbucket e da ferramenta de controlo de versões Git – **o repositório deverá ser privado e partilhado com o professor das aulas práticas – o nome do repositório deve respeitar o seguinte formato: PPROG_Turma_NúmeroAluno1_NúmeroAluno2_TP3;**
 * **execução regular de commits por parte de cada um dos elementos 
do grupo durante todo o período de desenvolvimento do projeto.**

## Normas de Funcionamento
####  

### Constituição dos grupos
O trabalho será realizado em grupos constituídos por dois alunos da mesma turma PL. Os alunos deverão informar  por e-mail  os  respetivos  professores acerca  da constituição dos grupos até ao dia **18 de abril de 2019**. O não cumprimento deste requisito implicará a não avaliação do trabalho.

No  caso  de  não  ser  possível  proceder à  formação  de  um grupo  constituído  por 
elementos da mesma turma PL, serão admissíveis situações excecionais de grupos 
constituídos  por elementos  de  diferentes  turmas,  desde  que  devidamente
justificadas e após aprovação por parte dos professores das aulas PL.

### Avaliação

A avaliação levará em consideração fatores como a implementação  de funcionalidades, utilização de boas práticas de codificação, qualidade da documentação e apresentação.
A interface com o utilizador  terá um peso de 10% na avaliação do trabalho. No caso de ser implementada uma interface de consola, o respetivo item de avaliação será avaliado no máximo em 5%, sendo este máximo de 10% no caso de ser implementada uma interface gráfica.

### Alunos que não frequentam ESOFT
Sugere-se que os alunos que não  frequentem ESOFT constituam grupo com um aluno que se encontre a frequentar ESOFT. Serão disponibilizados no site da Wiki 
de ESOFT propostas de modelo de casos de uso, modelo de domínio e modelo de design (diagrama de classes e diagrama de sequência).

### Entrega

A entrega será realizada através de submissão no Moodle até às **23h30 do dia 18
de maio de 2019 (Sábado)**. Esta entrega é constituída por um único ficheiro em 
formato  ZIP,  não  sendo  consideradas  submissões  de  ficheiros  com  outros 
formatos. O ficheiro ZIP deve cumprir os seguintes requisitos:

* O nome do ficheiro deve seguir o seguinte formato **PPROG_Turma_NúmeroAluno1_NúmeroAluno2_TP3** (exemplo: PPROG_1DA_1870049_1120999_TP3.zip);
* O ficheiro ZIP deve incluir o seguinte:
	* Pasta contendo o projeto Maven, cujo nome segue as regras definidas para o nome do ficheiro ZIP;
	* Pasta com nome Doc contendo a documentação em formato HTML gerada pela ferramenta Javadoc;
	* Ficheiro em formato PDF com o nome requisitos.pdf contendo a enumeração das funcionalidades implementadas, assim como uma breve descrição de cada uma delas;
	* Ficheiro em formato PDF com o nome diagramaClasses.pdf contendo o diagrama de classes implementado.

Não  serão  consideradas  para  avaliação  submissões  que  não  cumpram 
completamente os requisitos definidos para a entrega. Poderão ser  feitas várias 
submissões,  sendo  considerada  apenas  a  última. As  submissões  devem  ser 
efetuadas por ambos os elementos do grupo.

### Apresentação
Após  a  submissão,  os  trabalhos  terão que ser  apresentados pelos membros  do 
grupo ao respetivo professor das aulas práticas durante a última semana de aulas 
(12ª semana).

###Nota importante
A utilização do repositório Bitbucket e da ferramenta de controlo de versões Git é obrigatória. A não existência deste repositório, assim como a falta de evidências acerca de commits regulares implica a não avaliação do trabalho.**