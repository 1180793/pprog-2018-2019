package com.xxsd.pprog_pw03.exceptions;

@SuppressWarnings("serial")
public class DuplicatedIDException extends Exception {
	
	public DuplicatedIDException(String message) {
		super(message);
	}
}
