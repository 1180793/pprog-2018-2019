package com.xxsd.pprog_pw03.controllers;

import java.io.IOException;

import com.xxsd.pprog_pw03.utils.FxUtils;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class DashboardAdministrativoController {

	@FXML
    private Label lblTitle;
	
    @FXML
    private Button btnPrestadores;
    
    @FXML
    private Button btnClientes;

    @FXML
    private Button btnServicos;
    
    @FXML
    private Button btnAtribuirPrestadores;

    @FXML
    private void handleClientes(ActionEvent event) throws IOException {
    	FxUtils.switchScene(event, "ClientesUI", "Clientes Registados");
    }

    @FXML
    private void handlePrestadores(ActionEvent event) throws IOException {
    	FxUtils.switchScene(event, "PrestadoresUI", "Prestadores Registados");
    }

    @FXML
    private void handleServicos(ActionEvent event) throws IOException {
    	FxUtils.switchScene(event, "ServicosUI", "Serviços Disponíveis");
    }

    @FXML
    private void handleAtribuirPrestadores(ActionEvent event) throws IOException {
    	FxUtils.switchScene(event, "PedidosUI", "Atribuir Prestadores de Serviços a Pedido");
    }

}
