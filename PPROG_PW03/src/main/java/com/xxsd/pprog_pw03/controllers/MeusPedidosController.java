package com.xxsd.pprog_pw03.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.xxsd.pprog_pw03.models.Cliente;
import com.xxsd.pprog_pw03.models.DescricaoServicoPedido;
import com.xxsd.pprog_pw03.models.Empresa;
import com.xxsd.pprog_pw03.models.PedidoPrestacaoServico;
import com.xxsd.pprog_pw03.models.PreferenciaHorario;
import com.xxsd.pprog_pw03.models.RegistoClientes;
import com.xxsd.pprog_pw03.models.RegistoPedidos;
import com.xxsd.pprog_pw03.utils.FxUtils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class MeusPedidosController implements Initializable {

	private AplicacaoGPSD oApp = AplicacaoGPSD.getInstance();
	private Empresa oEmpresa = oApp.getEmpresa();
	private RegistoClientes oClientes = oEmpresa.getRegistoClientes();
	private RegistoPedidos oPedidos = oEmpresa.getRegistoPedidos();

	@FXML
	private Label lblTitle;

	@FXML
	private ChoiceBox<PedidoPrestacaoServico> cbxPedido;

	@FXML
	private TextField txtNumSequencial;

	@FXML
	private TextField txtEnderecoPostal;

	@FXML
	private TextField txtStatus;

	@FXML
	private TableView<DescricaoServicoPedido> tableServicos;

	@FXML
	private TableColumn<DescricaoServicoPedido, String> colServico;

	@FXML
	private TableColumn<DescricaoServicoPedido, String> colDuracao;

	@FXML
	private TableColumn<DescricaoServicoPedido, String> colDescricao;

	@FXML
	private TableColumn<DescricaoServicoPedido, String> colPrestador;

	@FXML
	private TableView<PreferenciaHorario> tableHorarios;

	@FXML
	private TableColumn<PreferenciaHorario, Integer> colOrdem;

	@FXML
	private TableColumn<PreferenciaHorario, String> colData;

	@FXML
	private TableColumn<PreferenciaHorario, String> colHora;

	@FXML
	private Button btnVoltar;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		populatePedidos();
		listenerPedidos();
	}

	private void populatePedidos() {
		Cliente oCliente = oClientes.getClienteByEmail(oApp.getSessaoAtual().getEmailUtilizador());
		List<PedidoPrestacaoServico> lstPedidos = oPedidos.getPedidosByCliente(oCliente);
		cbxPedido.setItems(FXCollections.observableArrayList(lstPedidos));
	}

	private void listenerPedidos() {
		cbxPedido.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (newValue == null) {
				clearPedido();
				clearTables();
			} else {
				populatePedido(newValue);
				populateTables(newValue);
			}
		});
	}

	private void clearTables() {
		clearServicosTable();
		clearHorariosTable();
	}

	private void clearPedido() {
		txtNumSequencial.clear();
		txtEnderecoPostal.clear();
		txtStatus.clear();
	}

	private void clearServicosTable() {
		tableServicos.getItems().clear();
	}

	private void clearHorariosTable() {
		tableHorarios.getItems().clear();
	}

	private void populatePedido(PedidoPrestacaoServico oPedido) {
		if (oPedido == null) {
			System.out.println("ewdedwewewewew");
		}
		txtNumSequencial.setText(oPedido.toString());
		txtEnderecoPostal.setText(oPedido.getEnderecoString());
		txtStatus.setText(oPedido.getStatus());
	}

	private void populateTables(PedidoPrestacaoServico oPedido) {
		populateServicosTable(oPedido);
		populateHorariosTable(oPedido);
	}

	private void populateServicosTable(PedidoPrestacaoServico oPedido) {
		colServico.setCellValueFactory(new PropertyValueFactory<>("servicoString"));
		colDuracao.setCellValueFactory(new PropertyValueFactory<>("duracao"));
		colDescricao.setCellValueFactory(new PropertyValueFactory<>("descricao"));
		colPrestador.setCellValueFactory(new PropertyValueFactory<>("prestadorString"));
		ObservableList<DescricaoServicoPedido> list = FXCollections
				.observableArrayList(oPedido.getServicosPedidos());
		tableServicos.setItems(list);
	}

	private void populateHorariosTable(PedidoPrestacaoServico oPedido) {
		colOrdem.setCellValueFactory(new PropertyValueFactory<>("ordem"));
		colData.setCellValueFactory(new PropertyValueFactory<>("data"));
		colHora.setCellValueFactory(new PropertyValueFactory<>("hora"));
		ObservableList<PreferenciaHorario> list = FXCollections.observableArrayList(oPedido.getHorarios());
		tableHorarios.setItems(list);
	}
	
	@FXML
    private void handleCancelar(ActionEvent event) throws IOException {
    	String strPapel = oApp.getPapel();
    	FxUtils.switchDashboard(event, strPapel, oApp.getSessaoAtual().getEmailUtilizador());
    }
	
}
