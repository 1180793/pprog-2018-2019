package com.xxsd.pprog_pw03.controllers;

import java.io.IOException;

import com.xxsd.pprog_pw03.utils.FxUtils;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class DashboardClientesController {

	@FXML
    private Label lblTitle;

    @FXML
    private Button btnMeusPedidos;
    
    @FXML
    private Button btnServicos;
    
    @FXML
    private Button btnEfetuarPedido;

    @FXML
    private void handleMeusPedidos(ActionEvent event) throws IOException {
    	FxUtils.switchScene(event, "MeusPedidos", "Meus Pedidos");
    }

    @FXML
    private void handleServicos(ActionEvent event) throws IOException {
    	FxUtils.switchScene(event, "ServicosUI", "Serviços Disponíveis");
    }

    @FXML
    private void handleEfetuarPedido(ActionEvent event) throws IOException {
    	FxUtils.switchScene(event, "EfetuarPedidoPrestacaoServicos",
				"Efetuar Pedido de Prestação de Serviços");
    }

}
