package com.xxsd.pprog_pw03.controllers;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.xxsd.pprog_pw03.MainApp;
import com.xxsd.pprog_pw03.autorizacao.models.PapelUtilizador;
import com.xxsd.pprog_pw03.utils.FileChooserUtils;
import com.xxsd.pprog_pw03.utils.FxUtils;
import com.xxsd.pprog_pw03.utils.Utils;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

public class TopBarController implements Initializable {

	private AplicacaoGPSD oApp = AplicacaoGPSD.getInstance();

	@FXML
	private MenuItem itmChangePapeis;

	@FXML
	void handleMudarPapeis(ActionEvent event) throws IOException {
		FxUtils.switchPapeis(event);
	}

	@FXML
	private void doLogout(ActionEvent event) throws IOException {
		AplicacaoGPSD.getInstance().doLogout();
		FxUtils.switchLogin(event);
	}

	@FXML
	private void closeApp(ActionEvent event) {
		Window window = MainApp.primarystage;
        window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
	}

	@FXML
	private void loadPedidos(ActionEvent event) {
		FileChooser fileChooser = FileChooserUtils.getFileChooser();
		File savedFile = fileChooser.showOpenDialog(MainApp.primarystage);
		if (savedFile != null) {
			FileChooserUtils.deserealizePedidos(savedFile);
			Utils.openAlert("Pedidos de Prestação de Serviços Importados", "Pedidos de Prestação de Serviços Importados", "Pedidos de Prestação de Serviços Importados com Sucesso", AlertType.INFORMATION);
		} else {
			Utils.openAlert("Erro ao Importar Pedidos de Prestação de Serviços", "Erro ao Importar Pedidos de Prestação de Serviços", "Ficheiro Inválido", AlertType.ERROR);
		}
	}

	@FXML
	private void exportPedidos(ActionEvent event) {
		FileChooser fileChooser = FileChooserUtils.getFileChooser();
		File savedFile = fileChooser.showSaveDialog(MainApp.primarystage);
		if (savedFile != null) {
			FileChooserUtils.serealizePedidos(savedFile);
			Utils.openAlert("Pedidos de Prestação de Serviços Exportados", "Pedidos de Prestação de Serviços Exportados", "Pedidos de Prestação de Serviços Exportados com Sucesso", AlertType.INFORMATION);
		} else {
			Utils.openAlert("Erro ao Exportar Pedidos de Prestação de Serviços", "Erro ao Exportar Pedidos de Prestação de Serviços", "Ficheiro Inválido", AlertType.ERROR);
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		List<PapelUtilizador> lstPapeis = oApp.getSessaoAtual().getPapeisUtilizador();
		if (lstPapeis.size() <= 1) {
			itmChangePapeis.setDisable(true);
		}
	}

}
