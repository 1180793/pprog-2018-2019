package com.xxsd.pprog_pw03.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.xxsd.pprog_pw03.autorizacao.models.PapelUtilizador;
//import com.xxsd.pprog_pw03.autorizacao.models.SessaoUtilizador;
//import com.xxsd.pprog_pw03.models.Cliente;
import com.xxsd.pprog_pw03.models.Constantes;
//import com.xxsd.pprog_pw03.models.Empresa;
//import com.xxsd.pprog_pw03.models.PrestadorServicos;
//import com.xxsd.pprog_pw03.models.RegistoClientes;
//import com.xxsd.pprog_pw03.models.RegistoPrestadores;
import com.xxsd.pprog_pw03.utils.FxUtils;
import com.xxsd.pprog_pw03.utils.Utils;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class LoginController implements Initializable {

	private AplicacaoGPSD oApp = AplicacaoGPSD.getInstance();

	@FXML
	private Label lblTitle;
	@FXML
	private TextField txtEmail;
	@FXML
	private PasswordField txtPassword;
	@FXML
	private Button btnLogin;

	public LoginController() {
		this.oApp = AplicacaoGPSD.getInstance();
	}

	@FXML
	private void onLogin(ActionEvent event) throws IOException {
		String strEmail = txtEmail.getText();
		String strPwd = txtPassword.getText();
		boolean isValid = this.oApp.doLogin(strEmail, strPwd);
		if (!isValid) {
			Utils.openAlert("Erro de Autenticação", "Credenciais Inválidas", "E-mail e/ou palavra-passe inválidos.",
					AlertType.ERROR);
		} else {
			Utils.openAlert("Autenticação Concluída", "Autenticado com sucesso",
					"Clique em OK para prosseguir para a aplicação", AlertType.INFORMATION);
			List<PapelUtilizador> lstPapeis = oApp.getSessaoAtual().getPapeisUtilizador();
			PapelUtilizador oPapel = null;
			if (lstPapeis.size() == 1) {
				oPapel = lstPapeis.get(0);
			}
			if (oPapel != null) {
				if (oPapel.hasId(Constantes.PAPEL_CLIENTE)) {
					FxUtils.switchDashboard(event, Constantes.PAPEL_CLIENTE, strEmail);
					AplicacaoGPSD.getInstance().setPapel(Constantes.PAPEL_CLIENTE);
				}
				if (oPapel.hasId(Constantes.PAPEL_PRESTADOR_SERVICO)) {
					System.out.println("true3");
					FxUtils.switchDashboard(event, Constantes.PAPEL_PRESTADOR_SERVICO, strEmail);
					AplicacaoGPSD.getInstance().setPapel(Constantes.PAPEL_PRESTADOR_SERVICO);
				}
				if (oPapel.hasId(Constantes.PAPEL_ADMINISTRATIVO)) {
					FxUtils.switchDashboard(event, Constantes.PAPEL_ADMINISTRATIVO, strEmail);
					AplicacaoGPSD.getInstance().setPapel(Constantes.PAPEL_ADMINISTRATIVO);
				}
				if (oPapel.hasId(Constantes.PAPEL_FRH)) {
					Utils.openAlert("Erro ao Escolher Papel", "Dashboard Não Implementada",
							"A Dashboard para o Papel 'Funcionário de Recursos Humanos' ainda não foi implementada.",
							AlertType.ERROR);
				}
			} else {
				FxUtils.switchPapeis(event);
			}
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

}
