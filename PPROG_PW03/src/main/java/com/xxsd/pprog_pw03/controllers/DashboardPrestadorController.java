package com.xxsd.pprog_pw03.controllers;

import java.io.IOException;

import com.xxsd.pprog_pw03.utils.FxUtils;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class DashboardPrestadorController {

	@FXML
	private Button btnDisponibilidade;

	@FXML
	private Button btnServicos;

	@FXML
	private Label lblTitle;

	@FXML
	private void handleServicos(ActionEvent event) throws IOException {
		FxUtils.switchScene(event, "ServicosUI", "Serviços Disponíveis");
	}

	@FXML
	private void handleDisponibilidade(ActionEvent event) throws IOException {
		FxUtils.switchScene(event, "DisponibilidadeUI", "Adicionar Disponibilidade para Prestação de Serviços");
	}

}
