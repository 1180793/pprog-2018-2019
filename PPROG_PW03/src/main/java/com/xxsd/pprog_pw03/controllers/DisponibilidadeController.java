package com.xxsd.pprog_pw03.controllers;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import com.xxsd.pprog_pw03.autorizacao.models.SessaoUtilizador;
import com.xxsd.pprog_pw03.models.Disponibilidade;
import com.xxsd.pprog_pw03.models.Empresa;
import com.xxsd.pprog_pw03.models.PrestadorServicos;
import com.xxsd.pprog_pw03.models.RegistoPrestadores;
import com.xxsd.pprog_pw03.utils.FxUtils;
import com.xxsd.pprog_pw03.utils.Utils;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;

public class DisponibilidadeController implements Initializable {

	private AplicacaoGPSD oApp = AplicacaoGPSD.getInstance();
	private Empresa oEmpresa = oApp.getEmpresa();
	private SessaoUtilizador oSessao = oApp.getSessaoAtual();
	private String strEmail = oSessao.getEmailUtilizador();
	private RegistoPrestadores oPrestadores = oEmpresa.getRegistoPrestadores();
	private PrestadorServicos oPrestador = oPrestadores.getPrestadorByEmail(strEmail);

	@FXML
	private Label lblTitle;

	@FXML
	private DatePicker dpDataInicio;

	@FXML
	private ChoiceBox<String> cbxHoraInicio;

	@FXML
	private DatePicker dpDataFim;

	@FXML
	private ChoiceBox<String> cbxHoraFim;

	@FXML
	private Button btnAddDisponibilidade;

	@FXML
	private Button btnRemoveDisponibilidade;

	@FXML
	private TableView<Disponibilidade> tableHorario;

	@FXML
	private TableColumn<Disponibilidade, String> colDataInicio;

	@FXML
	private TableColumn<Disponibilidade, String> colHoraInicio;

	@FXML
	private TableColumn<Disponibilidade, String> colDataFim;

	@FXML
	private TableColumn<Disponibilidade, String> colHoraFim;

	@FXML
	private Button btnVoltar;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		populateDisponibilidades();
		listenerDatas();
		populateHoras();
	}

	private void listenerDatas() {
		dpDataInicio.setDayCellFactory(picker -> new DateCell() {
			public void updateItem(LocalDate date, boolean empty) {
				super.updateItem(date, empty);
				LocalDate today = LocalDate.now();
				setDisable(empty || date.compareTo(today) < 0);
			}
		});
		dpDataFim.setDayCellFactory(picker -> new DateCell() {
			public void updateItem(LocalDate date, boolean empty) {
				super.updateItem(date, empty);
				LocalDate today = LocalDate.now();
				setDisable(empty || date.compareTo(today) < 0);
			}
		});
	}

	private void populateHoras() {
		cbxHoraInicio.setItems(FXCollections.observableArrayList(Utils.LIST_OF_TIMES));
		cbxHoraFim.setItems(FXCollections.observableArrayList(Utils.LIST_OF_TIMES));
	}

	@FXML
	private void handleAddDisponibilidade(ActionEvent event) throws ParseException {
		Disponibilidade oDisponibilidade = checkDisponibilidade();
		if (oDisponibilidade != null) {
			if (Utils.validarDisponibilidade(oDisponibilidade, oPrestador.getDisponibilidades())) {
				Utils.openAlert("Disponibilidade Adicionada com Sucesso", "Detalhes da Disponibilidade Adicionado",
						"Data Início: " + oDisponibilidade.getDataInicio() + " ("
								+ Utils.getDiaSemana(oDisponibilidade.getDataInicio()) + ")\nHora Início: "
								+ oDisponibilidade.getHoraInicio() + "h\nData Fim: " + oDisponibilidade.getDataFim()
								+ " (" + Utils.getDiaSemana(oDisponibilidade.getDataFim()) + ")\nHora Fim: "
								+ oDisponibilidade.getHoraFim() + "h",
						AlertType.INFORMATION);
				dpDataInicio.setValue(null);
				cbxHoraInicio.setValue(null);
				dpDataFim.setValue(null);
				cbxHoraFim.setValue(null);
				oPrestador.addDisponibilidade(oDisponibilidade);
				populateDisponibilidades();
			} else {
				Utils.openAlert("Erro ao adicionar Disponibilidade", "Impossível Adicionar Disponibilidade",
						"Disponibilidade Inválida. Existe sobreposição de Horários", AlertType.ERROR);
			}
		}
	}

	private Disponibilidade checkDisponibilidade() {
		if (dpDataInicio.getValue() == null) {
			Utils.openAlert("Erro ao adicionar Disponibilidade", "Impossível Adicionar Disponibilidade",
					"Selecione uma Data de Início", AlertType.ERROR);
			return null;
		}
		if (cbxHoraInicio.getSelectionModel().isEmpty()) {
			Utils.openAlert("Erro ao adicionar Disponibilidade", "Impossível Adicionar Disponibilidade",
					"Selecione uma Hora de Início", AlertType.ERROR);
			return null;
		}
		if (dpDataFim.getValue() == null) {
			Utils.openAlert("Erro ao adicionar Disponibilidade", "Impossível Adicionar Disponibilidade",
					"Selecione uma Data de Fim", AlertType.ERROR);
			return null;
		}
		if (cbxHoraFim.getSelectionModel().isEmpty()) {
			Utils.openAlert("Erro ao adicionar Disponibilidade", "Impossível Adicionar Disponibilidade",
					"Selecione uma Hora de Fim", AlertType.ERROR);
			return null;
		}
		String strDataInicio = dpDataInicio.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String strHoraInicio = cbxHoraInicio.getValue();
		String strDataFim = dpDataFim.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String strHoraFim = cbxHoraFim.getValue();
		if (!Utils.validarData(strDataInicio, strHoraInicio, strDataFim, strHoraFim)) {
			Utils.openAlert("Erro ao adicionar Disponibilidade", "Impossível Adicionar Disponibilidade",
					"Disponibilidade Inválida. Verifique as Datas e Horas de Início e Fim.", AlertType.ERROR);
			return null;
		}
		Disponibilidade oDisponibilidade = new Disponibilidade(strDataInicio, strHoraInicio, strDataFim, strHoraFim);
		return oDisponibilidade;
	}

	@FXML
	private void handleRemoveDisponibilidade(ActionEvent event) {
		Disponibilidade oDisponibilidade = tableHorario.getSelectionModel().getSelectedItem();
		if (oDisponibilidade != null) {
			oPrestador.removeDisponibilidade(oDisponibilidade);
			populateDisponibilidades();
			Utils.openAlert("Disponibilidade Removida", "Disponibilidade Removida com Sucesso.", "A Disponibilidade Selecionada foi Removida com Sucesso.",
					AlertType.INFORMATION);
		}
	}

	@FXML
	private void handleCancelar(ActionEvent event) throws IOException {
		FxUtils.switchScene(event, "DashboardPrestador", "Dashboard - " + oPrestador.getNomeCompleto());
	}

	private void populateDisponibilidades() {
		colDataInicio.setCellValueFactory(new PropertyValueFactory<>("dataInicio"));
		colHoraInicio.setCellValueFactory(new PropertyValueFactory<>("horaInicio"));
		colDataFim.setCellValueFactory(new PropertyValueFactory<>("dataFim"));
		colHoraFim.setCellValueFactory(new PropertyValueFactory<>("horaFim"));
		tableHorario.setItems(FXCollections.observableArrayList(oPrestador.getDisponibilidades()));
	}

}
