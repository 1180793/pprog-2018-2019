package com.xxsd.pprog_pw03.controllers;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.List;
import java.util.ResourceBundle;

import com.xxsd.pprog_pw03.autorizacao.models.SessaoUtilizador;
import com.xxsd.pprog_pw03.autorizacao.models.Utilizador;
import com.xxsd.pprog_pw03.models.AlgoritmoAtribuirPrestador;
import com.xxsd.pprog_pw03.models.Cliente;
import com.xxsd.pprog_pw03.models.DescricaoServicoPedido;
import com.xxsd.pprog_pw03.models.Empresa;
import com.xxsd.pprog_pw03.models.PedidoPrestacaoServico;
import com.xxsd.pprog_pw03.models.PreferenciaHorario;
import com.xxsd.pprog_pw03.models.RegistoPedidos;
import com.xxsd.pprog_pw03.utils.FxUtils;
import com.xxsd.pprog_pw03.utils.Utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;

public class PedidosController implements Initializable {

	private AplicacaoGPSD oApp = AplicacaoGPSD.getInstance();
	private Empresa oEmpresa = oApp.getEmpresa();
	private SessaoUtilizador oSessao = oApp.getSessaoAtual();
	private Utilizador oUtilizador = oSessao.getUtilizador();
	private RegistoPedidos oPedidos = oEmpresa.getRegistoPedidos();

	@FXML
	private Label lblTitle;

	@FXML
	private ChoiceBox<PedidoPrestacaoServico> cbxPedido;

	@FXML
	private TextField txtNome;

	@FXML
	private TextField txtNif;

	@FXML
	private TextField txtTelefone;

	@FXML
	private TextField txtEmail;
	
	@FXML
	private TextField txtEnderecoPostal;

	@FXML
	private TableView<DescricaoServicoPedido> tblPedido;

	@FXML
	private TableColumn<DescricaoServicoPedido, String> colServico;

	@FXML
	private TableColumn<DescricaoServicoPedido, String> colDuracao;

	@FXML
	private TableColumn<DescricaoServicoPedido, String> colDescricao;

	@FXML
	private TableColumn<DescricaoServicoPedido, String> colPrestador;

	@FXML
	private TableView<PreferenciaHorario> tblHorario;

	@FXML
	private TableColumn<PreferenciaHorario, Integer> colOrdem;

	@FXML
	private TableColumn<PreferenciaHorario, String> colData;

	@FXML
	private TableColumn<PreferenciaHorario, String> colHora;

	@FXML
	private TextField txtId;

	@FXML
	private TextField txtServico;

	@FXML
	private TextField txtDuracao;

	@FXML
	private TextField txtCategoria;

	@FXML
	private TextArea txtDescricao;

	@FXML
	private Button btnAtribuirPrestador;

	@FXML
	private Button btnRemoverPrestador;

	@FXML
	private Label lblStatus;

	@FXML
	private Button btnCancelar;

	@FXML
	private Button btnFinalizar;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		populatePedidos();
		listenerPedidos();
		listenerServico();
	}

	private void populatePedidos() {
		List<PedidoPrestacaoServico> lstPedidos = oPedidos.getPedidos();
		cbxPedido.setItems(FXCollections.observableArrayList(lstPedidos));
	}

	private void listenerPedidos() {
		cbxPedido.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (newValue == null) {
				clearAll();
				updateStatus();
			} else {
				populateCliente(newValue);
				populateTables(newValue);
				updateStatus();
			}
		});
	}

	private void listenerServico() {
		tblPedido.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection == null) {
				clearServico();
			} else {
				populateServico(newSelection);
			}
		});
	}

	private void updateStatus() {
		PedidoPrestacaoServico oPedido = cbxPedido.getSelectionModel().getSelectedItem();
		if (oPedido == null) {
			lblStatus.setText("---");
		} else {
			if (oPedido.isReady()) {
				lblStatus.setText("Pronto Para Execução");
			} else {
				lblStatus.setText("A Aguardar Atribuição de Prestadores de Serviços");
			}
		}
	}

	private void populateCliente(PedidoPrestacaoServico oPedido) {
		Cliente oCliente = oPedido.getCliente();
		txtNome.setText(oCliente.getNome());
		txtNif.setText(oCliente.getNif());
		txtTelefone.setText(oCliente.getTelefone());
		txtEmail.setText(oCliente.getEmail());
		txtEnderecoPostal.setText(oPedido.getEnderecoString());
	}

	private void populateTables(PedidoPrestacaoServico oPedido) {
		populatePedidos(oPedido);
		populateHorarios(oPedido);
	}

	private void populatePedidos(PedidoPrestacaoServico oPedido) {
		if (oPedido != null) {
			colServico.setCellValueFactory(new PropertyValueFactory<>("servicoString"));
			colDuracao.setCellValueFactory(new PropertyValueFactory<>("duracao"));
			colDescricao.setCellValueFactory(new PropertyValueFactory<>("descricao"));
			colPrestador.setCellValueFactory(new PropertyValueFactory<>("prestadorString"));
			ObservableList<DescricaoServicoPedido> list = FXCollections
					.observableArrayList(oPedido.getServicosPedidos());
			tblPedido.setItems(list);
		}
	}

	private void populateHorarios(PedidoPrestacaoServico oPedido) {
		if (oPedido != null) {
			colOrdem.setCellValueFactory(new PropertyValueFactory<>("ordem"));
			colData.setCellValueFactory(new PropertyValueFactory<>("data"));
			colHora.setCellValueFactory(new PropertyValueFactory<>("hora"));
			ObservableList<PreferenciaHorario> list = FXCollections.observableArrayList(oPedido.getHorarios());
			tblHorario.setItems(list);
		}
	}

	private void populateServico(DescricaoServicoPedido oDescricao) {
		txtId.setText(oDescricao.getServico().getId());
		txtServico.setText(oDescricao.getServico().getDescricaoBreve());
		txtDuracao.setText(oDescricao.getDuracao());
		txtDescricao.setText(oDescricao.getDescricao());
	}

	private void clearAll() {
		clearCliente();
		clearTables();
		clearServico();
	}

	private void clearCliente() {
		txtNome.clear();
		txtNif.clear();
		txtTelefone.clear();
		txtEmail.clear();
		txtEnderecoPostal.clear();
	}

	private void clearTables() {
		clearPedido();
		clearHorario();
	}

	private void clearPedido() {
		tblPedido.getItems().clear();
	}

	private void clearHorario() {
		tblHorario.getItems().clear();
	}

	private void clearServico() {
		txtId.clear();
		txtServico.clear();
		txtDuracao.clear();
		txtCategoria.clear();
		txtDescricao.clear();
	}

	@FXML
	private void handleAtribuirPrestador(ActionEvent event) {
		PedidoPrestacaoServico oPedido = cbxPedido.getValue();
		if (oPedido != null) {
			DescricaoServicoPedido oDescricaoServicoPedido = tblPedido.getSelectionModel().getSelectedItem();
			AlgoritmoAtribuirPrestador oAlgoritmo;
			try {
				oAlgoritmo = AlgoritmoAtribuirPrestador.atribuirPrestador(oPedido, oDescricaoServicoPedido);
				if (!oAlgoritmo.isValido()) {
					Utils.openAlert("Atribuição de Prestadores de Serviços",
							"Atribuição de Prestadores de Serviços Inválida", oAlgoritmo.getMensagem() + ".",
							AlertType.ERROR);
				} else {
					Utils.openAlert("Atribuição de Prestadores de Serviços",
							"Atribuição de Prestadores de Serviços Efetuada com Sucesso",
							"Prestador de Serviços atribuido aos Pedido com sucesso.", AlertType.INFORMATION);
					updateStatus();
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
	}

	@FXML
	private void handleRemoverPrestador(ActionEvent event) {
		DescricaoServicoPedido oDescricao = tblPedido.getSelectionModel().getSelectedItem();
		if (oDescricao != null) {
			oDescricao.removePrestador();
			cbxPedido.getSelectionModel().getSelectedItem().setReady(false);
			updateStatus();
		}
	}

	@FXML
	private void handleCancelar(ActionEvent event) throws IOException {
		FxUtils.switchScene(event, "DashboardAdministrativo", "Dashboard - " + oUtilizador.getNome());
	}

	@FXML
	private void handleFinalizar(ActionEvent event) {
		PedidoPrestacaoServico oPedido = cbxPedido.getValue();
		if (oPedido != null) {
			if (!oPedido.isReady()) {
				boolean bRet = true;
				for (DescricaoServicoPedido oDescricao : oPedido.getServicosPedidos()) {
					if (oDescricao.getPrestador() == null) {
						bRet = false;
					}
				}
				if (bRet == true) {
					oPedido.setReady(true);
					updateStatus();
					Utils.openAlert("Atribuição de Prestadores de Serviços",
							"Atribuição de Prestadores de Serviços Efetuada com Sucesso",
							"O Pedido de Prestação de Serviços selecionado foi atribuido aos Prestadores com sucesso.",
							AlertType.INFORMATION);
				} else {
					Utils.openAlert("Atribuição de Prestadores de Serviços",
							"Atribuição de Prestadores de Serviços Inválida",
							"O Pedido de Prestação de Serviços selecionado possui Serviços sem Prestadores atribuidos.",
							AlertType.ERROR);
				}
			} else {
				Utils.openAlert("Atribuição de Prestadores de Serviços",
						"Atribuição de Prestadores de Serviços Inválida",
						"O Pedido de Prestação de Serviços selecionado já possui Prestadores atribuidos.",
						AlertType.ERROR);
			}
		} else {
			Utils.openAlert("Atribuição de Prestadores de Serviços", "Atribuição de Prestadores de Serviços Inválida",
					"Verifique se preencheu todos os campos necessários para atribuir os Prestadores.",
					AlertType.ERROR);
		}
	}

}
