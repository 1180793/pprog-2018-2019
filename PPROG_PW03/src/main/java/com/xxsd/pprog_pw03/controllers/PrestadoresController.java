package com.xxsd.pprog_pw03.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.xxsd.pprog_pw03.models.Empresa;
import com.xxsd.pprog_pw03.models.PrestadorServicos;
import com.xxsd.pprog_pw03.models.RegistoPrestadores;
import com.xxsd.pprog_pw03.utils.FxUtils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class PrestadoresController implements Initializable {

	private AplicacaoGPSD oApp = AplicacaoGPSD.getInstance();
	private Empresa oEmpresa = oApp.getEmpresa();
	private RegistoPrestadores oPrestadores = oEmpresa.getRegistoPrestadores();
	
	@FXML
    private Label lblTitle;

    @FXML
    private TableView<PrestadorServicos> tablePrestadores;

    @FXML
    private TableColumn<PrestadorServicos, String> colNumMecanografico;

    @FXML
    private TableColumn<PrestadorServicos, String> colNome;
    
    @FXML
    private TableColumn<PrestadorServicos, String> colNomeAbrev;

    @FXML
    private TableColumn<PrestadorServicos, String> colEmail;
    
    @FXML
    private TableColumn<PrestadorServicos, String> colCategorias;
    
    @FXML
    private TableColumn<PrestadorServicos, String> colAreasGeograficas;

    @FXML
    private Button btnVoltar;

    @FXML
    private void handleCancelar(ActionEvent event) throws IOException {
    	String strPapel = oApp.getPapel();
    	FxUtils.switchDashboard(event, strPapel, oApp.getSessaoAtual().getEmailUtilizador());
    }

    private void populatePrestadores() {
    	colNumMecanografico.setCellValueFactory(new PropertyValueFactory<PrestadorServicos, String>("numeroMecanografico"));
    	colNome.setCellValueFactory(new PropertyValueFactory<PrestadorServicos, String>("nomeCompleto"));
    	colNomeAbrev.setCellValueFactory(new PropertyValueFactory<PrestadorServicos, String>("nomeAbreviado"));
    	colEmail.setCellValueFactory(new PropertyValueFactory<PrestadorServicos, String>("email"));
    	colCategorias.setCellValueFactory(new PropertyValueFactory<PrestadorServicos, String>("categoriasString"));
    	colAreasGeograficas.setCellValueFactory(new PropertyValueFactory<PrestadorServicos, String>("areasGeograficasString"));
		ObservableList<PrestadorServicos> list = FXCollections.observableArrayList(oPrestadores.getPrestadores());
		tablePrestadores.setItems(list);
    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		populatePrestadores();
	}

}
