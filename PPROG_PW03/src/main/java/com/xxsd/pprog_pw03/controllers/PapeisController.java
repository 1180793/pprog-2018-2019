package com.xxsd.pprog_pw03.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.xxsd.pprog_pw03.autorizacao.models.PapelUtilizador;
import com.xxsd.pprog_pw03.autorizacao.models.SessaoUtilizador;
import com.xxsd.pprog_pw03.utils.FxUtils;
import com.xxsd.pprog_pw03.utils.Utils;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;

public class PapeisController implements Initializable {

	private AplicacaoGPSD oApp = AplicacaoGPSD.getInstance();
	private SessaoUtilizador oSessao = oApp.getSessaoAtual();
	private String strEmail = oSessao.getEmailUtilizador();

	@FXML
	private ChoiceBox<PapelUtilizador> cbxPapeis;

	@FXML
	private Button btnEscolher;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		populatePapeis();
	}

	private void populatePapeis() {
		List<PapelUtilizador> lstPapeis = AplicacaoGPSD.getInstance().getSessaoAtual().getPapeisUtilizador();
		cbxPapeis.setItems(FXCollections.observableArrayList(lstPapeis));
	}

	@FXML
	private void handleEscolherPapel(ActionEvent event) throws IOException {
		if (cbxPapeis.getValue() != null) {
			PapelUtilizador oPapel = cbxPapeis.getValue();
			String strPapel = oPapel.getPapel();
			FxUtils.switchDashboard(event, strPapel, strEmail);
			oApp.setPapel(strPapel);
		} else {
			Utils.openAlert("Erro ao Escolher Papel", "Erro ao Escolher Papel",
					"Escolha um Papel para Continuar para a Dashboard", AlertType.ERROR);
		}
	}
}
