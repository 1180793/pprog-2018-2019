package com.xxsd.pprog_pw03.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.xxsd.pprog_pw03.models.Cliente;
import com.xxsd.pprog_pw03.models.Empresa;
import com.xxsd.pprog_pw03.models.RegistoClientes;
import com.xxsd.pprog_pw03.utils.FxUtils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class ClientesController implements Initializable {

	private AplicacaoGPSD oApp = AplicacaoGPSD.getInstance();
	private Empresa oEmpresa = oApp.getEmpresa();
	private RegistoClientes oRegistoClientes = oEmpresa.getRegistoClientes();

	@FXML
	private TableView<Cliente> tableClientes;

	@FXML
	private TableColumn<Cliente, String> colNome;

	@FXML
	private TableColumn<Cliente, String> colNif;

	@FXML
	private TableColumn<Cliente, String> colEmail;

	@FXML
	private TableColumn<Cliente, String> colTelefone;

	@FXML
	private TableColumn<Cliente, String> colMorada;

	@FXML
	private TableColumn<Cliente, String> colPassword;

	@FXML
    private Button btnVoltar;

    @FXML
    private void handleCancelar(ActionEvent event) throws IOException {
    	String strPapel = oApp.getPapel();
    	FxUtils.switchDashboard(event, strPapel, oApp.getSessaoAtual().getEmailUtilizador());
    }

	public void initialize(URL location, ResourceBundle resources) {
		updateClientes();
	}

	private void updateClientes() {
		colNome.setCellValueFactory(new PropertyValueFactory<Cliente, String>("nome"));
		colNif.setCellValueFactory(new PropertyValueFactory<Cliente, String>("nif"));
		colEmail.setCellValueFactory(new PropertyValueFactory<Cliente, String>("email"));
		colTelefone.setCellValueFactory(new PropertyValueFactory<Cliente, String>("telefone"));
		colMorada.setCellValueFactory(new PropertyValueFactory<Cliente, String>("enderecoString"));
		colPassword.setCellValueFactory(new PropertyValueFactory<Cliente, String>("password"));
		ObservableList<Cliente> list = FXCollections.observableArrayList(oRegistoClientes.getClientes());
		tableClientes.setItems(list);
	}

}