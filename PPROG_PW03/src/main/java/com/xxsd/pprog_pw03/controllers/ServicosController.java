package com.xxsd.pprog_pw03.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.xxsd.pprog_pw03.models.Categoria;
import com.xxsd.pprog_pw03.models.Empresa;
import com.xxsd.pprog_pw03.models.RegistoServicos;
import com.xxsd.pprog_pw03.models.Servico;
import com.xxsd.pprog_pw03.models.TipoServico;
import com.xxsd.pprog_pw03.utils.FxUtils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class ServicosController implements Initializable {

	private AplicacaoGPSD oApp = AplicacaoGPSD.getInstance();
	private Empresa oEmpresa = oApp.getEmpresa();
	private RegistoServicos oServicos = oEmpresa.getRegistoServicos();
	
	@FXML
    private Label lblTitle;
	
    @FXML
    private TableView<Servico> tableServicos;

    @FXML
    private TableColumn<Servico, String> colId;

    @FXML
    private TableColumn<Servico, String> colDescCompleta;
    
    @FXML
    private TableColumn<Servico, String> colDescBreve;

    @FXML
    private TableColumn<Servico, String> colCustoHora;

    @FXML
    private TableColumn<Servico, Categoria> colCategoria;
    
    @FXML
    private TableColumn<Servico, TipoServico> colTipoServico;
    
    @FXML
    private Button btnVoltar;

    @FXML
    private void handleCancelar(ActionEvent event) throws IOException {
    	String strPapel = oApp.getPapel();
    	FxUtils.switchDashboard(event, strPapel, oApp.getSessaoAtual().getEmailUtilizador());
    }
    
    private void populateServicos() {
    	colId.setCellValueFactory(new PropertyValueFactory<Servico, String>("id"));
    	colDescCompleta.setCellValueFactory(new PropertyValueFactory<Servico, String>("descricaoCompleta"));
    	colDescBreve.setCellValueFactory(new PropertyValueFactory<Servico, String>("descricaoBreve"));
    	colCustoHora.setCellValueFactory(new PropertyValueFactory<Servico, String>("custoHora"));
    	colCategoria.setCellValueFactory(new PropertyValueFactory<Servico, Categoria>("categoria"));
		colTipoServico.setCellValueFactory(new PropertyValueFactory<Servico, TipoServico>("tipoServico"));
		ObservableList<Servico> list = FXCollections.observableArrayList(oServicos.getServicos());
		tableServicos.setItems(list);
    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		populateServicos();
	}

}