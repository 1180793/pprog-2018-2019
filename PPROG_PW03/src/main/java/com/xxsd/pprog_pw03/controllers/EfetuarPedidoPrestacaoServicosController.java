package com.xxsd.pprog_pw03.controllers;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import com.xxsd.pprog_pw03.models.Categoria;
import com.xxsd.pprog_pw03.models.Cliente;
import com.xxsd.pprog_pw03.models.DescricaoServicoPedido;
import com.xxsd.pprog_pw03.models.Empresa;
import com.xxsd.pprog_pw03.models.EnderecoPostal;
import com.xxsd.pprog_pw03.models.PedidoPrestacaoServico;
import com.xxsd.pprog_pw03.models.PreferenciaHorario;
import com.xxsd.pprog_pw03.models.RegistoCategorias;
import com.xxsd.pprog_pw03.models.RegistoClientes;
import com.xxsd.pprog_pw03.models.RegistoPedidos;
import com.xxsd.pprog_pw03.models.RegistoServicos;
import com.xxsd.pprog_pw03.models.Servico;
import com.xxsd.pprog_pw03.utils.FxUtils;
import com.xxsd.pprog_pw03.utils.Utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class EfetuarPedidoPrestacaoServicosController implements Initializable {

	private AplicacaoGPSD oApp = AplicacaoGPSD.getInstance();
	private Empresa oEmpresa = oApp.getEmpresa();
	private RegistoCategorias oCategorias = oEmpresa.getRegistoCategorias();
	private RegistoClientes oClientes = oEmpresa.getRegistoClientes();
	private RegistoPedidos oPedidos = oEmpresa.getRegistoPedidos();
	private RegistoServicos oServicos = oEmpresa.getRegistoServicos();
	private String strEmail = oApp.getSessaoAtual().getEmailUtilizador();
	private Cliente oCliente = oClientes.getClienteByEmail(strEmail);

	@FXML
	private Label lblTitle;

	@FXML
	private TextField txtNome;

	@FXML
	private TextField txtNif;

	@FXML
	private TextField txtTelefone;

	@FXML
	private TextField txtEmail;

	@FXML
	private ChoiceBox<EnderecoPostal> cbxEndPostal;

	@FXML
	private ChoiceBox<Categoria> cbxCategoria;

	@FXML
	private ChoiceBox<Servico> cbxServico;

	@FXML
	private ChoiceBox<String> cbxDuracao;

	@FXML
	private TextArea txtDescricao;

	@FXML
	private Button btnAddServico;

	@FXML
	private DatePicker dpData;

	@FXML
	private ChoiceBox<String> cbxHora;

	@FXML
	private Button btnAddHorario;

	@FXML
	private TableView<DescricaoServicoPedido> tblPedido;

	@FXML
	private TableColumn<DescricaoServicoPedido, String> colServico;

	@FXML
	private TableColumn<DescricaoServicoPedido, String> colDuracao;

	@FXML
	private TableColumn<DescricaoServicoPedido, String> colDescricao;

	@FXML
	private TableView<PreferenciaHorario> tblHorario;

	@FXML
	private TableColumn<PreferenciaHorario, Integer> colOrdem;

	@FXML
	private TableColumn<PreferenciaHorario, String> colData;

	@FXML
	private TableColumn<PreferenciaHorario, String> colHora;

	@FXML
	private Button btnCancelar;

	private PedidoPrestacaoServico oPedido;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		populateCliente();
		populateEnderecosPostais();
		populateHora();
		listenerEnderecos();
		listenerServicos();
		listenerDuracao();
		listenerData();
	}

	private void populateCliente() {
		txtNome.setText(oCliente.getNome());
		txtNif.setText(oCliente.getNif());
		txtTelefone.setText(oCliente.getTelefone());
		txtEmail.setText(strEmail);
	}

	private void listenerEnderecos() {
		cbxEndPostal.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (newValue == null) {
				cbxCategoria.getItems().clear();
				cbxCategoria.setDisable(true);
				cbxServico.getItems().clear();
				cbxServico.setDisable(true);
				cbxDuracao.getItems().clear();
				cbxDuracao.setDisable(true);
				tblPedido.getItems().clear();
				tblHorario.getItems().clear();
				oPedido = null;
			} else {
				cbxCategoria.getItems().clear();
				cbxCategoria.setDisable(false);
				cbxCategoria.getSelectionModel().clearSelection();
				populateCategoria();
				cbxServico.getItems().clear();
				cbxServico.getSelectionModel().clearSelection();
				cbxServico.setDisable(false);
				cbxDuracao.getItems().clear();
				cbxDuracao.setDisable(false);
				cbxDuracao.getSelectionModel().clearSelection();
				oPedido = new PedidoPrestacaoServico(oCliente, newValue);
			}
		});
	}

	private void listenerServicos() {
		cbxCategoria.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (newValue == null) {
				cbxServico.getItems().clear();
				cbxServico.setDisable(true);
				cbxDuracao.getItems().clear();
				cbxDuracao.setDisable(true);
			} else {
				List<Servico> lstServicos = oServicos.getServicosByCategoria(newValue);
				cbxServico.getItems().setAll(lstServicos);
				cbxServico.getSelectionModel().clearSelection();
				cbxServico.setDisable(false);
				cbxDuracao.getItems().clear();
				cbxDuracao.setDisable(false);
				cbxDuracao.getSelectionModel().clearSelection();
			}
		});
	}

	private void listenerDuracao() {
		cbxServico.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (newValue == null) {
				cbxDuracao.setItems(FXCollections.observableArrayList(Utils.LIST_OF_DURATIONS));
				cbxDuracao.setDisable(true);
			} else if (newValue.possuiOutrosAtributos()) {
				cbxDuracao.setItems(FXCollections.observableArrayList(Utils.LIST_OF_DURATIONS));
				cbxDuracao.setValue(newValue.getOutrosAtributos());
				cbxDuracao.setDisable(true);
			} else {
				cbxDuracao.setItems(FXCollections.observableArrayList(Utils.LIST_OF_DURATIONS));
				cbxDuracao.setDisable(false);
			}
		});
	}

	private void listenerData() {
		dpData.setDayCellFactory(picker -> new DateCell() {
                        @Override
			public void updateItem(LocalDate date, boolean empty) {
				super.updateItem(date, empty);
				LocalDate today = LocalDate.now();
				setDisable(empty || date.compareTo(today) < 0);
			}
		});
	}

	private void populateEnderecosPostais() {
		List<EnderecoPostal> lstEnderecos = oCliente.getEnderecosPostais();
		cbxEndPostal.setItems(FXCollections.observableArrayList(lstEnderecos));
	}

	private void populateCategoria() {
		List<Categoria> lstCategorias = oCategorias.getCategorias();
		cbxCategoria.setItems(FXCollections.observableArrayList(lstCategorias));
	}

	private void populateHora() {
		cbxHora.setItems(FXCollections.observableArrayList(Utils.LIST_OF_DURATIONS));
	}

	private void updatePedidoPreviewTables() {
		if (oPedido != null) {
			colServico.setCellValueFactory(new PropertyValueFactory<>("servicoString"));
			colDuracao.setCellValueFactory(new PropertyValueFactory<>("duracao"));
			colDescricao.setCellValueFactory(new PropertyValueFactory<>("descricao"));
			ObservableList<DescricaoServicoPedido> list = FXCollections
					.observableArrayList(oPedido.getServicosPedidos());
			tblPedido.setItems(list);
		}
	}

	private void updateHorarioPreviewTables() {
		if (oPedido != null) {
			colOrdem.setCellValueFactory(new PropertyValueFactory<>("ordem"));
			colData.setCellValueFactory(new PropertyValueFactory<>("data"));
			colHora.setCellValueFactory(new PropertyValueFactory<>("hora"));
			ObservableList<PreferenciaHorario> list = FXCollections.observableArrayList(oPedido.getHorarios());
			tblHorario.setItems(list);
		}
	}

	@FXML
	private void handleAddServico(ActionEvent event) {
		DescricaoServicoPedido oDescricao = checkServico();
		if (oDescricao != null) {
			oPedido.addPedidoServico(oDescricao);
			updatePedidoPreviewTables();
		}
	}

	private DescricaoServicoPedido checkServico() {
		if (oPedido == null) {
			Utils.openAlert("Erro ao adicionar Serviço ao Pedido", "Impossível Adicionar Serviço ao Pedido",
					"Selecione um Endereço Postal", AlertType.ERROR);
			return null;
		}
		if (cbxServico.getSelectionModel().isEmpty()) {
			Utils.openAlert("Erro ao adicionar Serviço ao Pedido", "Impossível Adicionar Serviço ao Pedido",
					"Selecione um Serviço", AlertType.ERROR);
			return null;
		}
		if (!cbxServico.getSelectionModel().getSelectedItem().possuiOutrosAtributos()) {
			if (cbxDuracao.getSelectionModel().isEmpty()) {
				Utils.openAlert("Erro ao adicionar Serviço ao Pedido", "Impossível Adicionar Serviço ao Pedido",
						"Selecione uma Duração para o Serviço pretendido", AlertType.ERROR);
				return null;
			}
		}
		if (txtDescricao.getText().isEmpty()) {
			Utils.openAlert("Erro ao adicionar Serviço ao Pedido", "Impossível Adicionar Serviço ao Pedido",
					"A Descrição do Pedido tem que ser preenchida", AlertType.ERROR);
			return null;
		}
		Servico oServico = cbxServico.getSelectionModel().getSelectedItem();
		String strDescricao = txtDescricao.getText();
		String strDuracao;
		if (oServico.possuiOutrosAtributos()) {
			strDuracao = oServico.getOutrosAtributos();
		} else {
			strDuracao = cbxDuracao.getSelectionModel().getSelectedItem();
		}
		DescricaoServicoPedido oDescricaoServicoPedido = new DescricaoServicoPedido(oServico, strDescricao, strDuracao);
		Utils.openAlert("Serviço Adicionado ao Pedido com Sucesso", "Detalhes do Serviço Adicionado",
				"Serviço(ID:" + oServico.getId() + ") - " + oServico.getDescricaoBreve() + "\nDuração: " + strDuracao
						+ "h\nCusto Estimado: " + Utils.df.format(oDescricaoServicoPedido.getCusto())
						+ "€\n\n\nOBS: No Custo Estimado não estão incluídos os Custos de Deslocação.",
				AlertType.INFORMATION);
		cbxCategoria.setValue(null);
		cbxServico.setValue(null);
		cbxDuracao.setValue(null);
		txtDescricao.setText(null);
		return oDescricaoServicoPedido;
	}

	@FXML
	private void handleAddHorario(ActionEvent event) throws ParseException {
		PreferenciaHorario oHorario = checkHorario();
		if (oHorario != null) {
			oPedido.addHorario(oHorario);
			updateHorarioPreviewTables();
		}
	}

	private PreferenciaHorario checkHorario() throws ParseException {
		if (oPedido == null) {
			Utils.openAlert("Erro ao adicionar Horário ao Pedido", "Impossível Adicionar Horário ao Pedido",
					"Selecione um Endereço Postal", AlertType.ERROR);
			return null;
		}
		if (dpData.getValue() == null) {
			Utils.openAlert("Erro ao adicionar Horário ao Pedido", "Impossível Adicionar Horário ao Pedido",
					"Selecione uma Data", AlertType.ERROR);
			return null;
		}
		if (cbxHora.getSelectionModel().isEmpty()) {
			Utils.openAlert("Erro ao adicionar Horário ao Pedido", "Impossível Adicionar Horário ao Pedido",
					"Selecione uma Hora", AlertType.ERROR);
			return null;
		}
		String strData = dpData.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String strHora = cbxHora.getValue();
		PreferenciaHorario oHorario = oPedido.createHorario(strData, strHora);
		Utils.openAlert("Horário Adicionado ao Pedido com Sucesso", "Detalhes do Horário Adicionado",
				"Ordem - " + oHorario.getOrdem() + "\nData: " + strData + " (" + Utils.getDiaSemana(strData) + ")"
						+ "\nHora: " + oHorario.getHora() + "h",
				AlertType.INFORMATION);
		dpData.setValue(null);
		cbxHora.setValue(null);
		return oHorario;
	}

	@FXML
	private void handleCancelar(ActionEvent event) throws IOException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Pedido de Prestação de Serviços");
		alert.setHeaderText("Pretende cancelar o Pedido?");
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK) {
			oPedido = null;
			FxUtils.switchScene(event, "DashboardClientes", "Dashboard - " + oCliente.getNome());
		} else {
			// Do nothing
		}
	}

	@FXML
	private void handleFinalizar(ActionEvent event) throws IOException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Pedido de Prestação de Serviços");
		alert.setHeaderText("Pretende Finalizar o Pedido?");
		alert.setContentText("Custo Total: " + oPedido.calculaCusto() + "€");
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK) {
			if (oPedidos.validaPedido(oPedido)) {
				oPedidos.registaPedido(oPedido);
				FxUtils.switchScene(event, "DashBoardClientes", "Dashboard - " + oCliente.getNome());
			} else {
				Utils.openAlert("Pedido de Prestação de Serviços", "Pedido de Prestação de Serviços Inválido",
						"Verifique se preencheu todos os campos necessários para efetuar o Pedido. Em caso de dúvida contacte um administrador.",
						AlertType.ERROR);
			}
		}
	}
}
