package com.xxsd.pprog_pw03.models;

import java.util.ArrayList;
import java.util.List;

import com.xxsd.pprog_pw03.exceptions.DuplicatedIDException;

public class RegistoServicos {

	private List<Servico> m_lstServicos = new ArrayList<>();

	public RegistoServicos() {
	}

	public List<Servico> getServicos() {
		return this.m_lstServicos;
	}

	public Servico getServicoById(String strId) {
		for (Servico serv : this.m_lstServicos) {
			if (serv.hasId(strId)) {
				return serv;
			}
		}
		return null;
	}

	public boolean registaServico(Servico oServico) throws DuplicatedIDException {
		if (this.validaServico(oServico)) {
			return addServico(oServico);
		} else
			throw new DuplicatedIDException("ID de Serviço Duplicado.");
	}

	private boolean addServico(Servico oServico) {
		if (oServico != null)
			return m_lstServicos.add(oServico);
		return false;
	}

	public boolean validaServico(Servico oServico) {
		boolean bRet = true;
		for (Servico serv : this.m_lstServicos) {
			if (serv.getId().equalsIgnoreCase(oServico.getId())) {
				bRet = false;
			}
		}
		return bRet;
	}

	public List<Servico> getServicosByCategoria(String strCategoria) {
		List<Servico> lstServ = new ArrayList<Servico>();
		for (Servico serv : this.m_lstServicos) {
			if (serv.getCategoria().getCodigo().equalsIgnoreCase(strCategoria)) {
				lstServ.add(serv);
			}
		}
		return lstServ;
	}

	public List<Servico> getServicosByCategoria(Categoria oCategoria) {
		String strCategoria = oCategoria.getCodigo();
		return getServicosByCategoria(strCategoria);
	}
}
