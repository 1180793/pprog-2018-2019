package com.xxsd.pprog_pw03.models;

import java.util.ArrayList;
import java.util.List;

import com.xxsd.pprog_pw03.utils.Utils;

import javafx.scene.control.Alert.AlertType;

public class RegistoPedidos {

	private List<PedidoPrestacaoServico> m_lstPedidos = new ArrayList<>();

	public RegistoPedidos() {
	}

	public List<PedidoPrestacaoServico> getPedidos() {
		return this.m_lstPedidos;
	}
	
	public void setPedidos(List<PedidoPrestacaoServico> lstPedidos) {
		this.m_lstPedidos = null;
		this.m_lstPedidos = lstPedidos;
	}

	public PedidoPrestacaoServico novoPedido(Cliente oCliente, EnderecoPostal oMorada) {
		return new PedidoPrestacaoServico(oCliente, oMorada);
	}
	
	public List<PedidoPrestacaoServico> getPedidosByCliente(Cliente oCliente) {
		List<PedidoPrestacaoServico> lstPedidosCliente = new ArrayList<>();
		for (PedidoPrestacaoServico oPedido : this.m_lstPedidos) {
			if (oPedido.hasCliente(oCliente)) {
				lstPedidosCliente.add(oPedido);
			}
		}
		return lstPedidosCliente;
	}

	public int gerarNumeroPedido() {
		return 10000 + m_lstPedidos.size();
	}

	public void registaPedido(PedidoPrestacaoServico oPedido) {
		int num = gerarNumeroPedido();
		oPedido.setNumero(num);
		addPedido(oPedido);
		notificaCliente(oPedido);
	}

	private boolean addPedido(PedidoPrestacaoServico oPedido) {
		return this.m_lstPedidos.add(oPedido);
	}

	private void notificaCliente(PedidoPrestacaoServico oPedido) {
		Utils.openAlert("Pedido de Prestação de Serviços", "Pedido de Prestação de Serviços Nº" + oPedido.getNumero(),
				"O seu Pedido foi registado com sucesso. Em breve receberá um email com todos os detalhes.\n\nCusto Total: "
						+ Utils.df.format(oPedido.calculaCusto()) + "€",
				AlertType.INFORMATION);
	}

	public boolean validaPedido(PedidoPrestacaoServico oPedido) {
		if (oPedido == null) {
			return false;
		} else {
			if (oPedido.getCliente() == null) {
				return false;
			}
			if (oPedido.getEnderecoPostal() == null) {
				return false;
			}
			if (oPedido.getServicosPedidos().isEmpty()) {
				return false;
			}
			if (oPedido.getHorarios().isEmpty()) {
				return false;
			}
			return true;
		}
	}
}
