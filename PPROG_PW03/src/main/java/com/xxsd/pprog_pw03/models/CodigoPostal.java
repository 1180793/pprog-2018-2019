package com.xxsd.pprog_pw03.models;

import java.io.Serializable;
import java.util.Objects;

public class CodigoPostal implements Serializable {

	private static final long serialVersionUID = 54325252345L;

	private String m_strCodPostal;

	private double m_dblLatitude;

	private double m_dlbLongitude;

	public CodigoPostal(String strCodPostal, double dblLatitude, double dblLongitude) {
		this.m_strCodPostal = strCodPostal;
		this.m_dblLatitude = dblLatitude;
		this.m_dlbLongitude = dblLongitude;
	}

	public String getCodigoPostalString() {
		return this.m_strCodPostal;
	}

	public double getLatitude() {
		return this.m_dblLatitude;
	}

	public double getLongitude() {
		return this.m_dlbLongitude;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (getClass() != o.getClass())
			return false;
		CodigoPostal obj = (CodigoPostal) o;
		return (Objects.equals(m_strCodPostal, obj.m_strCodPostal) && Objects.equals(m_dblLatitude, obj.m_dblLatitude)
				&& Objects.equals(m_dlbLongitude, obj.m_dlbLongitude));
	}

	@Override
	public String toString() {
		return String.format("%s \n %s | %s", this.m_strCodPostal, this.m_dblLatitude, this.m_dlbLongitude);
	}
}
