package com.xxsd.pprog_pw03.models;

import java.io.Serializable;

public class OutroCusto implements Serializable {

	private static final long serialVersionUID = 5434889675821802258L;

	private String m_strDesignacao;
	private double m_dblCusto;

	public OutroCusto(String strDesignacao, double dblCusto) {
		this.m_strDesignacao = strDesignacao;
		this.m_dblCusto = dblCusto;
	}

	public String getDesignacao() {
		return this.m_strDesignacao;
	}

	public double getCusto() {
		return this.m_dblCusto;
	}

	@Override
	public String toString() {
		return String.format("%s - %.2f", this.m_strDesignacao, this.m_dblCusto);
	}
}
