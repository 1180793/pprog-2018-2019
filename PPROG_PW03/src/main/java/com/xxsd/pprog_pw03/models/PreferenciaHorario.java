package com.xxsd.pprog_pw03.models;

import java.io.Serializable;
import java.text.ParseException;

import com.xxsd.pprog_pw03.utils.Utils;

public class PreferenciaHorario implements Serializable {

	private static final long serialVersionUID = 5068131455821802258L;
	
	private int m_intOrdem;
	private String m_strData;
	private String m_strHora;

	public PreferenciaHorario(int intOrdem, String strData, String strHora) {
		this.m_intOrdem = intOrdem;
		this.m_strData = strData;
		this.m_strHora = strHora;
	}

	public int getOrdem() {
		return m_intOrdem + 1;
	}

	public String getData() throws ParseException {
		return m_strData + " (" + Utils.getDiaSemana(m_strData) + ")";
	}
	
	public String getDataString() {
		return m_strData;
	}

	public String getHora() {
		return m_strHora;
	}

	public String toString() {
		return String.format("%d | %s %s", this.m_intOrdem, this.m_strData, this.m_strHora);
	}
}
