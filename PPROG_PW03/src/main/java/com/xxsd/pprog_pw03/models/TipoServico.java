package com.xxsd.pprog_pw03.models;

public enum TipoServico {

	FIXO("Fixo"), LIMITADO("Limitado"), EXPANSIVEL("Expansivel");

	private String m_strDesignacao;

	private TipoServico(String strDesignacao) {
		this.m_strDesignacao = strDesignacao;
	}

	public Servico novoServico(String strId, String strDescBreve, String strDescComp, double dCusto,
			Categoria oCategoria) {
		if (this == TipoServico.FIXO) {
			return new ServicoFixo(strId, strDescBreve, strDescComp, dCusto, oCategoria);
		}
		if (this == TipoServico.LIMITADO) {
			return new ServicoLimitado(strId, strDescBreve, strDescComp, dCusto, oCategoria);
		}
		if (this == TipoServico.EXPANSIVEL) {
			return new ServicoExpansivel(strId, strDescBreve, strDescComp, dCusto, oCategoria);
		}
		return null;
	}

	public String getId() {
		return this.m_strDesignacao;
	}

	public String toString() {
		return String.format("%s", this.m_strDesignacao);
	}
}
