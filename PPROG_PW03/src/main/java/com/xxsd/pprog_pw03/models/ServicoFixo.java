package com.xxsd.pprog_pw03.models;

import java.util.Objects;

import com.xxsd.pprog_pw03.utils.Utils;

public class ServicoFixo implements Servico {

	private static final long serialVersionUID = 53453533259629L;

	private String m_strId;
	private String m_strDescricaoBreve;
	private String m_strDescricaoCompleta;
	private double m_dCustoHora;
	private String m_strDuracaoPreDefinida;
	private Categoria m_oCategoria;

	public ServicoFixo(String strId, String strDescricaoBreve, String strDescricaoCompleta, double dCustoHora,
			Categoria oCategoria) {
		if ((strId == null) || (strDescricaoBreve == null) || (strDescricaoCompleta == null) || (dCustoHora < 0)
				|| (oCategoria == null) || (strId.isEmpty()) || (strDescricaoBreve.isEmpty())
				|| (strDescricaoCompleta.isEmpty()))
			throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");

		this.m_strId = strId;
		this.m_strDescricaoBreve = strDescricaoBreve;
		this.m_strDescricaoCompleta = strDescricaoCompleta;
		this.m_dCustoHora = dCustoHora;
		this.m_oCategoria = oCategoria;
	}

	public String getId() {
		return this.m_strId;
	}

	public String getDescricaoBreve() {
		return this.m_strDescricaoBreve;
	}

	public String getDescricaoCompleta() {
		return this.m_strDescricaoCompleta;
	}
	
	public TipoServico getTipoServico() {
		return TipoServico.FIXO;
	}

	public Categoria getCategoria() {
		return this.m_oCategoria;
	}

	public boolean hasId(String strId) {
		return this.m_strId.equalsIgnoreCase(strId);
	}

	@Override
	public boolean possuiOutrosAtributos() {
		return true;
	}

	@Override
	public String getOutrosAtributos() {
		return this.m_strDuracaoPreDefinida;
	}

	@Override
	public void setOutrosAtributos(String strOutrosAtributos) {
		this.m_strDuracaoPreDefinida = strOutrosAtributos;
	}

	@Override
	public double getCustoParaDuracao(String strDuracao) {
		return m_dCustoHora * Utils.convertDuracao(m_strDuracaoPreDefinida);
	}
	
	public String getCustoHora() {
		return this.m_dCustoHora + "€";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (getClass() != o.getClass())
			return false;
		ServicoFixo obj = (ServicoFixo) o;
		return (Objects.equals(m_strId, obj.m_strId));
	}

	@Override
	public String toString() {
		return String.format("%s - %s | %.2f€", this.m_strId, this.m_strDescricaoCompleta, this.m_dCustoHora);
	}
}
