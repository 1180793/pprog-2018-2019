package com.xxsd.pprog_pw03.models;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class RegistoTiposServico {

	private List<TipoServico> m_lstTiposServico;

	public RegistoTiposServico() {
		this.m_lstTiposServico = new ArrayList<TipoServico>(EnumSet.allOf(TipoServico.class));
	}

	public TipoServico getTipoServicoById(String strId) {
		for (TipoServico tipo : this.m_lstTiposServico) {
			if (tipo.getId().equalsIgnoreCase(strId)) {
				return tipo;
			}
		}
		return null;
	}

	public List<TipoServico> getTiposServico() {
		return this.m_lstTiposServico;
	}
}
