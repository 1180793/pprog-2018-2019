package com.xxsd.pprog_pw03.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import com.xxsd.pprog_pw03.utils.Utils;

public class PrestadorServicos implements Serializable {

	private static final long serialVersionUID = 5068131455821802348L;

	private String m_strNumMecanografico;
	private String m_strNomeComp;
	private String m_strNomeAbrev;
	private String m_strEmail;
	private List<Categoria> m_lstCategorias = new ArrayList<Categoria>();
	private List<AreaGeografica> m_lstAreasGeograficas = new ArrayList<AreaGeografica>();
	private List<Disponibilidade> m_lstDisponibilidades = new ArrayList<Disponibilidade>();

	public PrestadorServicos(String strNumMecanografico, String strNomeComp, String strNomeAbrev, String strEmail,
			ArrayList<Categoria> lstCategorias, ArrayList<AreaGeografica> lstAreasGeograficas) {
		if ((strNumMecanografico == null) || (strNomeComp == null) || (strNomeAbrev == null) || (strEmail == null)
				|| (lstCategorias == null) || (lstAreasGeograficas == null) || (strNumMecanografico.isEmpty())
				|| (strNomeComp.isEmpty()) || (strNomeAbrev.isEmpty()) || (strEmail.isEmpty()))
			throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");

		this.m_strNumMecanografico = strNumMecanografico;
		this.m_strEmail = strEmail;
		this.m_strNomeComp = strNomeComp;
		this.m_strNomeAbrev = strNomeAbrev;
		for (Categoria cat : lstCategorias) {
			this.m_lstCategorias.add(cat);
		}
		for (AreaGeografica aGeo : lstAreasGeograficas) {
			this.m_lstAreasGeograficas.add(aGeo);
		}
	}

	public String getNumeroMecanografico() {
		return this.m_strNumMecanografico;
	}

	public String getNomeCompleto() {
		return this.m_strNomeComp;
	}

	public String getNomeAbreviado() {
		return this.m_strNomeAbrev;
	}

	public String getEmail() {
		return this.m_strEmail;
	}

	public List<Categoria> getCategorias() {
		return this.m_lstCategorias;
	}

	public List<AreaGeografica> getAreasGeograficas() {
		return this.m_lstAreasGeograficas;
	}

	public String getCategoriasString() {
		StringBuilder sbReturn = new StringBuilder();
		String strPrefix = "";
		for (Categoria oCat : this.m_lstCategorias) {
			sbReturn.append(strPrefix);
			strPrefix = "; ";
			sbReturn.append(oCat.getCodigo());
		}
		return sbReturn.toString();
	}

	public String getAreasGeograficasString() {
		StringBuilder sbReturn = new StringBuilder();
		String strPrefix = "";
		for (AreaGeografica oAGeo : this.m_lstAreasGeograficas) {
			sbReturn.append(strPrefix);
			strPrefix = "; ";
			sbReturn.append(oAGeo.getDesignacao());
		}
		return sbReturn.toString();
	}

	public List<Disponibilidade> getDisponibilidades() {
		Utils.ordenarDisponibilidades(this.m_lstDisponibilidades);
		return this.m_lstDisponibilidades;
	}

	public boolean hasEmail(String strEmail) {
		return this.m_strEmail.equalsIgnoreCase(strEmail);
	}

	public boolean hasNumMecanografico(String strNumMecanografico) {
		return this.m_strNumMecanografico.equalsIgnoreCase(strNumMecanografico);
	}

	public boolean addCategoria(Categoria oCategoria) {
		if (this.validaCategoria(oCategoria))
			return this.m_lstCategorias.add(oCategoria);
		return false;
	}

	public boolean addAreaGeografica(AreaGeografica oAreaGeografica) {
		if (this.validaAreaGeografica(oAreaGeografica))
			return this.m_lstAreasGeograficas.add(oAreaGeografica);
		return false;
	}

	public boolean addDisponibilidade(Disponibilidade oDisponibilidade) {
		if (this.validaDisponibilidade(oDisponibilidade)) {
			return this.m_lstDisponibilidades.add(oDisponibilidade);
		}
		return false;
	}

	public boolean removeCategoria(Categoria oCategoria) {
		return this.m_lstCategorias.remove(oCategoria);
	}

	public boolean removeAreaGeografica(AreaGeografica oAreaGeografica) {
		return this.m_lstAreasGeograficas.remove(oAreaGeografica);
	}

	public boolean removeDisponibilidade(Disponibilidade oDisponibilidade) {
		return this.m_lstDisponibilidades.remove(oDisponibilidade);
	}

	public boolean validaCategoria(Categoria oCategoria) {
		boolean bRet = true;
		for (Categoria cat : m_lstCategorias) {
			if (oCategoria.equals(cat)) {
				bRet = false;
			}
		}
		return bRet;
	}

	public boolean validaAreaGeografica(AreaGeografica oAreaGeografica) {
		boolean bRet = true;
		for (AreaGeografica aGeo : m_lstAreasGeograficas) {
			if (oAreaGeografica.equals(aGeo)) {
				bRet = false;
			}
		}
		return bRet;
	}

	public boolean validaDisponibilidade(Disponibilidade oDisponibilidade) {
		boolean bRet = true;
		for (Disponibilidade disp : m_lstDisponibilidades) {
			if (oDisponibilidade.equals(disp)) {
				bRet = false;
			}
		}
		return bRet;
	}

	public boolean atuaEm(AreaGeografica aGeo) {
		if (this.m_lstAreasGeograficas.contains(aGeo)) {
			return true;
		}
		return false;
	}

	public boolean isDisponivel(String strData, String strHora, String strDuracao) {
		String[] sData = strData.split("-");
		String[] sHora = strHora.split(":");
		int[] iData = new int[3];
		int[] iHora = new int[2];
		for (int i = 0; i < sData.length; i++) {
			iData[i] = Integer.parseInt(sData[i]);
		}
		for (int i = 0; i < sHora.length; i++) {
			iHora[i] = Integer.parseInt(sHora[i]);
		}
		Date dataInicial = new Date(iData[0], iData[1], iData[2], iHora[0], iHora[1]);
		Date dataFinal = Utils.somarTempo(strData, strHora, strDuracao);
		for (Disponibilidade oDisponibilidade : this.m_lstDisponibilidades) {
			String[] sDataInicio = oDisponibilidade.getDataInicio().split("-");
			String[] sHoraInicio = oDisponibilidade.getHoraInicio().split(":");
			String[] sDataFim = oDisponibilidade.getDataFim().split("-");
			String[] sHoraFim = oDisponibilidade.getHoraFim().split(":");
			Date dataInicial2 = new Date(Integer.parseInt(sDataInicio[0]), Integer.parseInt(sDataInicio[1]),
					Integer.parseInt(sDataInicio[2]), Integer.parseInt(sHoraInicio[0]),
					Integer.parseInt(sHoraInicio[1]));
			Date dataFinal2 = new Date(Integer.parseInt(sDataFim[0]), Integer.parseInt(sDataFim[1]),
					Integer.parseInt(sDataFim[2]), Integer.parseInt(sHoraFim[0]), Integer.parseInt(sHoraFim[1]));
			if (dataInicial.after(dataInicial2)) {
				if (dataFinal.equals(dataFinal2)) {
					return true;
				}
				if (dataFinal.before(dataFinal2)) {
					return true;
				}
			}
			if (dataInicial.equals(dataInicial2)) {
				if (dataFinal.equals(dataFinal2)) {
					return true;
				}
				if (dataFinal.before(dataFinal2)) {
					return true;
				}
			}

		}
		return false;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (getClass() != o.getClass())
			return false;
		PrestadorServicos obj = (PrestadorServicos) o;
		return (Objects.equals(m_strEmail, obj.m_strEmail)
				|| Objects.equals(m_strNumMecanografico, obj.m_strNumMecanografico));
	}

	@Override
	public String toString() {
		String str = String.format("%s - %s - %s - %s", this.m_strNumMecanografico, this.m_strNomeComp,
				this.m_strNomeAbrev, this.m_strEmail);
		for (Categoria cat : this.m_lstCategorias)
			str += "\n" + cat.toString();
		for (AreaGeografica aGeo : this.m_lstAreasGeograficas)
			str += "\nÁrea Geográfica:\n" + aGeo.toString();
		return str;
	}

}