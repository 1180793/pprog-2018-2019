package com.xxsd.pprog_pw03.models;

import java.io.Serializable;

public class DescricaoServicoPedido implements Serializable {

	private static final long serialVersionUID = 50532523522258L;

	private Servico m_oServico;
	private String m_strDescricao;
	private String m_strDuracao;
	private PrestadorServicos m_oPrestador;

	public DescricaoServicoPedido(Servico oServico, String strDescricao, String strDuracao) {
		this.m_oServico = oServico;
		this.m_strDescricao = strDescricao;
		this.m_strDuracao = strDuracao;
		this.m_oPrestador = null;
	}
	
	public PrestadorServicos getPrestador() {
		return this.m_oPrestador;
	}
	
	public String getPrestadorString() {
		if (this.getPrestador() == null) {
			return "Não Atribuido";
		} else {
			return this.m_oPrestador.getNomeAbreviado();
		}
	}

	public Servico getServico() {
		return this.m_oServico;
	}

	public String getServicoString() {
		return "ID: " + this.m_oServico.getId();
	}

	public String getDescricao() {
		return this.m_strDescricao;
	}

	public String getDuracao() {
		return this.m_strDuracao;
	}

	public double getCusto() {
		return this.m_oServico.getCustoParaDuracao(m_strDuracao);
	}
	
	public void associarPrestador(PrestadorServicos oPrestador) {
		this.m_oPrestador = oPrestador;
	}
	
	public void removePrestador() {
		if (this.m_oPrestador != null) {
			this.m_oPrestador = null;
		}
	}
	
}
