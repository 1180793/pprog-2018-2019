package com.xxsd.pprog_pw03.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.xxsd.pprog_pw03.controllers.AplicacaoGPSD;

public class PedidoPrestacaoServico implements Serializable {

	private static final long serialVersionUID = 282587463583259629L;

	private int m_intNumero;
	private Cliente m_oCliente;
	private EnderecoPostal m_oMorada;
	private List<DescricaoServicoPedido> m_lstPedidoServicos;
	private List<PreferenciaHorario> m_lstHorarios;
	private List<OutroCusto> m_lstOutrosCustos = new ArrayList<>();
	private boolean m_bIsReady;

	public PedidoPrestacaoServico(Cliente oCliente, EnderecoPostal oMorada) {
		this.m_oCliente = oCliente;
		this.m_oMorada = oMorada;
		this.m_lstPedidoServicos = new ArrayList<>();
		this.m_lstHorarios = new ArrayList<>();
		this.m_bIsReady = false;
	}

	public void setReady(boolean bReady) {
		this.m_bIsReady = bReady;
	}

	public boolean isReady() {
		return this.m_bIsReady;
	}

	public boolean addPedidoServico(Servico oServico, String strDescricao, String strDuracao) {
		DescricaoServicoPedido oServicoPedido = new DescricaoServicoPedido(oServico, strDescricao, strDuracao);
		if (validaPedidoServico(oServicoPedido)) {
			return this.m_lstPedidoServicos.add(oServicoPedido);
		}
		return false;
	}

	public boolean addPedidoServico(DescricaoServicoPedido oServicoPedido) {
		if (validaPedidoServico(oServicoPedido)) {
			return this.m_lstPedidoServicos.add(oServicoPedido);
		}
		return false;
	}

	private boolean validaPedidoServico(DescricaoServicoPedido oServicoPedido) {
		boolean bRet = true;
		for (DescricaoServicoPedido pedidos : this.m_lstPedidoServicos) {
			if (pedidos.getServico().equals(oServicoPedido.getServico())) {
				bRet = false;
			}
		}
		return bRet;
	}

	public PreferenciaHorario createHorario(String strData, String strHora) {
		int intOrdem = countHorarios();
		PreferenciaHorario oHorario = new PreferenciaHorario(intOrdem, strData, strHora);
		return oHorario;
	}

	public boolean addHorario(String strData, String strHora) {
		int intOrdem = countHorarios();
		PreferenciaHorario oHorario = new PreferenciaHorario(intOrdem, strData, strHora);
		return this.m_lstHorarios.add(oHorario);
	}

	public boolean addHorario(PreferenciaHorario oHorario) {
		return this.m_lstHorarios.add(oHorario);
	}

	private int countHorarios() {
		return this.m_lstHorarios.size();
	}

	public Cliente getCliente() {
		return this.m_oCliente;
	}

	public EnderecoPostal getEnderecoPostal() {
		return this.m_oMorada;
	}

	public String getStatus() {
		if (this.m_bIsReady) {
			return "Pronto Para Execução";
		} else {
			return "A Aguardar Atribuição de Prestadores de Serviços";
		}
	}

	public double calculaCusto() {
		double dblCusto = 0;
		for (DescricaoServicoPedido pedidos : this.m_lstPedidoServicos) {
			dblCusto = dblCusto + pedidos.getCusto();
		}
		RegistoAreasGeograficas oAreasGeograficas = AplicacaoGPSD.getInstance().getEmpresa()
				.getRegistoAreasGeograficas();
		CodigoPostal oCodigoPostal = this.m_oMorada.getCodigoPostal();
		AreaGeografica aGeo = oAreasGeograficas.getAreaGeograficaMaisPerto(oCodigoPostal);
		double custoDeslocacao = aGeo.getCustoDeslocacao();
		OutroCusto outroCusto = new OutroCusto("Custo de Deslocação", custoDeslocacao);
		this.m_lstOutrosCustos.add(outroCusto);
		for (OutroCusto custos : this.m_lstOutrosCustos) {
			dblCusto = dblCusto + custos.getCusto();
		}
		return dblCusto;
	}

	public int getNumero() {
		return this.m_intNumero;
	}

	public String getEnderecoString() {
		return this.m_oMorada.toString();
	}

	public List<DescricaoServicoPedido> getServicosPedidos() {
		return this.m_lstPedidoServicos;
	}

	public List<PreferenciaHorario> getHorarios() {
		return this.m_lstHorarios;
	}

	public void setNumero(int intNumero) {
		this.m_intNumero = intNumero;
	}

	public boolean hasCliente(Cliente oCliente) {
		if (oCliente.getEmail().equals(this.m_oCliente.getEmail())) {
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "" + this.m_intNumero;
	}

}
