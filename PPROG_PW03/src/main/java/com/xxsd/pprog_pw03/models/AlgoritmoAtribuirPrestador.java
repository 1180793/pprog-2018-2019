package com.xxsd.pprog_pw03.models;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.xxsd.pprog_pw03.controllers.AplicacaoGPSD;

public class AlgoritmoAtribuirPrestador {
	
	private boolean m_bValido;
	private String m_strMensagem;
	
	public AlgoritmoAtribuirPrestador(boolean bValido, String strMensagem) {
		this.m_bValido = bValido;
		this.m_strMensagem = strMensagem;
	}
	
	public String getMensagem() {
		return this.m_strMensagem;
	}
	
	public boolean isValido() {
		return this.m_bValido;
	}

	public static AlgoritmoAtribuirPrestador atribuirPrestador(PedidoPrestacaoServico oPedido, DescricaoServicoPedido oDescricaoServicoPedido) throws ParseException {
		EnderecoPostal oEndPostal = oPedido.getEnderecoPostal();
		List<AreaGeografica> lstAreas = new ArrayList<>();
		List<PrestadorServicos> lstPrestadores = new ArrayList<>();
		for (AreaGeografica aGeo : AplicacaoGPSD.getInstance().getEmpresa().getRegistoAreasGeograficas().getAreasGeograficas()) {
			if (aGeo.atuaEm(oEndPostal)) {
				lstAreas.add(aGeo);
				System.out.println("1");
			}
		}
		if (lstAreas.isEmpty()) {
			System.out.println("2");
			return new AlgoritmoAtribuirPrestador(false, "Não existem Áreas Geográficas para o Endereço Postal especificado no Pedido");
		}
		for (PrestadorServicos oPrestador : AplicacaoGPSD.getInstance().getEmpresa().getRegistoPrestadores().getPrestadores()) {
			for (AreaGeografica aGeo : lstAreas) {
				if (oPrestador.atuaEm(aGeo)) {
					System.out.println("3");
					lstPrestadores.add(oPrestador);
				}
			}
		}
		if (lstPrestadores.isEmpty()) {
			System.out.println("4");
			return new AlgoritmoAtribuirPrestador(false, "Não existem Prestadores de Serviços para o Endereço Postal especificado no Pedido");
		}
		for (PrestadorServicos oPrestador : lstPrestadores) {
			System.out.println("5");
			for (PreferenciaHorario oHorario : oPedido.getHorarios()) {
				System.out.println("6");
				if (oPrestador.isDisponivel(oHorario.getDataString(), oHorario.getHora(), oDescricaoServicoPedido.getDuracao())) {
					oDescricaoServicoPedido.associarPrestador(oPrestador);
					return new AlgoritmoAtribuirPrestador(true, "Prestador Associado com Sucesso");
				}
			}
		}
		return new AlgoritmoAtribuirPrestador(false, "Não existem Prestadores de Serviços com Disponibilidades compatíveis para prestar o Serviço.");
	}

}
