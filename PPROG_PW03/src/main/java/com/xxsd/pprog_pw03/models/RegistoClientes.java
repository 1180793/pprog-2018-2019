package com.xxsd.pprog_pw03.models;

import java.util.ArrayList;
import java.util.List;

import com.xxsd.pprog_pw03.autorizacao.AutorizacaoFacade;
import com.xxsd.pprog_pw03.exceptions.DuplicatedIDException;

public class RegistoClientes {

	private AutorizacaoFacade m_oAutorizacao;
	private List<Cliente> m_lstClientes = new ArrayList<>();

	public RegistoClientes(AutorizacaoFacade at) {
		this.m_oAutorizacao = at;
	}

	public List<Cliente> getClientes() {
		return this.m_lstClientes;
	}

	public Cliente novoCliente(String strNome, String strNIF, String strTelefone, String strEmail,
			EnderecoPostal morada) {
		return new Cliente(strNome, strNIF, strTelefone, strEmail, morada);
	}

	public boolean addCliente(Cliente oCliente) {
		if (oCliente != null)
			return m_lstClientes.add(oCliente);
		return false;
	}

	public Cliente getClienteByEmail(String strEmail) {
		for (Cliente cliente : this.m_lstClientes) {
			if (cliente.hasEmail(strEmail)) {
				return cliente;
			}
		}
		return null;
	}

	public boolean registaCliente(Cliente oCliente, String strPwd) throws DuplicatedIDException {
		if (this.validaCliente(oCliente, strPwd)) {
			if (this.m_oAutorizacao.registaUtilizadorComPapel(oCliente.getNome(), oCliente.getEmail(), strPwd,
					Constantes.PAPEL_CLIENTE)) {
				return addCliente(oCliente);
			}
		} else {
			throw new DuplicatedIDException("Email e/ou NIF já registados.");
		}
		return false;
	}

	public boolean validaCliente(Cliente oCliente, String strPwd) {
		boolean bRet = true;
		if (strPwd == null) {
			bRet = false;
		}
		if (strPwd.isEmpty()) {
			bRet = false;
		}
		for (Cliente cli : this.m_lstClientes) {
			if (cli.getEmail().equalsIgnoreCase(oCliente.getEmail())) {
				bRet = false;
			}
			if (cli.getNif().equalsIgnoreCase(oCliente.getNif())) {
				bRet = false;
			}
		}
		if (this.m_oAutorizacao.existeUtilizador(oCliente.getEmail())) {
			bRet = false;
		}
		return bRet;
	}

}
