package com.xxsd.pprog_pw03.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.xxsd.pprog_pw03.controllers.AplicacaoGPSD;

public class Cliente implements Serializable {

	private static final long serialVersionUID = 53456734521802258L;

	private String m_strNome;
	private String m_strNIF;
	private String m_strTelefone;
	private String m_strEmail;
	private List<EnderecoPostal> m_lstMoradas = new ArrayList<EnderecoPostal>();

	public Cliente(String strNome, String strNIF, String strTelefone, String strEmail, EnderecoPostal oMorada) {
		if ((strNome == null) || (strNIF == null) || (strTelefone == null) || (strEmail == null) || (strNome.isEmpty())
				|| (strNIF.isEmpty()) || (strTelefone.isEmpty()) || (strEmail.isEmpty()))
			throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");

		this.m_strNome = strNome;
		this.m_strEmail = strEmail;
		this.m_strNIF = strNIF;
		this.m_strTelefone = strTelefone;
		m_lstMoradas.add(oMorada);
	}

	public String getNome() {
		return this.m_strNome;
	}

	public String getEmail() {
		return this.m_strEmail;
	}

	public String getNif() {
		return this.m_strNIF;
	}

	public String getTelefone() {
		return this.m_strTelefone;
	}

	public boolean hasEmail(String strEmail) {
		return this.m_strEmail.equalsIgnoreCase(strEmail);
	}

	public boolean hasNif(String strNIF) {
		return this.m_strNIF.equalsIgnoreCase(strNIF);
	}

	public List<EnderecoPostal> getEnderecosPostais() {
		return this.m_lstMoradas;
	}

	public String getPassword() {
		return AplicacaoGPSD.getInstance().getEmpresa().getAutorizacaoFacade().getRegistoUtilizadores()
				.procuraUtilizador(this.m_strEmail).getPasswordEncriptada();
	}

	public String getEnderecoString() {
		EnderecoPostal oEnderecoPostal = this.m_lstMoradas.get(0);
		if (oEnderecoPostal != null)
			return oEnderecoPostal.toString();
		return null;
	}

	public boolean addEnderecoPostal(EnderecoPostal oEnderecoPostal) {
		if (validaEnderecoPostal(oEnderecoPostal))
			return addEPostal(oEnderecoPostal);
		return false;
	}

	private boolean validaEnderecoPostal(EnderecoPostal oEnderecoPostal) {
		boolean bRet = true;
		for (EnderecoPostal endPostal : m_lstMoradas) {
			if (oEnderecoPostal.equals(endPostal)) {
				bRet = false;
			}
		}
		return bRet;
	}

	private boolean addEPostal(EnderecoPostal oEnderecoPostal) {
		return this.m_lstMoradas.add(oEnderecoPostal);
	}

	public boolean removeEnderecoPostal(EnderecoPostal oMorada) {
		return this.m_lstMoradas.remove(oMorada);
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 23 * hash + Objects.hashCode(this.m_strEmail);
		return hash;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (getClass() != o.getClass())
			return false;
		Cliente obj = (Cliente) o;
		return (Objects.equals(m_strEmail, obj.m_strEmail) || Objects.equals(m_strNIF, obj.m_strNIF));
	}

	@Override
	public String toString() {
		String str = String.format("%s - %s - %s - %s", this.m_strNome, this.m_strNIF, this.m_strTelefone,
				this.m_strEmail);
		for (EnderecoPostal morada : this.m_lstMoradas)
			str += "\nMorada:\n" + morada.toString();
		return str;
	}

	public static EnderecoPostal novoEnderecoPostal(String strLocal, CodigoPostal oCodPostal, String strLocalidade) {
		return new EnderecoPostal(strLocal, oCodPostal, strLocalidade);
	}

}
