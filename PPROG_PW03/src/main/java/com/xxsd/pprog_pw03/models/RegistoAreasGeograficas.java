package com.xxsd.pprog_pw03.models;

import java.util.ArrayList;
import java.util.List;

import com.xxsd.pprog_pw03.exceptions.DuplicatedIDException;
import com.xxsd.pprog_pw03.utils.Utils;

public class RegistoAreasGeograficas {

	private List<AreaGeografica> m_lstAreasGeograficas = new ArrayList<>();

	public RegistoAreasGeograficas() {
	}

	public List<AreaGeografica> getAreasGeograficas() {
		return this.m_lstAreasGeograficas;
	}

	public AreaGeografica getAreaGeograficaById(String strId) {
		for (AreaGeografica aGeo : this.m_lstAreasGeograficas) {
			if (aGeo.getCodigoPostal().getCodigoPostalString().equalsIgnoreCase(strId)) {
				return aGeo;
			}
		}
		return null;
	}

	public boolean registaAreaGeografica(AreaGeografica oAreaGeografica) throws DuplicatedIDException {
		if (this.validaAreaGeografica(oAreaGeografica)) {
			return addAreaGeografica(oAreaGeografica);
		} else
			throw new DuplicatedIDException("Área Geográfica Duplicada.");
	}

	private boolean addAreaGeografica(AreaGeografica oAreaGeografica) {
		return m_lstAreasGeograficas.add(oAreaGeografica);
	}

	public boolean validaAreaGeografica(AreaGeografica oAreaGeografica) {
		boolean bRet = true;
		for (AreaGeografica aGeo : this.m_lstAreasGeograficas) {
			if (aGeo.getCodigoPostal().getCodigoPostalString()
					.equalsIgnoreCase(oAreaGeografica.getCodigoPostal().getCodigoPostalString())) {
				bRet = false;
			}
		}
		return bRet;
	}

	public AreaGeografica getAreaGeograficaMaisPerto(CodigoPostal oCodPostal) {
		double menor = Double.MAX_VALUE;
		AreaGeografica aGeoMaisPerto = null;
		for (AreaGeografica aGeo : this.m_lstAreasGeograficas) {
			CodigoPostal codPostal = aGeo.getCodigoPostal();
			double distancia = Utils.distancia(codPostal.getLatitude(), codPostal.getLongitude(),
					oCodPostal.getLatitude(), oCodPostal.getLongitude());
			if (distancia < menor) {
				distancia = menor;
				aGeoMaisPerto = aGeo;
			}
		}
		return aGeoMaisPerto;
	}
}
