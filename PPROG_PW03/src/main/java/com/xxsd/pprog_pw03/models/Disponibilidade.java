package com.xxsd.pprog_pw03.models;

import java.io.Serializable;
import java.util.Objects;

public class Disponibilidade implements Serializable {

	private static final long serialVersionUID = 5068232456821802258L;

	private String m_strDataInicio;
	private String m_strHoraInicio;
	private String m_strDataFim;
	private String m_strHoraFim;

	public Disponibilidade(String strDataInicio, String strHoraInicio, String strDataFim, String strHoraFim) {
		this.m_strDataInicio = strDataInicio;
		this.m_strHoraInicio = strHoraInicio;
		this.m_strDataFim = strDataFim;
		this.m_strHoraFim = strHoraFim;
	}

	public String getDataInicio() {
		return this.m_strDataInicio;
	}

	public String getHoraInicio() {
		return this.m_strHoraInicio;
	}

	public String getDataFim() {
		return this.m_strDataFim;
	}

	public String getHoraFim() {
		return this.m_strHoraFim;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (getClass() != o.getClass())
			return false;
		Disponibilidade obj = (Disponibilidade) o;
		return (Objects.equals(m_strDataInicio, obj.m_strDataInicio)
				&& Objects.equals(m_strHoraInicio, obj.m_strHoraInicio)
				&& Objects.equals(m_strDataFim, obj.m_strDataFim) && Objects.equals(m_strHoraFim, obj.m_strHoraFim));
	}
}
