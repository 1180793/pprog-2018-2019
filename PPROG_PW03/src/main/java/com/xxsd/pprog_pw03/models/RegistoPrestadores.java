package com.xxsd.pprog_pw03.models;

import java.util.ArrayList;
import java.util.List;

import com.xxsd.pprog_pw03.autorizacao.AutorizacaoFacade;
import com.xxsd.pprog_pw03.exceptions.DuplicatedIDException;

public class RegistoPrestadores {

	private AutorizacaoFacade m_oAutorizacao;
	private List<PrestadorServicos> m_lstPrestadores = new ArrayList<>();

	public RegistoPrestadores(AutorizacaoFacade at) {
		this.m_oAutorizacao = at;
	}

	public List<PrestadorServicos> getPrestadores() {
		return this.m_lstPrestadores;
	}

	public PrestadorServicos novoPrestador(String strNumMecanografico, String strNomeComp, String strNomeAbrev,
			String strEmail, ArrayList<Categoria> lstCategorias, ArrayList<AreaGeografica> lstAreasGeograficas) {
		return new PrestadorServicos(strNumMecanografico, strNomeComp, strNomeAbrev, strEmail, lstCategorias,
				lstAreasGeograficas);
	}

	public boolean addPrestador(PrestadorServicos oPrestador) {
		if (oPrestador != null)
			return m_lstPrestadores.add(oPrestador);
		return false;
	}

	public PrestadorServicos getPrestadorByEmail(String strEmail) {
		for (PrestadorServicos prestador : this.m_lstPrestadores) {
			if (prestador.hasEmail(strEmail)) {
				return prestador;
			}
		}
		return null;
	}

	public boolean registaPrestador(PrestadorServicos oPrestador, String strPwd) throws DuplicatedIDException {
		if (this.validaPrestador(oPrestador, strPwd)) {
			if (this.m_oAutorizacao.registaUtilizadorComPapel(oPrestador.getNomeCompleto(), oPrestador.getEmail(),
					strPwd, Constantes.PAPEL_PRESTADOR_SERVICO)) {
				return addPrestador(oPrestador);
			}
		} else {
			throw new DuplicatedIDException("Email já registado.");
		}
		return false;
	}

	public boolean validaPrestador(PrestadorServicos oPrestador, String strPwd) {
		boolean bRet = true;
		if (strPwd == null) {
			bRet = false;
		}
		if (strPwd.isEmpty()) {
			bRet = false;
		}
		for (PrestadorServicos pres : this.m_lstPrestadores) {
			if (pres.getEmail().equalsIgnoreCase(oPrestador.getEmail())) {
				bRet = false;
			}
		}
		if (this.m_oAutorizacao.existeUtilizador(oPrestador.getEmail())) {
			bRet = false;
		}
		return bRet;
	}

}
