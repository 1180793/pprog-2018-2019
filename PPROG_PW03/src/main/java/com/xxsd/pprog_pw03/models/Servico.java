package com.xxsd.pprog_pw03.models;

import java.io.Serializable;

public interface Servico extends Serializable {

	String getId();

	String getDescricaoBreve();
	
	String getDescricaoCompleta();

	boolean hasId(String strId);

	TipoServico getTipoServico();

	Categoria getCategoria();

	boolean possuiOutrosAtributos();

	String getOutrosAtributos();

	void setOutrosAtributos(String strOutrosAtributos);

	double getCustoParaDuracao(String strDuracao);

	String getCustoHora();
	
}
