package com.xxsd.pprog_pw03.models;

import java.io.Serializable;
import java.util.Objects;

public class Categoria implements Serializable {

	private static final long serialVersionUID = 5434343455821802258L;

	/**
	 * O código da categoria
	 */
	private String m_strCodigo;
	/**
	 * A descrição da categoria
	 */
	private String m_strDescricao;

	/**
	 * Controi uma instância categoria recebendo o código e a designação nao
	 * deixando que algum dos argumentos esteja por preencher
	 * 
	 * @param strCodigo    o código da categoria
	 * @param strDescricao a descrição da categoria
	 */
	public Categoria(String strCodigo, String strDescricao) {
		if ((strCodigo == null) || (strDescricao == null) || (strCodigo.isEmpty()) || (strDescricao.isEmpty()))
			throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");

		this.m_strCodigo = strCodigo;
		this.m_strDescricao = strDescricao;
	}

	/**
	 * 
	 * @param strId
	 * @return
	 */
	public boolean hasId(String strId) {
		return this.m_strCodigo.equalsIgnoreCase(strId);
	}

	/**
	 * Devolve o codigo da categoria
	 * 
	 * @return o codigo da categoria
	 */
	public String getCodigo() {
		return this.m_strCodigo;
	}

	/**
	 * Compara a categoria com o objeto recebido
	 * 
	 * @param o objeto a comparar com a categoria
	 * @return true se o objeto recebido representar uma categotia equivalente à
	 *         categoria. Caso contrário, retorna false.
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (getClass() != o.getClass())
			return false;
		Categoria obj = (Categoria) o;
		return (Objects.equals(m_strCodigo, obj.m_strCodigo));
	}

	/**
	 * Devolve a descrição textual da categoria no formato: %s - %s
	 * 
	 * @return caracteristicas da categoria
	 */
	@Override
	public String toString() {
		return String.format("%s | %s", this.m_strCodigo, this.m_strDescricao);
	}

}