package com.xxsd.pprog_pw03.models;

import java.io.Serializable;

import com.xxsd.pprog_pw03.utils.Utils;

public class AreaGeografica implements Serializable {

	private static final long serialVersionUID = 5068131455423422258L;

	/**
	 * A designação da Area Geografica
	 */
	private String m_strDesignacao;
	/**
	 * O código postal da Area Geografica
	 */
	private CodigoPostal m_oCodPostal;
	/**
	 * O custo de deslocação de acordo com a Area Geografica
	 */
	private double m_dblCustoDeslocacao;
	/**
	 * O raio de ação mediante a Area Geografica
	 */
	private double m_dblRaioAcao;

	/**
	 * Constrói uma instância de Area Geografica recebendo a designação, o código
	 * postal, o custo da deslocação e o raio de ação.
	 * 
	 * @param strDesignacao      a designação da Area Geografica
	 * @param oCodPostal         o código postal da Area Geografica
	 * @param dblCustoDeslocacao o custo de deslocação de acordo com a Area
	 *                           Geografica
	 * @param dblRaioAcao        o raio de ação mediante a Area Geografica
	 */
	public AreaGeografica(String strDesignacao, CodigoPostal oCodPostal, double dblCustoDeslocacao,
			double dblRaioAcao) {
		this.m_strDesignacao = strDesignacao;
		this.m_oCodPostal = oCodPostal;
		this.m_dblCustoDeslocacao = dblCustoDeslocacao;
		this.m_dblRaioAcao = dblRaioAcao;
	}

	/**
	 * Devolve a designação da Area Geografica
	 * 
	 * @return a designação da Area Geografica
	 */
	public String getDesignacao() {
		return this.m_strDesignacao;
	}

	/**
	 * Devolve o código postal da Area Geografica
	 * 
	 * @return o código postal da Area Geografica
	 */
	public CodigoPostal getCodigoPostal() {
		return this.m_oCodPostal;
	}

	/**
	 * Devolve o custo de deslocação de acordo com a Area Geografica
	 * 
	 * @return o custo de deslocação de acordo com a Area Geografica
	 */
	public double getCustoDeslocacao() {
		return this.m_dblCustoDeslocacao;
	}

	/**
	 * Devolve o raio de ação mediante a Area Geografica
	 * 
	 * @return o raio de ação mediante a Area Geografica
	 */
	public double getRaioAcao() {
		return this.m_dblRaioAcao;
	}

	public boolean atuaEm(EnderecoPostal oEndPostal) {
		double dblDistancia = Utils.distancia(this.m_oCodPostal.getLatitude(), this.m_oCodPostal.getLongitude(),
				oEndPostal.getCodigoPostal().getLatitude(), oEndPostal.getCodigoPostal().getLongitude());
		if (dblDistancia <= this.m_dblRaioAcao) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Compara a Area Geografica com o objeto recebido
	 * 
	 * @param o objeto a comparar com a Area Geografica
	 * @return true se o objeto recebido representar uma Area Geografica equivalente
	 *         à Area Geografica. Caso contrário, retorna false.
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (getClass() != o.getClass())
			return false;
		AreaGeografica obj = (AreaGeografica) o;
		return m_oCodPostal.equals(obj.m_oCodPostal);
	}

	/**
	 * Devolve a descrição textualda Area Geografica no formato: %s | %s | %.2f€ |
	 * %.2f Km
	 * 
	 * @return caracteristicas da Area Geografica
	 */
	@Override
	public String toString() {
		return String.format("%s | %s | %.2f€ | %.2f Km", this.m_strDesignacao,
				this.m_oCodPostal.getCodigoPostalString(), this.m_dblCustoDeslocacao, this.m_dblRaioAcao);
	}
}
