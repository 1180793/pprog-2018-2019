package com.xxsd.pprog_pw03.models;

import java.io.Serializable;
import java.util.Objects;

public class EnderecoPostal implements Serializable {

	private static final long serialVersionUID = 54343434558213412L;

	private String m_strLocal;
	private CodigoPostal m_oCodPostal;
	private String m_strLocalidade;

	public EnderecoPostal(String strLocal, CodigoPostal oCodPostal, String strLocalidade) {
		if ((strLocal == null) || (strLocalidade == null) || (strLocal.isEmpty()) || (strLocalidade.isEmpty()))
			throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");

		this.m_strLocal = strLocal;
		this.m_oCodPostal = oCodPostal;
		this.m_strLocalidade = strLocalidade;
	}

	public CodigoPostal getCodigoPostal() {
		return this.m_oCodPostal;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (getClass() != o.getClass())
			return false;
		EnderecoPostal obj = (EnderecoPostal) o;
		return (Objects.equals(m_strLocal, obj.m_strLocal) && m_oCodPostal.equals(obj.m_oCodPostal)
				&& Objects.equals(m_strLocalidade, obj.m_strLocalidade));
	}

	@Override
	public String toString() {
		return String.format("%s | %s - %s", this.m_strLocal, this.m_oCodPostal.getCodigoPostalString(),
				this.m_strLocalidade);
	}

}
