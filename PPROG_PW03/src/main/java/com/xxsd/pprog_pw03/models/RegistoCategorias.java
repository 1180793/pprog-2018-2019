package com.xxsd.pprog_pw03.models;

import java.util.ArrayList;
import java.util.List;

import com.xxsd.pprog_pw03.exceptions.DuplicatedIDException;

public class RegistoCategorias {

	private List<Categoria> m_lstCategorias = new ArrayList<>();

	public RegistoCategorias() {
	}

	public List<Categoria> getCategorias() {
		return this.m_lstCategorias;
	}

	public Categoria getCategoriaById(String strId) {
		for (Categoria cat : this.m_lstCategorias) {
			if (cat.hasId(strId)) {
				return cat;
			}
		}
		return null;
	}

	public boolean registaCategoria(Categoria oCategoria) throws DuplicatedIDException {
		if (this.validaCategoria(oCategoria)) {
			return addCategoria(oCategoria);
		} else
			throw new DuplicatedIDException("ID de Categoria Duplicado.");
	}

	private boolean addCategoria(Categoria oCategoria) {
		return m_lstCategorias.add(oCategoria);
	}

	public boolean validaCategoria(Categoria oCategoria) {
		boolean bRet = true;
		for (Categoria cat : this.m_lstCategorias) {
			if (cat.getCodigo().equalsIgnoreCase(oCategoria.getCodigo())) {
				bRet = false;
			}
		}
		return bRet;
	}

}
