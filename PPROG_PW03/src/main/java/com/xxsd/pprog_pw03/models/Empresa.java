package com.xxsd.pprog_pw03.models;

import com.xxsd.pprog_pw03.autorizacao.AutorizacaoFacade;

public class Empresa {

	private String m_strDesignacao;
	private String m_strNIF;

	private final AutorizacaoFacade m_oAutorizacao;

	private final RegistoClientes m_oClientes;
	private final RegistoCategorias m_oCategorias;
	private final RegistoServicos m_oServicos;
	private final RegistoTiposServico m_oTiposServico;
	private final RegistoAreasGeograficas m_oAreasGeograficas;
	private final RegistoPrestadores m_oPrestadores;
	private final RegistoPedidos m_oPedidos;

	public Empresa(String strDesignacao, String strNIF) {
		if ((strDesignacao == null) || (strNIF == null) || (strDesignacao.isEmpty()) || (strNIF.isEmpty()))
			throw new IllegalArgumentException("A Designação e o NIF da Empresa não podem ser nulos.");

		this.m_strDesignacao = strDesignacao;
		this.m_strNIF = strNIF;

		this.m_oAutorizacao = new AutorizacaoFacade();

		this.m_oClientes = new RegistoClientes(m_oAutorizacao);
		this.m_oCategorias = new RegistoCategorias();
		this.m_oServicos = new RegistoServicos();
		this.m_oTiposServico = new RegistoTiposServico();
		this.m_oAreasGeograficas = new RegistoAreasGeograficas();
		this.m_oPrestadores = new RegistoPrestadores(m_oAutorizacao);
		this.m_oPedidos = new RegistoPedidos();
	}

	@Override
	public String toString() {
		return String.format("Empresa - %s %nNIF:%s%n", this.m_strDesignacao, this.m_strNIF);
	}

	// ##############################################################################################################

	// Autorização Facade

	public AutorizacaoFacade getAutorizacaoFacade() {
		return this.m_oAutorizacao;
	}

	// ##############################################################################################################

	// Registo Clientes

	public RegistoClientes getRegistoClientes() {
		return this.m_oClientes;
	}

	// ##############################################################################################################

	// Registo Áreas Geográficas

	public RegistoAreasGeograficas getRegistoAreasGeograficas() {
		return this.m_oAreasGeograficas;
	}

	// ##############################################################################################################

	// Registo Categorias

	public RegistoCategorias getRegistoCategorias() {
		return this.m_oCategorias;
	}

	// ##############################################################################################################

	// Registo Serviços

	public RegistoServicos getRegistoServicos() {
		return this.m_oServicos;
	}

	// ##############################################################################################################

	// Registo Tipos de Serviço

	public RegistoTiposServico getRegistoTiposServico() {
		return this.m_oTiposServico;
	}

	// ##############################################################################################################

	// Registo Prestadores de Serviços

	public RegistoPrestadores getRegistoPrestadores() {
		return this.m_oPrestadores;
	}

	// ##############################################################################################################

	// Registo Pedidos de Prestação de Serviços

	public RegistoPedidos getRegistoPedidos() {
		return this.m_oPedidos;
	}

}
