package com.xxsd.pprog_pw03.utils;

import java.util.ArrayList;
import java.util.List;

import com.xxsd.pprog_pw03.controllers.AplicacaoGPSD;
import com.xxsd.pprog_pw03.exceptions.DuplicatedIDException;
import com.xxsd.pprog_pw03.models.AreaGeografica;
import com.xxsd.pprog_pw03.models.Categoria;
import com.xxsd.pprog_pw03.models.Cliente;
import com.xxsd.pprog_pw03.models.CodigoPostal;
import com.xxsd.pprog_pw03.models.Empresa;
import com.xxsd.pprog_pw03.models.EnderecoPostal;
import com.xxsd.pprog_pw03.models.PrestadorServicos;
import com.xxsd.pprog_pw03.models.Servico;
import com.xxsd.pprog_pw03.models.TipoServico;

public class ConfigUtils {

	private static AplicacaoGPSD oApp = AplicacaoGPSD.getInstance();
	private static Empresa oEmpresa = oApp.getEmpresa();

	public static void loadClientes() {
		List<ArrayList<String>> list = Utils.getDataFromFile("./src/main/resources/files/Clientes.txt");
		for (ArrayList<String> d : list) {
			String strEmail = d.get(3);
			String strNome = d.get(0);
			String strNif = d.get(1);
			String strTel = d.get(2);
			ArrayList<EnderecoPostal> oMoradas = getEnderecoPostalCliente(strEmail);
			EnderecoPostal oMorada = oMoradas.get(0);
			Cliente oCliente = new Cliente(strNome, strNif, strTel, strEmail, oMorada);
			if (oMoradas.size() > 1) {
				for (int i = 1; i < oMoradas.size(); i++) {
					oCliente.addEnderecoPostal(oMoradas.get(i));
				}
			}
			try {
				oEmpresa.getRegistoClientes().registaCliente(oCliente, d.get(4));
			} catch (DuplicatedIDException e) {
				System.out.println("Email já registado no sistema.");
			}
		}
	}

	public static void loadAreasGeograficas() {
		List<ArrayList<String>> list = Utils.getDataFromFile("./src/main/resources/files/AreasGeograficas.txt");
		for (ArrayList<String> d : list) {
			String strDesignacao = d.get(0);
			double dblCustoDeslocacao = Double.parseDouble(d.get(2));
			double dblRaioAcao = Double.parseDouble(d.get(3));
			CodigoPostal oCodPostal = getCodigoPostalById(d.get(1));
			AreaGeografica oAreaGeografica = new AreaGeografica(strDesignacao, oCodPostal, dblCustoDeslocacao,
					dblRaioAcao);
			try {
				oEmpresa.getRegistoAreasGeograficas().registaAreaGeografica(oAreaGeografica);
			} catch (DuplicatedIDException e) {
				System.out.println("Área Geográfica já registada no sistema.");
			}
		}
	}

	public static void loadCategorias() {
		List<ArrayList<String>> list = Utils.getDataFromFile("./src/main/resources/files/Categorias.txt");
		for (ArrayList<String> d : list) {
			String strId = d.get(0);
			String strDesignacao = d.get(1);
			Categoria oCategoria = new Categoria(strId, strDesignacao);
			try {
				oEmpresa.getRegistoCategorias().registaCategoria(oCategoria);
			} catch (DuplicatedIDException e) {
				System.out.println("Categoria já registada no sistema.");
			}
		}
	}

	public static void loadServicos() {
		List<ArrayList<String>> list = Utils.getDataFromFile("./src/main/resources/files/Servicos.txt");
		for (ArrayList<String> d : list) {
			String strId = d.get(0);
			String strDescricaoBreve = d.get(1);
			String strDescricaoCompleta = d.get(2);
			double dCustoHora = Double.parseDouble(d.get(3));
			Categoria oCategoria = oEmpresa.getRegistoCategorias().getCategoriaById(d.get(4));
			TipoServico oTipoServico = oEmpresa.getRegistoTiposServico().getTipoServicoById(d.get(5));
			if (oTipoServico == null) {
				System.out.println("Erro ao ler o ficheiro 'Servicos.txt'");
				return;
			}
			Servico oServico = oTipoServico.novoServico(strId, strDescricaoBreve, strDescricaoCompleta, dCustoHora,
					oCategoria);
			if (oServico.possuiOutrosAtributos()) {
				oServico.setOutrosAtributos(d.get(6));
			}
			try {
				oEmpresa.getRegistoServicos().registaServico(oServico);
			} catch (DuplicatedIDException e) {
				System.out.println("Servico ja registado no sistema.");
			}
		}
	}

	public static void loadPrestadoresServico() {
		List<ArrayList<String>> list = Utils.getDataFromFile("./src/main/resources/files/PrestadoresServico.txt");
		for (ArrayList<String> d : list) {
			String strNumMecanografico = d.get(0);
			String strNomeComp = d.get(1);
			String strNomeAbrev = d.get(2);
			String strEmail = d.get(3);
			ArrayList<Categoria> lstCategorias = readCategorias(d.get(4));
			ArrayList<AreaGeografica> lstAreasGeograficas = readAreasGeograficas(d.get(5));
			String strPassword = d.get(6);
			PrestadorServicos oPrestador = new PrestadorServicos(strNumMecanografico, strNomeComp, strNomeAbrev,
					strEmail, lstCategorias, lstAreasGeograficas);
			try {
				AplicacaoGPSD.getInstance().getEmpresa().getRegistoPrestadores().registaPrestador(oPrestador,
						strPassword);
			} catch (DuplicatedIDException e) {
				System.out.println("Prestador de Serviços já registado no sistema.");
			}
		}
	}

	private static ArrayList<EnderecoPostal> getEnderecoPostalCliente(String strEmail) {
		ArrayList<EnderecoPostal> oMoradas = new ArrayList<>();
		List<ArrayList<String>> data = Utils.getDataFromFile("./src/main/resources/files/EnderecosPostais.txt");
		for (ArrayList<String> d : data) {
			if (d.get(0).equalsIgnoreCase(strEmail)) {
				EnderecoPostal oMorada = new EnderecoPostal(d.get(1), getCodigoPostalById(d.get(2)), d.get(3));
				oMoradas.add(oMorada);
			}
		}
		return oMoradas;
	}

	private static CodigoPostal getCodigoPostalById(String strCodigo) {
		List<ArrayList<String>> data = Utils.getDataFromFile("./src/main/resources/files/CodigosPostais.txt");
		for (ArrayList<String> d : data) {
			if (d.get(0).equalsIgnoreCase(strCodigo)) {
				return new CodigoPostal(d.get(0), Double.parseDouble(d.get(1)), Double.parseDouble(d.get(2)));
			}
		}
		return null;
	}

	private static ArrayList<Categoria> readCategorias(String strCategorias) {
		strCategorias = strCategorias.substring(1, strCategorias.length() - 1);
		ArrayList<Categoria> lstCategorias = new ArrayList<Categoria>();
		String[] lstCategoriasString = strCategorias.split(",");
		for (String strCat : lstCategoriasString) {
			Categoria oCat = AplicacaoGPSD.getInstance().getEmpresa().getRegistoCategorias().getCategoriaById(strCat);
			if (oCat != null) {
				lstCategorias.add(oCat);
			}
		}
		return lstCategorias;
	}

	private static ArrayList<AreaGeografica> readAreasGeograficas(String strAreasGeograficas) {
		strAreasGeograficas = strAreasGeograficas.substring(1, strAreasGeograficas.length() - 1);
		ArrayList<AreaGeografica> lstAreasGeograficas = new ArrayList<AreaGeografica>();
		String[] lstAreasGeograficasString = strAreasGeograficas.split(",");
		for (String strAGeo : lstAreasGeograficasString) {
			AreaGeografica oAGeo = AplicacaoGPSD.getInstance().getEmpresa().getRegistoAreasGeograficas()
					.getAreaGeograficaById(strAGeo);
			if (oAGeo != null) {
				lstAreasGeograficas.add(oAGeo);
			}
		}
		return lstAreasGeograficas;
	}

}
