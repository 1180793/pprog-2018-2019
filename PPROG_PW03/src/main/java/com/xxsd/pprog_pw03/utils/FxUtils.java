package com.xxsd.pprog_pw03.utils;

import java.io.IOException;

import com.xxsd.pprog_pw03.MainApp;
import com.xxsd.pprog_pw03.autorizacao.models.PapelUtilizador;
import com.xxsd.pprog_pw03.autorizacao.models.RegistoPapeisUtilizador;
import com.xxsd.pprog_pw03.autorizacao.models.SessaoUtilizador;
import com.xxsd.pprog_pw03.controllers.AplicacaoGPSD;
import com.xxsd.pprog_pw03.models.Cliente;
import com.xxsd.pprog_pw03.models.Constantes;
import com.xxsd.pprog_pw03.models.PrestadorServicos;
import com.xxsd.pprog_pw03.models.RegistoClientes;
import com.xxsd.pprog_pw03.models.RegistoPrestadores;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class FxUtils {

	public static void switchScene(ActionEvent event, String fileName, String title) throws IOException {

		BorderPane tableViewParent;

		Parent topBar = FXMLLoader.load(MainApp.class.getResource("/fxml/TopBar.fxml"));

		try {

			tableViewParent = FXMLLoader.load(MainApp.class.getClassLoader().getResource("/fxml/" + fileName + ".fxml"));
			tableViewParent.setTop(topBar);

			Scene tableViewScene = new Scene(tableViewParent);

			Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

			window.setScene(tableViewScene);
			window.setTitle("AplicacaoGPSD | " + title);
			window.setResizable(false);
			window.centerOnScreen();
			window.show();

			window.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    Alert alerta = new Alert(Alert.AlertType.CONFIRMATION);

                    alerta.setTitle("Aplicação");
                    alerta.setHeaderText("Sair da Aplicação.");
                    alerta.setContentText("Deseja mesmo encerrar a aplicação?");

                    if (alerta.showAndWait().get() == ButtonType.CANCEL) {
                        event.consume();
                    }
                }
            });
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void switchLogin(ActionEvent event) throws IOException {

		Parent root;

		try {
			root = FXMLLoader.load(MainApp.class.getClassLoader().getResource("/fxml/LoginUI.fxml"));

			Scene scene = new Scene(root);

			scene.getStylesheets().add("/styles/Styles.css");

			Stage window = MainApp.primarystage;
			if (window == null) {
				System.out.println("Erro window");
			}
			window.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    Alert alerta = new Alert(Alert.AlertType.CONFIRMATION);

                    alerta.setTitle("Aplicação");
                    alerta.setHeaderText("Sair da Aplicação.");
                    alerta.setContentText("Deseja mesmo encerrar a aplicação?");

                    if (alerta.showAndWait().get() == ButtonType.CANCEL) {
                        event.consume();
                    }
                }
            });
			window.setTitle("AplicacaoGPSD | Login");
			window.setResizable(false);
			// stage.initStyle(StageStyle.UNDECORATED);
			window.setScene(scene);
			window.centerOnScreen();
			window.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void switchPapeis(ActionEvent event) throws IOException {

		Parent root;

		try {
			root = FXMLLoader.load(MainApp.class.getClassLoader().getResource("/fxml/PapeisUI.fxml"));

			Scene scene = new Scene(root);

			scene.getStylesheets().add("/styles/Styles.css");

			Stage window = MainApp.primarystage;
			if (window == null) {
				System.out.println("Erro Window");
			}
			window.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    Alert alerta = new Alert(Alert.AlertType.CONFIRMATION);

                    alerta.setTitle("Aplicação");
                    alerta.setHeaderText("Sair da Aplicação.");
                    alerta.setContentText("Deseja mesmo encerrar a aplicação?");

                    if (alerta.showAndWait().get() == ButtonType.CANCEL) {
                        event.consume();
                    }
                }
            });
			window.setTitle("AplicacaoGPSD | Papel");
			window.setResizable(false);
			// stage.initStyle(StageStyle.UNDECORATED);
			window.setScene(scene);
			window.centerOnScreen();
			window.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void switchDashboard(ActionEvent event, String strPapel, String strEmail) throws IOException {
		RegistoClientes oClientes = AplicacaoGPSD.getInstance().getEmpresa().getRegistoClientes();
		RegistoPrestadores oPrestadores = AplicacaoGPSD.getInstance().getEmpresa().getRegistoPrestadores();
		RegistoPapeisUtilizador oPapeis = AplicacaoGPSD.getInstance().getEmpresa().getAutorizacaoFacade().getRegistoPapeis();
		PapelUtilizador oPapel = oPapeis.procuraPapel(strPapel);
		if (oPapel == null) {
			return;
		}
		SessaoUtilizador oSessao = AplicacaoGPSD.getInstance().getSessaoAtual();
		if (oPapel.hasId(Constantes.PAPEL_CLIENTE)) {
			Cliente oCliente = oClientes.getClienteByEmail(strEmail);
			FxUtils.switchScene(event, "DashboardClientes", "Dashboard - " + oCliente.getNome());
		} else if (oPapel.hasId(Constantes.PAPEL_PRESTADOR_SERVICO)) {
			PrestadorServicos oPrestador = oPrestadores.getPrestadorByEmail(strEmail);
			FxUtils.switchScene(event, "DashboardPrestador", "Dashboard - " + oPrestador.getNomeCompleto());
		} else if (oPapel.hasId(Constantes.PAPEL_ADMINISTRATIVO)) {
			FxUtils.switchScene(event, "DashboardAdministrativo", "Dashboard - " + oSessao.getNomeUtilizador());
		} else if (oPapel.hasId(Constantes.PAPEL_FRH)) {
			Utils.openAlert("Erro ao Escolher Papel", "Dashboard Não Implementada",
					"A Dashboard para o Papel 'Funcionário de Recursos Humanos' ainda não foi implementada.",
					AlertType.ERROR);
		} else {
			// Nunca deverá acontecer
			Utils.openAlert("Erro ao Escolher Papel", "Papel Inválido", "O Papel selecionado não está registado.",
					AlertType.ERROR);
		}
	}

}