package com.xxsd.pprog_pw03.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.xxsd.pprog_pw03.controllers.AplicacaoGPSD;
import com.xxsd.pprog_pw03.models.PedidoPrestacaoServico;

import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class FileChooserUtils {

    public static FileChooser getFileChooser() {
        FileChooser fileChooser = new FileChooser();
        ExtensionFilter filtro = new ExtensionFilter("Ficheiros de Pedidos de Prestação de Serviços", "*.bin");
        fileChooser.getExtensionFilters().add(filtro);
		return fileChooser;
    }

    public static void serealizePedidos(File file) {
		try {
			FileOutputStream fileOut = new FileOutputStream(file);
			ObjectOutputStream outStream = new ObjectOutputStream(fileOut);
			List<PedidoPrestacaoServico> lstPedidos = AplicacaoGPSD.getInstance().getEmpresa().getRegistoPedidos()
					.getPedidos();
			outStream.writeObject(lstPedidos);
			outStream.close();
			fileOut.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public static void deserealizePedidos(File file) {
		try {
			FileInputStream fileIn = new FileInputStream(file);
			ObjectInputStream inStream = new ObjectInputStream(fileIn);
			ArrayList<PedidoPrestacaoServico> lstPedidos = (ArrayList<PedidoPrestacaoServico>) inStream.readObject();
			AplicacaoGPSD.getInstance().getEmpresa().getRegistoPedidos().setPedidos(lstPedidos);
			inStream.close();
			fileIn.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

}