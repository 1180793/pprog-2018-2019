package com.xxsd.pprog_pw03.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Utils {

	public static String convert(String password) {
		String pass = null;
		MessageDigest md = null;

		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException ex) {
			ex.printStackTrace();
		}

		BigInteger hash = new BigInteger(1, md.digest(password.getBytes()));
		pass = hash.toString(16);
		return pass;
	}
}