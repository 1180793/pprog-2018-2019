package com.xxsd.pprog_pw03.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import com.xxsd.pprog_pw03.models.Disponibilidade;

import javafx.scene.control.Alert;

/**
 * @author XXSD
 */
public class Utils {

	/**
	 * Formato Decimal para apresentação de custos.
	 */
	public static DecimalFormat df = new DecimalFormat("#.00");

	/**
	 * Lista dos dias da semana.
	 */
	public static final String[] LIST_OF_DAYS = { "Domingo", "Segunda-Feira", "Terça-Feira", "Quarta-Feira",
			"Quinta-Feira", "Sexta-Feira", "Sábado" };

	/**
	 * Lista de tempos(hh:mm) permitidos.
	 */
	public static final String[] LIST_OF_TIMES = { "00:00", "00:30", "01:00", "01:30", "02:00", "02:30", "03:00",
			"03:30", "04:00", "04:30", "05:00", "05:30", "06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00",
			"09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00",
			"15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00", "20:30", "21:00",
			"21:30", "22:00", "22:30", "23:00", "23:30" };

	public static final String[] LIST_OF_DURATIONS = { "00:30", "01:00", "01:30", "02:00", "02:30", "03:00", "03:30",
			"04:00", "04:30", "05:00", "05:30", "06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00", "09:30",
			"10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00", "15:30",
			"16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00", "20:30", "21:00", "21:30",
			"22:00", "22:30", "23:00", "23:30" };

	/**
	 * Cria uma janela de alerta.
	 * 
	 * @param title       o título da janela do alerta
	 * @param headerText  o cabeçalho do alerta
	 * @param contentText o conteúdo do alerta
	 * @param alertType   o tipo de alerta
	 */
	public static void openAlert(String title, String headerText, String contentText, Alert.AlertType alertType) {
		Alert alert = new Alert(alertType);
		alert.setTitle(title);
		alert.setHeaderText(headerText);
		alert.setContentText(contentText);
		alert.showAndWait();
	}

	public static double convertDuracao(String strDuracao) {
		String[] temp = strDuracao.split(":");
		int firstDigit = Integer.parseInt(temp[0]);
		int secondDigit = Integer.parseInt(temp[1]);
		if (secondDigit == 0) {
			return firstDigit;
		} else if (secondDigit == 30) {
			return firstDigit + 0.5;
		} else {
			System.out.println("Duração Inválida");
			return 0;
		}
	}

	public static double distancia(double lat1, double lon1, double lat2, double lon2) {
		final double R = 6371e3;
		double theta1 = Math.toRadians(lat1);
		double theta2 = Math.toRadians(lat2);
		double deltaTheta = Math.toRadians(lat2 - lat1);
		double deltaLambda = Math.toRadians(lon2 - lon1);
		double a = Math.sin(deltaTheta / 2) * Math.sin(deltaTheta / 2)
				+ Math.cos(theta1) * Math.cos(theta2) * Math.sin(deltaLambda / 2) * Math.sin(deltaLambda / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double d = R * c;
		return d;
	}

	/**
	 * Retorna a informação presente num ficheiro.
	 * 
	 * @param filePath o caminho até ao ficheiro.
	 * @return ArrayList que contem ArrayLists referentes às diferentes linhas do
	 *         ficheiro.
	 */
	public static List<ArrayList<String>> getDataFromFile(String filePath) {
		List<ArrayList<String>> data = new ArrayList<>();
		File file = new File(filePath);
		try (Scanner scanner = new Scanner(file)) {
			while (scanner.hasNextLine()) {
				String[] lineData = scanner.nextLine().split(";");
				ArrayList<String> line = new ArrayList<>();
				for (String item : lineData) {
					line.add(item);
				}
				data.add(line);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<String> getFileNamesFromFolder(String folderPath) {
		File folder = new File(folderPath);
		File[] listOfFiles = folder.listFiles();
		List<String> fileNames = new ArrayList<>();
		for (File listOfFile : listOfFiles) {
			if (listOfFile.isFile()) {
				fileNames.add(listOfFile.getName());
			} else if (listOfFile.isDirectory()) {
				// do nothing
			}
		}
		return fileNames;
	}

	public static String getDiaSemana(String strData) throws ParseException {
		Calendar c = Calendar.getInstance();
		Date data = new SimpleDateFormat("yyyy-MM-dd").parse(strData);
		c.setTime(data);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
		String diaSemana = LIST_OF_DAYS[dayOfWeek];
		return diaSemana;
	}

	public static Date somarTempo(String strData, String strTempo1, String strTempo2) {
		String[] sData = strData.split("-");
		String[] sHora1 = strTempo1.split(":");
		String[] sHora2 = strTempo2.split(":");
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(Integer.parseInt(sData[0]), Integer.parseInt(sData[1]), Integer.parseInt(sData[2]),
				Integer.parseInt(sHora1[0]), Integer.parseInt(sHora1[1])));
		cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(sHora2[0]));
		cal.add(Calendar.MINUTE, Integer.parseInt(sHora2[1]));
		Date novaData = cal.getTime();
		return novaData;
	}

	public static boolean validarData(String strDataInicio, String strHoraInicio, String strDataFim,
			String strHoraFim) {
		String[] sDataInicio = strDataInicio.split("-");
		String[] sDataFim = strDataFim.split("-");
		String[] sHoraInicio = strHoraInicio.split(":");
		String[] sHoraFim = strHoraFim.split(":");
		Date dataInicio = new Date(Integer.parseInt(sDataInicio[0]), Integer.parseInt(sDataInicio[1]),
				Integer.parseInt(sDataInicio[2]), Integer.parseInt(sHoraInicio[0]), Integer.parseInt(sHoraInicio[1]));
		Date dataFim = new Date(Integer.parseInt(sDataFim[0]), Integer.parseInt(sDataFim[1]),
				Integer.parseInt(sDataFim[2]), Integer.parseInt(sHoraFim[0]), Integer.parseInt(sHoraFim[1]));
		if (dataInicio.before(dataFim)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean validarDisponibilidade(Disponibilidade oDisponibilidade,
			List<Disponibilidade> lstDisponibilidades) {
		String[] sDataInicio = oDisponibilidade.getDataInicio().split("-");
		String[] sDataFim = oDisponibilidade.getDataFim().split("-");
		String[] sHoraInicio = oDisponibilidade.getHoraInicio().split(":");
		String[] sHoraFim = oDisponibilidade.getHoraFim().split(":");

		Date dispInicio = new Date(Integer.parseInt(sDataInicio[0]), Integer.parseInt(sDataInicio[1]),
				Integer.parseInt(sDataInicio[2]), Integer.parseInt(sHoraInicio[0]), Integer.parseInt(sHoraInicio[1]));
		Date dispFim = new Date(Integer.parseInt(sDataFim[0]), Integer.parseInt(sDataFim[1]),
				Integer.parseInt(sDataFim[2]), Integer.parseInt(sHoraFim[0]), Integer.parseInt(sHoraFim[1]));

		if (lstDisponibilidades == null) {
			return true;
		}

		for (Disponibilidade o : lstDisponibilidades) {
			String[] sDataInicio2 = o.getDataInicio().split("-");
			String[] sDataFim2 = o.getDataFim().split("-");
			String[] sHoraInicio2 = o.getHoraInicio().split(":");
			String[] sHoraFim2 = o.getHoraFim().split(":");

			Date dispInicio2 = new Date(Integer.parseInt(sDataInicio2[0]), Integer.parseInt(sDataInicio2[1]),
					Integer.parseInt(sDataInicio2[2]), Integer.parseInt(sHoraInicio2[0]),
					Integer.parseInt(sHoraInicio2[1]));
			Date dispFim2 = new Date(Integer.parseInt(sDataFim2[0]), Integer.parseInt(sDataFim2[1]),
					Integer.parseInt(sDataFim2[2]), Integer.parseInt(sHoraFim2[0]), Integer.parseInt(sHoraFim2[1]));

			if (dispInicio.before(dispFim2) && dispInicio2.before(dispFim)) {
				return false;
			}
		}
		return true;
	}

	public static List<Disponibilidade> ordenarDisponibilidades(List<Disponibilidade> lstDisponibilidades) {
		Collections.sort(lstDisponibilidades, new OrdenarDisponibilidades());
		return lstDisponibilidades;
	}

	static class OrdenarDisponibilidades implements Comparator<Disponibilidade> {

		@Override
		public int compare(Disponibilidade o1, Disponibilidade o2) {
			String[] sDataInicio = o1.getDataInicio().split("-");
			String[] sHoraInicio = o1.getHoraInicio().split(":");
			Date o1Date = new Date(Integer.parseInt(sDataInicio[0]), Integer.parseInt(sDataInicio[1]),
					Integer.parseInt(sDataInicio[2]), Integer.parseInt(sHoraInicio[0]),
					Integer.parseInt(sHoraInicio[1]));

			String[] sDataInicio2 = o2.getDataInicio().split("-");
			String[] sHoraInicio2 = o2.getHoraInicio().split(":");
			Date o2Date = new Date(Integer.parseInt(sDataInicio2[0]), Integer.parseInt(sDataInicio2[1]),
					Integer.parseInt(sDataInicio2[2]), Integer.parseInt(sHoraInicio2[0]),
					Integer.parseInt(sHoraInicio2[1]));

			if (o1Date.before(o2Date)) {
				return -1;
			} else if (o1Date.after(o2Date)) {
				return 1;
			} else {
				return 0;
			}
		}

	}

}
