package com.xxsd.pprog_pw03.autorizacao.models;

import java.util.Objects;

public class PapelUtilizador {

	private String m_strPapel;
	private String m_strDescricao;

	public PapelUtilizador(String strPapel) {
		if ((strPapel == null) || (strPapel.isEmpty()))
			throw new IllegalArgumentException("O argumento não pode ser nulo ou vazio.");

		this.m_strPapel = strPapel;
		this.m_strDescricao = strPapel;
	}

	public PapelUtilizador(String strPapel, String strDescricao) {
		if ((strPapel == null) || (strDescricao == null) || (strPapel.isEmpty()) || (strDescricao.isEmpty()))
			throw new IllegalArgumentException("Nenhum dos argumentos não pode ser nulo ou vazio.");

		this.m_strPapel = strPapel;
		this.m_strDescricao = strDescricao;
	}

	public String getPapel() {
		return this.m_strPapel;
	}

	public String getDescricao() {
		return this.m_strDescricao;
	}

	public boolean hasId(String strId) {
		return this.m_strPapel.equals(strId);
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 23 * hash + Objects.hashCode(this.m_strPapel);
		return hash;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (getClass() != o.getClass())
			return false;
		PapelUtilizador obj = (PapelUtilizador) o;
		return Objects.equals(m_strPapel, obj.m_strPapel);
	}

	@Override
	public String toString() {
		if (this.m_strPapel.equalsIgnoreCase(this.m_strDescricao)) {
			return String.format("%s", this.m_strPapel);
		} else {
			return String.format("%s - %s", this.m_strPapel, this.m_strDescricao);
		}
	}
	
}
