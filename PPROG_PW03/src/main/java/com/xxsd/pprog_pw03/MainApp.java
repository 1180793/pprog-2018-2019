package com.xxsd.pprog_pw03;

import com.xxsd.pprog_pw03.utils.ConfigUtils;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
//import javafx.stage.StageStyle;

public class MainApp extends Application {

	public static Stage primarystage;

	@Override
	public void start(Stage stage) throws Exception {
		MainApp.primarystage = stage;
		ConfigUtils.loadClientes();
		ConfigUtils.loadAreasGeograficas();
		ConfigUtils.loadCategorias();
		ConfigUtils.loadServicos();
		ConfigUtils.loadPrestadoresServico();
//		for (Categoria oCat : oApp.getEmpresa().getRegistoCategorias().getCategorias()) {
//			System.out.println("##############");
//			System.out.println(oCat.toString());
//		}
//		Cliente oCliente = AplicacaoGPSD.getInstance().getEmpresa().getRegistoClientes().getClienteByEmail("cli2@esoft.pt");
//		for (AreaGeografica oAGeo : AplicacaoGPSD.getInstance().getEmpresa().getRegistoAreasGeograficas().getAreasGeograficas()) {
//			System.out.println("##############");
//			System.out.println(oAGeo.toString());
//				double dblDistancia = Utils.distancia(oAGeo.getCodigoPostal().getLatitude(), oAGeo.getCodigoPostal().getLongitude(),
//						oCliente.getEnderecosPostais().get(0).getCodigoPostal().getLatitude(), oCliente.getEnderecosPostais().get(0).getCodigoPostal().getLongitude());
//				if (dblDistancia <= oAGeo.getCustoDeslocacao()) {
//					System.out.println("true");
//				} else {
//					System.out.println("faLSE");
//				}
//		}
//		for (Servico oServ : oApp.getEmpresa().getRegistoServicos().getServicos()) {
//			System.out.println("##############");
//			System.out.println(oServ.toString());
//		}
//		for (PrestadorServicos oPrestador : AplicacaoGPSD.getInstance().getEmpresa().getRegistoPrestadores().getPrestadores()) {
//			System.out.println("##############");
//			System.out.println(oPrestador.toString());
//		}
		
	    Parent root = FXMLLoader.load(getClass().getResource("/fxml/LoginUI.fxml"));

		Scene scene = new Scene(root);
		scene.getStylesheets().add("/styles/Styles.css");

		stage.setTitle("AplicacaoGPSD - Login");
		stage.setResizable(false);
		// stage.initStyle(StageStyle.UNDECORATED);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * The main() method is ignored in correctly deployed JavaFX application. main()
	 * serves only as fallback in case the application can not be launched through
	 * deployment artifacts, e.g., in IDEs with limited FX support. NetBeans ignores
	 * main().
	 *
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}

}
