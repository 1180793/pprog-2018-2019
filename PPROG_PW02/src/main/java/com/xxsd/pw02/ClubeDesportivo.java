package com.xxsd.pw02;

import com.xxsd.utilitarios.Data;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author XXSD
 */
public class ClubeDesportivo {
    
    /**
     * O nome do clube desportivo.
     */
    private String m_strNome;
    
    /**
     * A data de fundação do clube desportivo.
     */
    private Data m_oDataFundacao;
    
    /**
     * A lista de atletas do clube desportivo.
     */
    private List<Atleta> m_lstAtletas;
    
    /**
     * Constrói uma instância de Clube Desportivo recebendo o nome, a data de fundação 
     * e a lista de atletas do clube desportivo.
     * 
     * @param nome o nome do clube desportivo
     * @param dataFundacao a data de fundação do clube desportivo
     * @param atletas a lista de atletas do clube desportivo
     */
    public ClubeDesportivo(String nome, Data dataFundacao, List<Atleta> atletas) {
        this.m_strNome = nome;
        this.m_oDataFundacao = dataFundacao;
        this.m_lstAtletas = new ArrayList<>(atletas);
    }
    
    /**
     * Retorna o nome do clube desportivo.
     * 
     * @return o nome do clube desportivo
     */
    public String getNome() {
        return this.m_strNome;
    }
    
    /**
     * Retorna a data de fundação do clube desportivo.
     * 
     * @return a data de fundação do clube desportivo
     */
    public Data getData() {
        return this.m_oDataFundacao;
    }
    
    /**
     * Retorna a lista de atletas do clube desportivo.
     * 
     * @return a lista de atletas do clube desportivo
     */
    public List<Atleta> getAtletas() {
        return new ArrayList<>(this.m_lstAtletas);
    }
    
    /**
     * Modifica o nome do clube desportivo.
     * 
     * @param strNovoNome o novo nome do clube desportivo
     */
    public void setNome(String strNovoNome) {
        this.m_strNome = strNovoNome;
    }
    
    /**
     * Modifica a data de fundação do clube desportivo.
     * 
     * @param oNovaData a nova data de fundação do clube desportivo
     */
    public void setData(Data oNovaData) {
        this.m_oDataFundacao = oNovaData;
    }
    
    /**
     * Modifica a lista de atletas do clube desportivo.
     * 
     * @param lstNovoAtletas a nova lista de atletas do clube desportivo
     */
    public void setAtletas(List<Atleta> lstNovoAtletas) {
        this.m_lstAtletas = new ArrayList<>(lstNovoAtletas);
    }
    
    /**
     * Devolve a descrição textual do clube desportivo.
     *
     * @return caraterísticas do clube desportivo
     */
    @Override
    public String toString() {
        return String.format("Nome: %s%nData de Fundação: %s%n", this.m_strNome, this.m_oDataFundacao.toString());
    }
    
    /**
     * Adiciona um atleta ao clube desportivo.
     * 
     * @param novoAtleta novo atleta a adicionar
     */
    public void addAtleta(Atleta novoAtleta) {
        m_lstAtletas.add(novoAtleta.clone());
    }
    
    /**
     * Retorna o valor total a pagar aos atletas.
     * 
     * @return valor total a pagar aos atletas
     */
    public double calcularTotalSalario() {
        double total = 0;
        for (Atleta atleta : getAtletas()) {
            if (atleta instanceof AtletaAmador) {
                total =+ atleta.calcularSalario();
            }
            if (atleta instanceof AtletaSemiProfissional) {
                total =+ ((AtletaSemiProfissional) atleta).calcularValorIRS();
            }
            if (atleta instanceof AtletaProfissional) {
                total =+ ((AtletaProfissional) atleta).calcularValorIRS();
            }
        }
        return total;
    }
    
    /**
     * Retorna a lista de atletas ordenada alfabeticamente por nome.
     * 
     * @return lista de atletas ordenada alfabeticamente por nome.
     */
    public List<Atleta> getAtletasAlfabeticamente() {
        List<Atleta> sortedList = this.m_lstAtletas;
        Collections.sort(sortedList, new CompararAtletaAlfabeto());
        return sortedList;
    }
    
    /**
     * Retorna a lista de atletas ordenada inversamente pelo valor dos prémios.
     * 
     * @return lista de atletas ordenada inversamente pelo valor dos prémios.
     */
    public List<Atleta> getAtletasPremioInverso() {
        List<Atleta> sortedList = this.m_lstAtletas;
        Collections.sort(sortedList, Collections.reverseOrder(new CompararAtletaPremios()));
        return sortedList;
    }
    
    /**
     * Retorna a lista de atletas ordenada inversamente pelo valor dos prémios.
     * 
     * @return lista de atletas ordenada alfabeticamente por categoria, modalidade e nome.
     */
    public List<Atleta> getAtletasOrdenados() {
        List<Atleta> sortedList = this.m_lstAtletas;
        Collections.sort(sortedList, new CompararAtletaCategoriaModalidadeNome());
        return sortedList;
    }
    
    /**
     * Nested Class que cria critério de comparação 
     * para ordenar atletas por ordem alfabética.
     */
    class CompararAtletaAlfabeto implements Comparator<Atleta> {
        @Override
        public int compare(Atleta o1, Atleta o2) {
            return o1.getNome().compareTo(o2.getNome());
        }    
    }
    
    /**
     * Nested Class que cria critério de comparação 
     * para ordenar atletas por ordem alfabética.
     */
    class CompararAtletaPremios implements Comparator<Atleta> {
        @Override
        public int compare(Atleta o1, Atleta o2) {
            if (o1.getPremios() > o2.getPremios()) {
                return 1;
            }
            if (o1.getPremios() < o2.getPremios()) {
                return -1;
            }
            return 0;
        }
    }
    
    /**
     * Nested Class que cria critério de comparação 
     * para ordenar atletas alfabeticamente por 
     * categoria, modalidade e nome.
     */
    class CompararAtletaCategoriaModalidadeNome implements Comparator<Atleta> {
        @Override
        public int compare(Atleta o1, Atleta o2) {
            if (o1.getClass() != o2.getClass()) {
                return o1.getClass().toString().compareTo(o2.getClass().toString());
            } else if (!o1.getAtividade().equalsIgnoreCase(o2.getAtividade())) {
                return o1.getAtividade().compareTo(o2.getAtividade());
            } else {
                return o1.getNome().compareTo(o2.getNome());
            }
        }
    }
}
