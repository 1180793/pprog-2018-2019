package com.xxsd.pw02;

/**
 * @author XXSD
 */
public abstract class Atleta {

    /**
     * O nome do atleta.
     */
    private String m_strNome;

    /**
     * O número de identificação civil do atleta.
     */
    private String m_strNIC;

    /**
     * O género do atleta.
     */
    private String m_strGenero;

    /**
     * A idade do atleta.
     */
    private int m_intIdade;

    /**
     * A atividade do atleta.
     */
    private String m_strAtividade;

    /**
     * O valor mensal dos prémios arrecadados pelo atleta.
     */
    private double m_dblPremios;
    
     /**
     * O valor da frequência cardíaca em repouso do atleta.
     */
    private int m_intFCR;
    
    /**
     * O nome do atleta, por omissão.
     */
    private final String NOME_POR_OMISSAO = "Sem Nome";
    
    /**
     * O número de identificação fiscal do atleta, por omissão.
     */
    private final String NIC_POR_OMISSAO = "00000000";
    
    /**
     * O género do atleta, por omissão.
     */
    private final String GENERO_POR_OMISSAO = "Género Não Especificado";
    
    /**
     * A idade do atleta, por omissão.
     */
    private final int IDADE_POR_OMISSAO = 0;
    
    /**
     * A atividade do atleta, por omissão.
     */
    private final String ATIVIDADE_POR_OMISSAO = "Sem Atividade";

    /**
     * O valor de prémios arrecadados mensalmente pelo atleta, por omissão.
     */
    private final double PREMIOS_POR_OMISSAO = 0;
    
    /**
     * O valor da frequência cardíaca em repouso do atleta, por omissão.
     */
    private final int FCR_POR_OMISSAO = 0;
    
    /**
     * A atividade caminhada.
     */
    static public final String ATIVIDADE_CAMINHADA = "Caminhada";
    
    /**
     * A atividade ciclismo.
     */
    static public final String ATIVIDADE_CICLISMO = "Ciclismo";
    
    /**
     * A atividade corrida.
     */
    static public final String ATIVIDADE_CORRIDA = "Corrida";
    
    /**
     * A atividade natação.
     */
    static public final String ATIVIDADE_NATACAO = "Natação";
    
    /**
     * O género masculino.
     */
    static public final String GENERO_MASCULINO = "Masculino";
    
    /**
     * O género feminino.
     */
    static public final String GENERO_FEMININO = "Feminino";
    
    /**
     * Constante da taxa do IRS a descontar.
     */
    static public final int TAXA_IRS = 10;
    
    /**
     * Constante da percentagem de intensidade de treino necessária para a queima de gordura.
     */
    static private final int INTENSIDADE_TREINO_QUEIMAGORDURA = 60;
    
    /**
     * Constante da percentagem de intensidade de treino necessária para trabalhar a capacidade cardiorrespiratória.
     */
    static private final int INTENSIDADE_TREINO_CAPACIDADE_CARDIORRESPIRATORIA = 75;
    
    /**
     * Constrói uma instância de Atleta atribuindo ao nome, número de identificação 
     * civil, género, idade, atividade, frequência cardíaca em repouso do atleta e 
     * valor de prémios arrecadados mensalmente os valores por omissão respetivos.
     */
    public Atleta() {
        this.m_strNome = NOME_POR_OMISSAO;
        this.m_strNIC = NIC_POR_OMISSAO;
        this.m_strGenero = GENERO_POR_OMISSAO;
        this.m_intIdade = IDADE_POR_OMISSAO;
        this.m_strAtividade = ATIVIDADE_POR_OMISSAO;
        this.m_intFCR = FCR_POR_OMISSAO;
        this.m_dblPremios = PREMIOS_POR_OMISSAO;
    }

    /**
     * Constrói uma instância de Atleta recebendo o nome, número de identificação
     * civil, género, idade, atividade e valor de prémios arrecadados mensalmente.
     * 
     * @param strNome o nome do atleta
     * @param strNIC o número de identificação civil do atleta
     * @param strGenero o género do atleta
     * @param intIdade a idade do atleta
     * @param strAtividade a atividade do atleta
     * @param intFCR a frequência cardíaca em repouso do atleta
     * @param dblPremios o valor de prémios arrecadados mensalmente pelo atleta
     */
    public Atleta(String strNome, String strNIC, String strGenero, int intIdade, String strAtividade, int intFCR, double dblPremios) {
            this.m_strNome = strNome;
            this.m_strNIC = strNIC;
            this.m_strGenero = strGenero;
            this.m_intIdade = intIdade;
            this.m_strAtividade = strAtividade;
            this.m_intFCR = intFCR;
            this.m_dblPremios = dblPremios;
    }
    
    /**
     * Constrói uma instância cópia de Atleta.
     * 
     * @param oAtleta a instância Atleta a ser copiada
     */
    public Atleta(Atleta oAtleta) {
        this(oAtleta.getNome(), oAtleta.getNIC(), oAtleta.getGenero(), oAtleta.getIdade(), oAtleta.getAtividade(), oAtleta.getFCR(), oAtleta.getPremios());
    }

    /**
     * Devolve o nome do atleta.
     * 
     * @return nome do atleta 
     */
    public String getNome() {
        return this.m_strNome;
    }

    /**
     * Devolve o número de identificação civil do atleta.
     * 
     * @return número de identificação civil do atleta 
     */
    public String getNIC() {
        return this.m_strNIC;
    }

    /**
     * Devolve o género do atleta.
     * 
     * @return género do atleta
     */
    public String getGenero() {
        return this.m_strGenero;
    }

    /**
     * Devolve a idade do atleta.
     * 
     * @return idade do atleta 
     */
    public int getIdade() {
        return this.m_intIdade;
    }

    /**
     * Devolve a atividade do atleta.
     * 
     * @return atividade do atleta 
     */
    public String getAtividade() {
        return this.m_strAtividade;
    }
    
    /**
     * Devolve a frequência cardíaca em repouso do atleta.
     * 
     * @return a frequência cardíaca em repouso do atleta
     */
    public int getFCR() {
        return this.m_intFCR;
    }

    /**
     * Devolve o valor de prémios arrecadados mensalmente pelo atleta.
     * 
     * @return valor de prémios arrecadados mensalmente pelo atleta
     */
    public double getPremios() {
        return this.m_dblPremios;
    }
    
    /**
     * Modifica o nome do atleta.
     * 
     * @param strNovoNome o novo nome do atleta
     */
    public void setNome(String strNovoNome) {
        this.m_strNome = strNovoNome;
    }

    /**
     * Modifica o número de identificação civil do atleta.
     * 
     * @param strNovoNIC o novo número de identificação civil do atleta
     */
    public void setNIC(String strNovoNIC) {
        this.m_strNIC = strNovoNIC;
    }

    /**
     * Modifica o género do atleta.
     * 
     * @param strNovoGenero o novo género do atleta
     */
    public void setGenero(String strNovoGenero) {
        this.m_strGenero = strNovoGenero;
    }

    /**
     * Modifica a idade do atleta.
     * 
     * @param intNovaIdade a nova idade do atleta 
     */
    public void setIdade(int intNovaIdade) {
        this.m_intIdade = intNovaIdade;
    }

    /**
     * Modifica a atividade do atleta.
     * 
     * @param strNovaAtividade a nova atividade do atleta
     */
    public void setAtividade(String strNovaAtividade) {
        this.m_strAtividade = strNovaAtividade;
    }
    
    /**
     * Modifica a frequência cardíaca em repouso do atleta.
     * 
     * @param intNovaFCR a nova frequência cardíaca em repouso do atleta
     */
    public void setFCR(int intNovaFCR) {
        this.m_intFCR = intNovaFCR;
    }

    /**
     * Modifica o valor de prémios arrecadados mensalmente pelo atleta.
     * 
     * @param dblNovoPremios o novo valor de prémios arrecadados mensalmente pelo atleta
     */
    public void setPremios(double dblNovoPremios) {
        this.m_dblPremios = dblNovoPremios;
    }
    
    /**
     * Devolve a descrição textual do atleta.
     *
     * @return caraterísticas do atleta
     */
    @Override
    public String toString() {
        return String.format("Nome: %s%n"
//                + "NIC: %s%n"
//                + "Género: %s%n"
//                + "Idade: %s%n"
                + "Modalidade: %s%n"
//                + "Frequência Cardíaca em Repouso: %d%n"
                + "Valor Mensal Arrecadado em Prémios: %.2f€%n",
                this.m_strNome, 
                //this.m_strNIC, 
                //this.m_strGenero, 
                //this.m_intIdade, 
                this.m_strAtividade, 
                //this.m_intFCR, 
                this.m_dblPremios);
    }
    
    /**
     * Calcula a frequência cardíaca máxima do atleta.
     * 
     * @return frequência cardíaca máxima do atleta
     */
    public double calcFreqCardiacaMax() {
        String atividade = this.m_strAtividade;
        String genero = this.m_strGenero;
        int idade = this.m_intIdade;
        if (atividade.equalsIgnoreCase(ATIVIDADE_CAMINHADA) || (atividade.equalsIgnoreCase(ATIVIDADE_CORRIDA))) {
            return (208.75 - (0.73 * idade));
        } else if (atividade.equals(ATIVIDADE_CICLISMO)) {
            if (genero.equalsIgnoreCase(GENERO_FEMININO)) {
                return (189 - (0.56 * idade));
            } else if (genero.equalsIgnoreCase(GENERO_MASCULINO)) {
                return (202 - (0.72 * idade));
            }
        } else if (atividade.equalsIgnoreCase(ATIVIDADE_NATACAO)) {
            return (204 - (1.70 * idade));
        }
        return 0f;
    }
    
    /**
     * Calcula a frequência cardíaca de trabalho quando o objetivo é a queima de gordura.
     * 
     * @return frequência cardíaca de trabalho quando o objetivo é a queima de gordura
     */
    public double calcFCT_QueimaGordura() {
        return this.m_intFCR + ( (INTENSIDADE_TREINO_QUEIMAGORDURA / 100) * (this.calcFreqCardiacaMax() - this.m_intFCR));
    }
    
    /**
     * Calcula a frequência cardíaca de trabalho quando o objetivo é trabalhar a capacidade cardiorrespiratória.
     * 
     * @return frequência cardíaca de trabalho quando o objetivo é trabalhar a capacidade cardiorrespiratória
     */
    public double calcFCT_CapacidadeCardio() {
        return this.m_intFCR + ( (INTENSIDADE_TREINO_CAPACIDADE_CARDIORRESPIRATORIA / 100) * (this.calcFreqCardiacaMax() - this.m_intFCR));
    }

    /**
     * Calcula o valor mensal a atribuir a cada atleta.
     * 
     * @return valor mensal a atribuir a cada atleta
     */
    abstract public double calcularSalario();
    
    /**
     * Copia e retorna uma instância Atleta.
     * 
     * @return clone da instância Atleta 
     */
    @Override
    abstract public Atleta clone();
}
