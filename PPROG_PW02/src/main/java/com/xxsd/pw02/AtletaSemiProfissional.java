package com.xxsd.pw02;

/**
 * @author XXSD
 */
public class AtletaSemiProfissional extends AtletaNaoProfissional implements PagavelIRS{
    
    /**
     * Parcela fixa dos atletas semi profissionais.
     */
    private double dblValorFixo = 300;

    /**
     * Constrói uma instância de Atleta Semi Profissional atribuindo ao nome, número de 
     * identificação civil, género, idade, atividade, frequência cardíaca em repouso
     * do atleta, valor de prémios arrecadados mensalmente e antiguidade do atleta
     * os valores por omissão respetivos.
     */
    public AtletaSemiProfissional() {
        super();
    }
    
    /**
     * Constrói uma instância de Atleta Semi Profissional recebendo o nome, número de identificação
     * civil, género, idade, atividade, valor de prémios arrecadados mensalmente
     * e antiguidade.
     * 
     * @param strNome o nome do atleta
     * @param strNIC o número de identificação civil do atleta
     * @param strGenero o género do atleta
     * @param intIdade a idade do atleta
     * @param strAtividade a atividade do atleta
     * @param intFCR a frequência cardíaca em repouso do atleta
     * @param dblPremios o valor de prémios arrecadados mensalmente pelo atleta
     * @param intAntiguidade a antiguidade do atleta
     */
    public AtletaSemiProfissional(String strNome, String strNIC, String strGenero, int intIdade, String strAtividade, int intFCR, double dblPremios, int intAntiguidade) {
        super(strNome, strNIC, strGenero, intIdade, strAtividade, intFCR, dblPremios, intAntiguidade);
    }
    
    /**
     * Constrói uma instância cópia de Atleta Semi Profissional.
     * 
     * @param oAtletaSemiProfissional a instância Atleta Semi Profissional a ser copiada
     */
    public AtletaSemiProfissional(AtletaSemiProfissional oAtletaSemiProfissional) {
        this(oAtletaSemiProfissional.getNome(), oAtletaSemiProfissional.getNIC(), oAtletaSemiProfissional.getGenero(), oAtletaSemiProfissional.getIdade(), oAtletaSemiProfissional.getAtividade(), oAtletaSemiProfissional.getFCR(), oAtletaSemiProfissional.getPremios(), oAtletaSemiProfissional.getAntiguidade());
    }
    
    /**
     * Devolve a parcela fixa dos atletas semi profissionais.
     * 
     * @return parcela fixa dos atletas semi profissionais
     */
    public double getValorFixo() {
        return dblValorFixo;
    }
    
    /**
     * Modifica a parcela fixa dos atletas semi profissionais.
     * 
     * @param novoValorFixo nova parcela fixa dos atletas semi profissionais
     */
    public void setValorFixo(double novoValorFixo) {
        dblValorFixo = novoValorFixo;
    }
    
    @Override
    public String toString() {
        return "Atleta Semi Profissional\n" + super.toString() + "Valor Fixo (-" + Atleta.TAXA_IRS + "%): " + calcularValorIRS();
    }
    
    /**
     * Calcula o valor mensal a atribuir a cada atleta semi profissional.
     * 
     * @return valor mensal a atribuir a cada atleta semi profissional
     */
    @Override
    public double calcularSalario() {
        return dblValorFixo + ((this.getAntiguidadePercentagem() / 100) * dblValorFixo);
    }
    
    /**
     * Retorna o valor a pagar ao Atleta Semi Profissional, deduzindo a taxa do IRS.
     * @return valor a pagar ao Atleta Semi Profissional, deduzindo a taxa do IRS.
     */
    @Override
    public double calcularValorIRS() {
        return (this.calcularSalario() - (TAXA_IRS / 100) * this.calcularSalario());
    }
    
    /**
     * Copia e retorna uma instância Atleta Semi Profissional.
     * 
     * @return clone da instância Atleta Semi Profissional 
     */
    @Override
    public AtletaSemiProfissional clone() {
        return new AtletaSemiProfissional(this);
    }
    
}
