package com.xxsd.pw02;

/**
 * @author XXSD
 */
public class AtletaProfissional extends Atleta implements PagavelIRS{
    
    /**
     * O valor fixo do atleta.
     */
    private double m_dblValorFixo;
    
    /**
     * A percentagem da parcela variável dos atletas profissionais.
     */
    private double percentagemParcelaVariavel = 20;

    /**
     * A parcela fixa do atleta, por omissão.
     */
    private final int FIXO_POR_OMISSAO = 0;
    
    /**
     * Contador de atletas profissionais.
     */
    static private int contadorAtletaProfissional = 0;
    
    /**
     * Constrói uma instância de Atleta Profissional atribuindo ao nome, número de 
     * identificação civil, género, idade, atividade, frequência cardíaca em repouso
     * do atleta, valor de prémios arrecadados mensalmente e parcela fixa do atleta
     * os valores por omissão respetivos.
     */
    public AtletaProfissional() {
        super();
        this.m_dblValorFixo = FIXO_POR_OMISSAO;
        contadorAtletaProfissional++;
    }
    
    /**
     * Constrói uma instância de AtletaProfissional recebendo o nome, número de identificação
     * civil, género, idade, atividade, valor de prémios arrecadados mensalmente e parcela fixa.
     * 
     * @param strNome o nome do atleta
     * @param strNIC o número de identificação civil do atleta
     * @param strGenero o género do atleta
     * @param intIdade a idade do atleta
     * @param strAtividade a atividade do atleta
     * @param intFCR a frequência cardíaca em repouso do atleta
     * @param dblPremios o valor de prémios arrecadados mensalmente pelo atleta
     * @param dblValorFixo a parcela fixa do atleta
     */
    public AtletaProfissional(String strNome, String strNIC, String strGenero, int intIdade, String strAtividade,int intFCR, double dblPremios, double dblValorFixo) {
        super(strNome, strNIC, strGenero, intIdade, strAtividade,intFCR,dblPremios);
        this.m_dblValorFixo = dblValorFixo;
        contadorAtletaProfissional++;
    }
    
    /**
     * Constrói uma instância cópia de Atleta Profissional.
     * 
     * @param oAtletaProfissional a instância Atleta Profissional a ser copiada
     */
    public AtletaProfissional(AtletaProfissional oAtletaProfissional) {
        this(oAtletaProfissional.getNome(), oAtletaProfissional.getNIC(), oAtletaProfissional.getGenero(), oAtletaProfissional.getIdade(), oAtletaProfissional.getAtividade(), oAtletaProfissional.getFCR(), oAtletaProfissional.getPremios(), oAtletaProfissional.getValorFixo());
    }
    
    /**
     * Devolve a parcela fixa do atleta.
     * 
     * @return parcela fixa do atleta 
     */
    public double getValorFixo() {
        return this.m_dblValorFixo;
    }
    
    /**
     * Devolve a parcela variável dos atletas profissionais.
     * 
     * @return parcela variável dos atletas profissionais
     */
    public double getPercentagemParcelaVariavel() {
        return percentagemParcelaVariavel;
    }
    
    /**
     * Devolve o valor do contador de atletas profissionais.
     * 
     * @return valor do contador de atletas profissionais. 
     */
    static public int getContadorAtletaProfissional() {
        return contadorAtletaProfissional;
    }
    
    /**
     * Modifica a parcela fixa do atleta.
     * 
     * @param dblNovoValorFixo a nova parcela fixa do atleta
     */
    public void setValorFixo(double dblNovoValorFixo) {
        this.m_dblValorFixo = dblNovoValorFixo;
    }
    
    /**
     * Modifica a percentagem da parcela variável dos atletas profissionais.
     * 
     * @param novaPercentagem  a nova percentagem da parcela variável dos atletas profissionais
     */
    public void setPercentagemParcelaVariavel(double novaPercentagem) {
        percentagemParcelaVariavel = novaPercentagem;
    }
    
    /**
     * Devolve a descrição textual do atleta profissional.
     *
     * @return caraterísticas do atleta profissional
     */
    @Override
    public String toString() {
        return "Atleta Profissional\n" + super.toString() + String.format("Valor Fixo: %.2f€%nValor Fixo (" + Atleta.TAXA_IRS + "% IRS): %.2f%n", this.m_dblValorFixo, this.calcularValorIRS());
    }
    
    /**
     * Calcula o valor mensal a atribuir a cada atleta profissional.
     * 
     * @return valor mensal a atribuir a cada atleta profissional
     */
    @Override
    public double calcularSalario() {
        return (this.m_dblValorFixo + ((percentagemParcelaVariavel / 100) * this.getPremios()));
    }
    
    /**
     * Retorna o valor a pagar ao Atleta Profissional, deduzindo a taxa do IRS.
     * 
     * @return valor a pagar ao Atleta Profissional, deduzindo a taxa do IRS.
     */
    @Override
    public double calcularValorIRS() {
        return (this.calcularSalario() - (TAXA_IRS / 100) * this.calcularSalario());
    }
    
    /**
     * Copia e retorna uma instância Atleta Profissional.
     * 
     * @return clone da instância Atleta Profissional
     */
    @Override
    public AtletaProfissional clone() {
        return new AtletaProfissional(this);
    }
    
}
