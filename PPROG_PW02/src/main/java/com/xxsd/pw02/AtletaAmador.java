package com.xxsd.pw02;

/**
 * @author XXSD
 */
public class AtletaAmador extends AtletaNaoProfissional {
    
    /**
     * Contador de atletas amadores.
     */
    static private int contadorAtletaAmador = 0;

    /**
     * Percentagem do valor mensal arrecadado dos atletas amadores.
     */
    static private double percentagemAmador = 7;

    /**
     * Constrói uma instância de Atleta Amador atribuindo ao nome, número de 
     * identificação civil, género, idade, atividade, frequência cardíaca em repouso
     * do atleta, valor de prémios arrecadados mensalmente e antiguidade do atleta
     * os valores por omissão respetivos.
     */
    public AtletaAmador() {
        super();
        contadorAtletaAmador++;
    }
    
    /**
     * Constrói uma instância de Atleta Amador recebendo o nome, número de identificação
     * civil, género, idade, atividade, valor de prémios arrecadados mensalmente
     * e antiguidade.
     * 
     * @param strNome o nome do atleta
     * @param strNIC o número de identificação civil do atleta
     * @param strGenero o género do atleta
     * @param intIdade a idade do atleta
     * @param strAtividade a atividade do atleta
     * @param intFCR a frequência cardíaca em repouso do atleta
     * @param dblPremios o valor de prémios arrecadados mensalmente pelo atleta
     * @param intAntiguidade a antiguidade do atleta
     */
    public AtletaAmador(String strNome, String strNIC, String strGenero, int intIdade, String strAtividade, int intFCR, double dblPremios, int intAntiguidade) {
        super(strNome, strNIC, strGenero, intIdade, strAtividade, intFCR, dblPremios, intAntiguidade);
        contadorAtletaAmador++;
    }
    
    /**
     * Constrói uma instância cópia de Atleta Amador.
     * 
     * @param oAtletaAmador a instância Atleta Amador a ser copiada
     */
    public AtletaAmador(AtletaAmador oAtletaAmador) {
        this(oAtletaAmador.getNome(), oAtletaAmador.getNIC(), oAtletaAmador.getGenero(), oAtletaAmador.getIdade(), oAtletaAmador.getAtividade(), oAtletaAmador.getFCR(), oAtletaAmador.getPremios(), oAtletaAmador.getAntiguidade());
    }
    
    /**
     * Devolve o valor contador de atletas amadores.
     * 
     * @return valor do contador de atletas amadores.
     */
    static public int getContadorAtletaAmador() {
        return contadorAtletaAmador;
    }
    
    /**
     * Devolve a descrição textual do atleta amador.
     *
     * @return caraterísticas do atleta amador
     */
    @Override
    public String toString() {
        return "Atleta Amador\n" + super.toString();
    }
    
    /**
     * Calcula o valor mensal a atribuir a cada atleta amador.
     * 
     * @return valor mensal a atribuir a cada atleta amador
     */
    @Override
    public double calcularSalario() {
        double salario = (((this.getAntiguidadePercentagem() / 100) * this.getPremios()) + ((percentagemAmador / 100) * this.getPremios()));
        if (salario < 5) {
            return 5;
        } else {
            return salario;
        }
    }
    
    /**
     * Copia e retorna uma instância Atleta Amador.
     * 
     * @return clone da instância Atleta Amador 
     */
    @Override
    public AtletaAmador clone() {
        return new AtletaAmador(this);
    }
    
}