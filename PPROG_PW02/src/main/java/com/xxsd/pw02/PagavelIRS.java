package com.xxsd.pw02;

/**
 * @author XXSD
 */
public interface PagavelIRS {
    
    /**
     * Retorna o novo valor a pagar ao atleta, descontando a taxa do IRS.
     * 
     * @return o novo valor a pagar ao atleta, descontando a taxad do IRS.
     */
    double calcularValorIRS();
}

