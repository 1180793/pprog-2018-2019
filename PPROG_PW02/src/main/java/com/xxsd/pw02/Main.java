package com.xxsd.pw02;

import com.xxsd.utilitarios.Data;
import java.util.ArrayList;
import java.util.List;

/**
 * @author XXSD
 */
public class Main {

    public static void main(String[] args) {
                
        List<Atleta> atletas = new ArrayList<>();
        
        AtletaProfissional prof1 = new AtletaProfissional("José Alberto", "30286721", Atleta.GENERO_MASCULINO, 32, Atleta.ATIVIDADE_NATACAO, 80, 300, 500);
        AtletaProfissional prof2 = new AtletaProfissional("Beatriz Ferreira", "10285932", Atleta.GENERO_FEMININO, 26, Atleta.ATIVIDADE_NATACAO, 75, 150, 610);
        AtletaProfissional prof3 = new AtletaProfissional("André Milhazes", "93223852", Atleta.GENERO_MASCULINO, 21, Atleta.ATIVIDADE_CICLISMO, 84, 220, 560);

        AtletaSemiProfissional semi1 = new AtletaSemiProfissional("Fernando Silva", "79680303", Atleta.GENERO_MASCULINO, 38, Atleta.ATIVIDADE_CAMINHADA, 82, 200, 11);
        AtletaSemiProfissional semi2 = new AtletaSemiProfissional("Ana Carneiro", "84938505", Atleta.GENERO_FEMININO, 33, Atleta.ATIVIDADE_CICLISMO, 70, 125, 3);
        AtletaSemiProfissional semi3 = new AtletaSemiProfissional("Tiago Pereira", "84026878", Atleta.GENERO_MASCULINO, 54, Atleta.ATIVIDADE_NATACAO, 82, 65, 24);

        AtletaAmador amad1 = new AtletaAmador("Jorge Pessoa", "89378862", Atleta.GENERO_MASCULINO, 25, Atleta.ATIVIDADE_CAMINHADA, 68, 250, 2);
        AtletaAmador amad2 = new AtletaAmador("Francisco Pontes", "87638468", Atleta.GENERO_MASCULINO, 35, Atleta.ATIVIDADE_NATACAO, 86, 100, 8);
        AtletaAmador amad3 = new AtletaAmador("Miguel Cunha", "76899783", Atleta.GENERO_MASCULINO, 32, Atleta.ATIVIDADE_CAMINHADA, 78, 135, 11);

        atletas.add(prof1);
        atletas.add(semi1);
        atletas.add(amad1);
        atletas.add(prof2);
        atletas.add(semi2);
        atletas.add(amad2);
        atletas.add(prof3);
        atletas.add(semi3);
        atletas.add(amad3);
        
        ClubeDesportivo clubeDesportivo = new ClubeDesportivo("Clube Desportivo TETSTETE", new Data(2018, 12, 1) , atletas);
        
        System.out.printf("%n%nNome do clube: %s%n", clubeDesportivo.getNome());
        
        System.out.println("\n\n### Lista de Atletas Inicial ###");
        for (Atleta atleta : clubeDesportivo.getAtletas()) {
            System.out.println(atleta.toString());
        }
        
        AtletaSemiProfissional novoAtleta = new AtletaSemiProfissional("Margarida Silva", "97303927", Atleta.GENERO_FEMININO, 28, Atleta.ATIVIDADE_CORRIDA, 78, 225, 4);
        
        clubeDesportivo.addAtleta(novoAtleta);
        
        System.out.println("\n\n### Lista de Atletas Ordenada inversamente pelo Valor dos Prémios ###");
        for (Atleta atleta : clubeDesportivo.getAtletasPremioInverso()) {
            System.out.println(atleta.toString());
        }
        
        System.out.println("\n\nValor Total (para efeitos de IRS) da totalidade dos Atletas: " + clubeDesportivo.calcularTotalSalario());
        
        System.out.println("\n\n### Lista de Atletas Ordenada Alfabeticamente por Categoria, Modalidade e Nome");
        for (Atleta atleta : clubeDesportivo.getAtletasOrdenados()) {
            System.out.println(atleta.toString());
        }
        
    }

}
