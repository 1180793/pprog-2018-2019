package com.xxsd.pw02;

import com.xxsd.utilitarios.Data;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author XXSD
 */
public class ClubeDesportivoTest {
    
    public ClubeDesportivoTest() {
    }

    /**
     * Test of getNome method, of class ClubeDesportivo.
     */
    @Test
    public void testGetNome() {
        System.out.println("getNome");
        
        ClubeDesportivo clube = new ClubeDesportivo("Clube Teste 123", new Data(2018, 12, 1) , new ArrayList<>());
        
        String expResult = "Clube Teste 123";
        String result = clube.getNome();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of addAtleta method, of class ClubeDesportivo.
     */
    @Test
    public void testAddAtleta() {
        System.out.println("addAtleta");
        
        List<Atleta> testAtletas = new ArrayList<>();
        
        AtletaSemiProfissional semi = new AtletaSemiProfissional("Ana Carneiro", "84938505", Atleta.GENERO_FEMININO, 33, Atleta.ATIVIDADE_CICLISMO, 70, 125, 3);
        AtletaProfissional prof = new AtletaProfissional("José Alberto", "30286721", Atleta.GENERO_MASCULINO, 32, Atleta.ATIVIDADE_NATACAO, 80, 300, 500);
        
        testAtletas.add(semi);
        testAtletas.add(prof);
        
        List<Atleta> testAtletasClube = new ArrayList<>();
        ClubeDesportivo clube = new ClubeDesportivo("Clube Teste 123", new Data(2018, 12, 1) , testAtletasClube);
        for (Atleta atleta : testAtletas) {
            clube.addAtleta(atleta);
        }
        
        assertEquals(clube.getAtletas().size(), testAtletas.size());
    }

    /**
     * Test of getAtletasAlfabeticamente method, of class ClubeDesportivo.
     */
    @Test
    public void testGetAtletasAlfabeticamente() {
        System.out.println("getAtletasAlfabeticamente");
        
        List<Atleta> testAtletas = new ArrayList<>();
        
        AtletaSemiProfissional atl1 = new AtletaSemiProfissional("Tiago Pereira", "84026878", Atleta.GENERO_MASCULINO, 54, Atleta.ATIVIDADE_NATACAO, 82, 65, 24);        
        AtletaSemiProfissional atl2 = new AtletaSemiProfissional("Ana Carneiro", "84938505", Atleta.GENERO_FEMININO, 33, Atleta.ATIVIDADE_CICLISMO, 70, 125, 3);
        AtletaProfissional atl3 = new AtletaProfissional("José Alberto", "30286721", Atleta.GENERO_MASCULINO, 32, Atleta.ATIVIDADE_NATACAO, 80, 300, 500);
        
        testAtletas.add(atl1);
        testAtletas.add(atl2);
        testAtletas.add(atl3);

        ClubeDesportivo clube = new ClubeDesportivo("Clube Teste 123", new Data(2018, 12, 1) , testAtletas);
        
        List<Atleta> listOrdenada = clube.getAtletasAlfabeticamente();
        char[] arrayChar = new char[3];
        arrayChar[0] = 'A';
        arrayChar[1] = 'J';
        arrayChar[2] = 'T';
        
        for (int k = 0; k < 3; k++) {
             assertEquals(arrayChar[k], listOrdenada.get(k).getNome().charAt(0));
        }
    }
}
