package com.xxsd.pw01;

/**
 * @author XXSD
 */
public abstract class AtletaNaoProfissional extends Atleta {
    
    /**
     * Antiguidade do atleta não profissional.
     */
    private int m_intAntiguidade;
    
    /**
     * A percentagem da antiguidade dos atletas não profissionais no intervalo [5,10] anos.
     */
    static private double percPequena = 2;
    
    /**
     * A percentagem da antiguidade dos atletas não profissionais no intervalo ]10,20] anos.
     */
    static private double percMedia = 8;
    
    /**
     * A percentagem da antiguidade dos atletas não profissionais no intervalo [20,+∞[ anos.
     */
    static private double percGrande = 20;
    
    /**
     * Contador de atletas não profissionais.
     */
    static private int contadorAtletaNaoProfissional = 0;

    /**
     * A antiguidade do atleta não profissional, por omissão.
     */
    private final int ANTIGUIDADE_POR_OMISSAO = 0;
    
    /**
     * Constrói uma instância de Atleta Não Profissional atribuindo ao nome, número de 
     * identificação civil, género, idade, atividade, frequência cardíaca em repouso
     * do atleta, valor de prémios arrecadados mensalmente e antiguidade do atleta
     * os valores por omissão respetivos.
     */
    public AtletaNaoProfissional() {
        super();
        this.m_intAntiguidade = ANTIGUIDADE_POR_OMISSAO;
        contadorAtletaNaoProfissional++;
    }
    
    /**
     * Constrói uma instância de Atleta Não Profissional recebendo o nome, número de identificação
     * civil, género, idade, atividade, valor de prémios arrecadados mensalmente
     * e antiguidade.
     * 
     * @param strNome o nome do atleta
     * @param strNIC o número de identificação civil do atleta
     * @param strGenero o género do atleta
     * @param intIdade a idade do atleta
     * @param strAtividade a atividade do atleta
     * @param intFCR a frequência cardíaca em repouso do atleta
     * @param dblPremios o valor de prémios arrecadados mensalmente pelo atleta
     * @param intAntiguidade a antiguidade do atleta
     */
    public AtletaNaoProfissional(String strNome, String strNIC, String strGenero, int intIdade, String strAtividade, int intFCR, double dblPremios, int intAntiguidade) {
        super(strNome, strNIC, strGenero, intIdade, strAtividade, intFCR, dblPremios);
        this.m_intAntiguidade = intAntiguidade;            
        contadorAtletaNaoProfissional++;
    }
    
    /**
     * Devolve a antiguidade do atleta.
     * 
     * @return antiguidade do atleta. 
     */
    public int getAntiguidade() {
        return this.m_intAntiguidade;
    }
    
    /**
     * Modifica a antiguidade do atleta.
     * 
     * @param novaAntiguidade a nova antiguidade do atleta 
     */
    public void setAntiguidade(int novaAntiguidade) {
        this.m_intAntiguidade = novaAntiguidade;
    }
    
    /**
     * Devolve a percentagem do valor mensal arrecadado, de acordo com a antiguidade.
     * 
     * @return percentagem do valor mensal arrecadado, de acordo com a antiguidade
     */
    public double getAntiguidadePercentagem() {
        int antiguidade = this.m_intAntiguidade;
        if (antiguidade >= 5 && antiguidade <= 10) {
            return percPequena;
        } else if (antiguidade > 10 && antiguidade <= 20) {
            return percMedia;
        } else if (antiguidade > 20) {
            return percGrande;
        } else {
            return 0;
        }
    }
    
    /**
     * Devolve o valor do contador de atletas não profissionais.
     * 
     * @return  valor do contador de atletas não profissionais
     */
    public int getContadorAtletaNaoProfissional() {
        return contadorAtletaNaoProfissional;
    }

    /**
     * Modifica a percentagem da antiguidade dos atletas não profissionais no intervalo [5,10] anos.
     * 
     * @param novaPercPequena nova percentagem da antiguidade dos atletas não profissionais no intervalo [5,10] anos
     */
    static public void setPercPequena(double novaPercPequena) {
        percPequena = novaPercPequena;
    }

    /**
     * Modifica a percentagem da antiguidade dos atletas não profissionais no intervalo ]10,20] anos.
     * 
     * @param novaPercMedia nova percentagem da antiguidade dos atletas não profissionais no intervalo ]10,20] anos
     */
    static public void setPercMedia(double novaPercMedia) {
        percMedia = novaPercMedia;
    }
    
    /**
     * Modifica a percentagem da antiguidade dos atletas não profissionais no intervalo [20,+∞[ anos.
     * 
     * @param novaPercGrande nova percentagem da antiguidade dos atletas não profissionais no intervalo [20,+∞[ anos
     */
    static public void setPercGrande(double novaPercGrande) {
        percGrande = novaPercGrande;
    }
    
    /**
     * Devolve a descrição textual do atleta não profissional.
     *
     * @return caraterísticas do atleta não profissional
     */
    @Override
    public String toString() {
        return super.toString() + String.format("Antiguidade: %d%n", this.m_intAntiguidade);
    }

}
