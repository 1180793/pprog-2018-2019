package com.xxsd.pw01;

/**
 * @author XXSD
 */
public class Main {

    public static void main(String[] args) {

        AtletaProfissional prof1 = new AtletaProfissional("José Alberto", "30286721", Atleta.GENERO_MASCULINO, 32, Atleta.ATIVIDADE_CORRIDA, 80, 300, 500);
        AtletaProfissional prof2 = new AtletaProfissional("Beatriz Ferreira", "10285932", Atleta.GENERO_FEMININO, 26, Atleta.ATIVIDADE_NATACAO, 75, 150, 610);

        AtletaSemiProfissional semi1 = new AtletaSemiProfissional("Fernando Silva", "79680303", Atleta.GENERO_MASCULINO, 38, Atleta.ATIVIDADE_CAMINHADA, 82, 200, 11);
        AtletaSemiProfissional semi2 = new AtletaSemiProfissional("Ana Carneiro", "84938505", Atleta.GENERO_FEMININO, 33, Atleta.ATIVIDADE_CICLISMO, 70, 125, 3);
        AtletaSemiProfissional semi3 = new AtletaSemiProfissional("Tiago Pereira", "84026878", Atleta.GENERO_MASCULINO, 54, Atleta.ATIVIDADE_NATACAO, 82, 65, 24);

        AtletaAmador amad1 = new AtletaAmador("Jorge Pessoa", "89378862", Atleta.GENERO_MASCULINO, 25, Atleta.ATIVIDADE_CAMINHADA, 68, 250, 2);
        AtletaAmador amad2 = new AtletaAmador("Francisco Pontes", "87638468", Atleta.GENERO_MASCULINO, 35, Atleta.ATIVIDADE_NATACAO, 86, 100, 8);

        Atleta[] atletas = new Atleta[7];

        atletas[0] = prof1;
        atletas[1] = prof2;
        atletas[2] = semi1;
        atletas[3] = semi2;
        atletas[4] = semi3;
        atletas[5] = amad1;
        atletas[6] = amad2;

        System.out.println("### Listagem de Nome, FCM, FCTs de Atletas Não Profissionais###");
        for (Atleta atleta : atletas) {
            if (atleta instanceof AtletaNaoProfissional) {
                System.out.println("\nNome: " + atleta.getNome());
                System.out.println("Frequência Cardíaca Máxima: " + atleta.calcFreqCardiacaMax());
                System.out.println("Frequência Cardíaca de Trabalho (Queima de Gordura): " + atleta.calcFCT_QueimaGordura());
                System.out.println("Frequência Cardíaca de Trabalho (Queima de Gordura): " + atleta.calcFCT_QueimaGordura());
            }
        }

        double totalProfissional = 0;
        double totalSemiProfissional = 0;
        double totalAmador = 0;
        double total = 0;

        System.out.println("\n### Listagem de Atletas e Valores a Pagar###");
        for (Atleta atleta : atletas) {
            System.out.println("\nNome: " + atleta.getNome());
            System.out.println("Valor a Pagar: " + atleta.calcularSalario());
            total = total + atleta.calcularSalario();
            if (atleta instanceof AtletaProfissional) {
                totalProfissional = totalProfissional + atleta.calcularSalario();
            }
            if (atleta instanceof AtletaSemiProfissional) {
                totalSemiProfissional = totalSemiProfissional + atleta.calcularSalario();
            }
            if (atleta instanceof AtletaAmador) {
                totalAmador = totalAmador + atleta.calcularSalario();
            }
        }

        System.out.println("\nNúmero de Atletas Amadores: " + AtletaAmador.getContadorAtletaAmador());
        System.out.println("Número de Atletas Profissionais: " + AtletaProfissional.getContadorAtletaProfissional());
        
        System.out.println("\nValor a Pagar aos Atletas Profissionais: " + totalProfissional);
        System.out.println("Valor a Pagar aos Atletas SemiProfissionais: " + totalSemiProfissional);
        System.out.println("Valor a Pagar aos Atletas Amadores: " + totalAmador);
        System.out.println("Valor a Pagar a Todos os Atletas: " + total);

    }

}
