package com.xxsd.pw01;

/**
 * @author XXSD
 */
public class AtletaSemiProfissional extends AtletaNaoProfissional {
    
    /**
     * Parcela fixa dos atletas semi profissionais.
     */
    private double dblValorFixo = 300;
    
    /**
     * Contador dos atletas semi profissionais.
     */
    static private int contadorAtletaSemiProfissional = 0;

    /**
     * Constrói uma instância de Atleta Semi Profissional atribuindo ao nome, número de 
     * identificação civil, género, idade, atividade, frequência cardíaca em repouso
     * do atleta, valor de prémios arrecadados mensalmente e antiguidade do atleta
     * os valores por omissão respetivos.
     */
    public AtletaSemiProfissional() {
        super();
        contadorAtletaSemiProfissional++;
    }
    
    /**
     * Constrói uma instância de Atleta Semi Profissional recebendo o nome, número de identificação
     * civil, género, idade, atividade, valor de prémios arrecadados mensalmente
     * e antiguidade.
     * 
     * @param strNome o nome do atleta
     * @param strNIC o número de identificação civil do atleta
     * @param strGenero o género do atleta
     * @param intIdade a idade do atleta
     * @param strAtividade a atividade do atleta
     * @param intFCR a frequência cardíaca em repouso do atleta
     * @param dblPremios o valor de prémios arrecadados mensalmente pelo atleta
     * @param intAntiguidade a antiguidade do atleta
     */
    public AtletaSemiProfissional(String strNome, String strNIC, String strGenero, int intIdade, String strAtividade, int intFCR, double dblPremios, int intAntiguidade) {
        super(strNome, strNIC, strGenero, intIdade, strAtividade, intFCR, dblPremios, intAntiguidade);
        contadorAtletaSemiProfissional++;
    }
    
    /**
     * Devolve a parcela fixa dos atletas semi profissionais.
     * 
     * @return parcela fixa dos atletas semi profissionais
     */
    public double getValorFixo() {
        return dblValorFixo;
    }
    
    /**
     * Modifica a parcela fixa dos atletas semi profissionais.
     * 
     * @param novoValorFixo nova parcela fixa dos atletas semi profissionais
     */
    public void setValorFixo(double novoValorFixo) {
        dblValorFixo = novoValorFixo;
    }
    
    /**
     * Devolve o valor do contador de atletas semi profissionais.
     * 
     * @return valor do contador de atletas semi profissionais
     */
    static public int getContadorAtletaSemiProfissional() {
        return contadorAtletaSemiProfissional;
    }
    
    /**
     * Calcula o valor mensal a atribuir a cada atleta semi profissional.
     * 
     * @return valor mensal a atribuir a cada atleta semi profissional
     */
    @Override
    public double calcularSalario() {
        return dblValorFixo + ((this.getAntiguidadePercentagem() / 100) * dblValorFixo);
    }
    
}
