package com.xxsd.pw01;

/**
 * @author XXSD
 */
public class AtletaProfissional extends Atleta {
    
    /**
     * O valor fixo do atleta.
     */
    private double m_dblValorFixo;
    
    /**
     * A percentagem da parcela variável dos atletas profissionais.
     */
    private double percentagemParcelaVariavel = 20;
    
    /**
     * Contador de atletas profissionais.
     */
    static private int contadorAtletaProfissional = 0;

    /**
     * A parcela fixa do atleta, por omissão.
     */
    private final int FIXO_POR_OMISSAO = 0;
    
    /**
     * Constrói uma instância de Atleta Profissional atribuindo ao nome, número de 
     * identificação civil, género, idade, atividade, frequência cardíaca em repouso
     * do atleta, valor de prémios arrecadados mensalmente e parcela fixa do atleta
     * os valores por omissão respetivos.
     */
    public AtletaProfissional() {
        super();
        this.m_dblValorFixo = FIXO_POR_OMISSAO;
        contadorAtletaProfissional++;
    }
    
    /**
     * Constrói uma instância de AtletaProfissional recebendo o nome, número de identificação
     * civil, género, idade, atividade, valor de prémios arrecadados mensalmente e parcela fixa.
     * 
     * @param strNome o nome do atleta
     * @param strNIC o número de identificação civil do atleta
     * @param strGenero o género do atleta
     * @param intIdade a idade do atleta
     * @param strAtividade a atividade do atleta
     * @param intFCR a frequência cardíaca em repouso do atleta
     * @param dblPremios o valor de prémios arrecadados mensalmente pelo atleta
     * @param dblValorFixo a parcela fixa do atleta
     */
    public AtletaProfissional(String strNome, String strNIC, String strGenero, int intIdade, String strAtividade,int intFCR, double dblPremios, double dblValorFixo) {
        super(strNome, strNIC, strGenero, intIdade, strAtividade,intFCR,dblPremios);
        this.m_dblValorFixo = dblValorFixo;
        contadorAtletaProfissional++;
    }
    
    /**
     * Devolve a parcela fixa do atleta.
     * 
     * @return parcela fixa do atleta 
     */
    public double getValorFixo() {
        return this.m_dblValorFixo;
    }
    
    /**
     * Modifica a parcela fixa do atleta.
     * 
     * @param dblNovoValorFixo a nova parcela fixa do atleta
     */
    public void setValorFixo(double dblNovoValorFixo) {
        this.m_dblValorFixo = dblNovoValorFixo;
    }
    
    /**
     * Devolve a parcela variável dos atletas profissionais.
     * 
     * @return parcela variável dos atletas profissionais
     */
    public double getPercentagemParcelaVariavel() {
        return percentagemParcelaVariavel;
    }
    
    /**
     * Modifica a percentagem da parcela variável dos atletas profissionais.
     * 
     * @param novaPercentagem  a nova percentagem da parcela variável dos atletas profissionais
     */
    public void setPercentagemParcelaVariavel(double novaPercentagem) {
        percentagemParcelaVariavel = novaPercentagem;
    }
    
    /**
     * Devolve o valor do contador de atletas profissionais.
     * 
     * @return valor do contador de atletas profissionais. 
     */
    static public int getContadorAtletaProfissional() {
        return contadorAtletaProfissional;
    }
    
    /**
     * Calcula o valor mensal a atribuir a cada atleta profissional.
     * 
     * @return valor mensal a atribuir a cada atleta profissional
     */
    @Override
    public double calcularSalario() {
        return (this.m_dblValorFixo + ((percentagemParcelaVariavel / 100) * this.getPremios()));
    }
    
    /**
     * Devolve a descrição textual do atleta profissional.
     *
     * @return caraterísticas do atleta profissional
     */
    @Override
    public String toString() {
        return super.toString() + String.format("Valor Fixo: %d%n", this.m_dblValorFixo);
    }
}
