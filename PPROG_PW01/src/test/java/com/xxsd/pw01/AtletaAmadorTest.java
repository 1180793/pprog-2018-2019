package com.xxsd.pw01;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author XXSD
 */
public class AtletaAmadorTest {

    /**
     * Test of calcularSalario method, of class AtletaAmador.
     */
    @Test
    public void testCalcularSalario() {
        System.out.println("calcularSalario");
        AtletaAmador amad1 = new AtletaAmador("Francisca Pontes", "87638468", Atleta.GENERO_FEMININO, 35, Atleta.ATIVIDADE_CICLISMO, 86, 100, 8);
        double expResult = 9;
        double result = amad1.calcularSalario();
        assertEquals(expResult, result, 0.0);
    }
    
}
