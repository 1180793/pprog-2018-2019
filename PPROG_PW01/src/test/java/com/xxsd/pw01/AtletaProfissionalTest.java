package com.xxsd.pw01;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author XXSD
 */
public class AtletaProfissionalTest {

    /**
     * Test of calcularSalario method, of class AtletaProfissional.
     */
    @Test
    public void testCalcularSalario() {
        System.out.println("calcularSalario");
        AtletaProfissional prof1 = new AtletaProfissional("José Alberto", "30286721", Atleta.GENERO_MASCULINO, 32, Atleta.ATIVIDADE_CORRIDA, 80, 300, 500);
        double expResult = 560;
        double result = prof1.calcularSalario();
        assertEquals(expResult, result, 0.0);
    }
}
